const {I} = inject();

Given(`I have entered in {string} page`, (page) => {
    I.amOnPage(page);
});

When(`I enter login data :`, (table) => {
    table.rows.forEach(row => {
        I.fillField(row.cells[0].value, row.cells[1].value);
    })
});

When('the checkbox {string} is active', (str) => {
    I.click(str, '//main');
});

When('I press form button {string}', (str) => {
    I.click(str, '//main//form//button[@type="submit"]');
});

When(`I press button {string}`, (buttonText) => {
    I.click(`//main//a[contains(text(), "${buttonText}")]`);
});

Then('I wait {int} sec', function (sec) {
    I.wait(sec);
});

When(`I enter creation data:`, (table) => {
    table.rows.forEach(row => {
        I.fillField(row.cells[0].value, row.cells[1].value);
    });
});

Then(`I see message {string}`,  function (str) {
    I.see(str);
});

When('I enter new part number data :', function (table) {
    table.rows.forEach(row => {
        I.fillField(row.cells[0].value, row.cells[1].value);
    });
});

When('I press add part number button', function () {
    I.click(`(//button[contains(@id, 'addBtn')])[1]`);
});

