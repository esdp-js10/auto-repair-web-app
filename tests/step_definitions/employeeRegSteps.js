const {I} = inject();

Given(`I have entered in {string} page`, (page) => {
	I.amOnPage(page);
});

When(`I enter login data :`, (table) => {
	table.rows.forEach(row => {
		I.fillField(row.cells[0].value, row.cells[1].value);
	})
});

When('the checkbox {string} is active', (str) => {
	I.click(str, '//main');
});

When('I press reg form button {string}', (str) => {
	I.click(str, '//main//form//button[@type="submit"]');
});

When(`I press warehouse button {string}`, (buttonText) => {
	I.click(`//main//p[contains(text(), "Активный")]/parent::div/parent::div//a[contains(text(), "${buttonText}")]`);
});

Then('I wait {int} sec', function (sec) {
	I.wait(sec);
});

When(`I enter registration data :`, (table) => {
	table.rows.forEach(row => {
		I.fillField(row.cells[0].value, row.cells[1].value);
	});
});

Then(`I see message {string}`,  function (str) {
	I.see(str);
});

When('I select the 1st option for element',  function () {
	I.click(`//form//div[contains(@class, 'MuiSelect-select')]`);
	I.click('//ul//li[contains(., "Складовщик")]');
});

Then('I have entered in {string} page', function (page) {
	I.amOnPage(page);
});