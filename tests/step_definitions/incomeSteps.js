const {I} = inject();

Given(`I have entered in {string} page`, (page) => {
	I.amOnPage(page);
});

When(`I enter login data :`, (table) => {
	table.rows.forEach(row => {
		I.fillField(row.cells[0].value, row.cells[1].value);
	});
});

When('the checkbox {string} is active', (str) => {
	I.click(str, '//main');
});

When('I press form button {string}', (str) => {
	I.click(str, '//main//form//button[@type="submit"]');
});

When(`I press button {string}`, (buttonText) => {
	I.click(`(//div[contains(@class, 'warehouse')]//a[contains(text(), '${buttonText}')])[1]`);
});

Then('I wait {int} sec', function (sec) {
	I.wait(sec);
});

Then(`I see message {string}`,  function (str) {
	I.see(str);
});

Then('I have entered in {string} page', function (page) {
	I.amOnPage(page);
});

Then('I select {string} type, part number', function (str) {
	if (str === 'first') {
		I.click(`(//form//div[contains(@class, 'formRow')]//input[contains(@name, 'type')])[1]`);
		I.click('//ul//li[contains(., "Гвоздь")]');
		I.wait(1);
		I.click(`(//form//div[contains(@class, 'formRow')]//input[contains(@name, 'partNumber')])[1]`);
		I.click('//ul//li[contains(., "a1")]');
	}

	if (str === 'second') {
		I.click(`(//form//div[contains(@class, 'formRow')]//input[contains(@name, 'type')])[2]`);
		I.click('//ul//li[contains(., "Болт")]');
		I.wait(1);
		I.click(`(//form//div[contains(@class, 'formRow')]//input[contains(@name, 'partNumber')])[2]`);
		I.click('//ul//li[contains(., "b3")]');
	}
});

When('I enter new detail data :', function (table) {
	table.rows.forEach(row => {
		I.fillField(row.cells[0].value, row.cells[1].value);
	});
});

When('I enter {string} currency', function (str) {
	if (str === 'delivery') {
		I.click(`(//input[contains(@name, 'currency')])[1]`);
		I.click(`//ul//li[contains(., "USD")]`);
	}

	if (str === 'customs') {
		I.click(`(//input[contains(@name, 'currency')])[2]`);
		I.click('//ul//li[contains(., "RUB")]');
	}

	if (str === 'commission') {
		I.click(`(//input[contains(@name, 'currency')])[3]`);
		I.click('//ul//li[contains(., "EUR")]');
	}
});

When('I press add detail button', function () {
	I.click(`(//button[contains(@id, 'addBtn')])[1]`);
});