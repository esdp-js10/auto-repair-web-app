@addDetail
Feature: Detail type creation
  As an admin
  I should be able to create detail type

  Background:
    Given I have entered in "login" page
    When I enter login data :
      | email    | admin@gmail.com |
      | password | 123             |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I wait 1 sec

  @detailCreation
  Scenario:
    Given I have entered in "admin/warehouses/details" page
    And I press button "Создать деталь"
    When I enter creation data:
      | type | Detail |
    And I press button "Создать"
    Then I see message "Деталь создана"

  @addPartNumber
  Scenario: 
    Given I have entered in "admin/warehouses/details" page
    And I press button "Добавить парт номер"
    When I enter new part number data :
      | (//form//input[contains(@name, 'partNumber')])[1]     | part1  |
      | (//form//input[contains(@name, 'sizeIn')])[1]     | 2m  |
      | (//form//input[contains(@name, 'sizeOut')])[1] | 3m |
      | (//form//input[contains(@name, 'material')])[1] | metal |
    And I press add part number button
    When I enter new part number data :
      | (//form//input[contains(@name, 'partNumber')])[2]     | part2  |
      | (//form//input[contains(@name, 'sizeIn')])[2]     | 5m  |
      | (//form//input[contains(@name, 'sizeOut')])[2] | 6m |
      | (//form//input[contains(@name, 'material')])[2] | aluminum |
    And I press form button "Сохранить"
    Then I see message "Данные успешно сохранены!"

  


