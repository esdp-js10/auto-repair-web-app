@userRegReq
Feature: Registration requests
  As an admin
  I need to be able to see new user registration requests
  And approve or deny any user request

  Background:
    Given I have entered in "login" page
    When I enter login data :
      | email    | admin@gmail.com |
      | password | 123             |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I'm not on the "login" page
    And I don't see "Войти" button

  @userRegReqAccept
  Scenario:
    Given I have entered in "admin/clients/reg_requests" page
    Then "John" have entered in "register" page
    When "John" filled out account information and press "Подтвердить" to confirm registration :
      | email    | john347@gmail.com |
      | password | 123               |
      | name     | John              |
      | phone    | (996) 42-99-54    |
      | address  | Bishkek           |
    Then I wait 7 sec until I see the text "John"
    Then I press button "Подробнее"
    Then I press button "Принять"
    When "John" have entered in "login" page
    Then "John" enter login data :
      | email    | john347@gmail.com |
      | password | 123               |
    When "John" press form button "Войти"
    Then "John" not on the "login" page
    And "John" don't see "Войти" button in "header"

  @userRegReqDecline
  Scenario:
    Given I have entered in "admin/clients/reg_requests" page
    Then "Marya" have entered in "register" page
    Then "Marya" filled out account information and press "Подтвердить" to confirm registration :
      | email    | marya544@gmail.com |
      | password | 123                |
      | name     | Marya              |
      | phone    | (996) 42-99-54     |
      | address  | Bishkek            |
    Then I wait 7 sec until I see the text "Marya"
    When I press button "Подробнее"
    Then I press button "Отклонить"
    Then "Marya" have entered in "login" page
    Then "Marya" enter login data :
      | email    | marya544@gmail.com |
      | password | 123                |
    When "Marya" press form button "Войти"
    Then "Marya" does not see message "Ваш профиль еще неактивирован"
