@login
Feature: Login user
  As a registered user
  I should be able to log into the system
  When I enter my details data
  Then I will be on the main page
  And can't login again unless I log out

  @UserLogIn
  Scenario:
    Given I have entered in "login" page
    When I enter login data :
      | email    | user@gmail.com |
      | password | 123            |
    And I press form button "Войти"
    Then I'm not on the "login" page
    And I don't see "Войти" button

  @RepairerLogIn
  Scenario:
    Given I have entered in "login" page
    When I enter login data :
      | email    | repairer@gmail.com |
      | password | 123                |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I'm not on the "login" page
    And I don't see "Войти" button


  @WarehousemanLogIn
  Scenario:
    Given I have entered in "login" page
    When I enter login data :
      | email    | warehouseman1@gmail.com |
      | password | 123                     |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I'm not on the "login" page
    And I don't see "Войти" button


  @AdminLogIn
  Scenario:
    Given I have entered in "login" page
    When I enter login data :
      | email    | admin@gmail.com |
      | password | 123             |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I'm not on the "login" page
    And I don't see "Войти" button
