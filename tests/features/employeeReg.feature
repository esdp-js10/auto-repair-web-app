@employeeRegister
Feature:  Register employee
  As an admin
  I should be able to create employee account

  Background:
    Given I have entered in "login" page
    When I enter login data :
      | email    | admin@gmail.com |
      | password | 123             |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I wait 1 sec

  Scenario:
    Given I have entered in "admin/employees" page
    And I press button "Создать"
    When I enter registration data :
      | email           | non15@gmail.com |
      | password        | secret          |
      | confirmPassword | secret          |
      | name            | Noni            |
      | INN             | 32145698783216  |
      | phoneNumber     | 0(777)-55-86-66 |
      | address         | Bishkek         |
    And I select the 1st option for element
    Then I wait 1 sec
    When I press reg form button "Создать"
    Then I see message "Создан новый сотрудник!"
    Then I have entered in "admin/employees" page
