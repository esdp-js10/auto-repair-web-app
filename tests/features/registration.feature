@registerUser
Feature: Register user
  As a user who want to register
  I should be able to create account
  When I going on registration page
  Then I enter my details data
  Then I will be on the main page
  And I see a message saying that
  I can't sign in until the administration has verified my account

  Scenario:
    Given I have entered in "register" page
    When I enter registration data :
      | email    | user347@gmail.com |
      | password | 123               |
      | name     | User              |
      | phone    | (996) 42-99-54    |
      | address  | Bishkek           |
    And I press form button "Подтвердить"
    Then I'm not on the "register" page
    Then I have entered in "login" page
    When I enter login data :
      | email    | user347@gmail.com |
      | password | 123               |
    And I press form button "Войти"
    Then I see message "Ваш аккаунт неактивен"