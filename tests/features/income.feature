@addIncome
Feature:  Create new parts in the warehouse
  As an admin (or warehouseman)
  I can make a incoming to a warehouse

  Background:
    Given I have entered in "login" page
    When I enter login data :
      | email    | admin@gmail.com |
      | password | 123             |
    When the checkbox "Войти как сотрудник" is active
    And I press form button "Войти"
    Then I wait 1 sec

  @incomeDetails
  Scenario:
    Given I have entered in "admin/warehouses" page
    Then I wait 1 sec
    And I press warehouse button "Приход"
    And I select "first" type, part number
    When I enter new detail data :
      | (//form//input[contains(@name, 'quantity')])[1]     | 120  |
      | (//form//input[contains(@name, 'priceInitial')])[1] | 4500 |
    And I press add detail button
    And I select "second" type, part number
    When I enter new detail data :
      | (//form//input[contains(@name, 'quantity')])[2]     | 220 |
      | (//form//input[contains(@name, 'priceInitial')])[2] | 700 |
    When I enter new detail data :
      | (//form//input[contains(@name, 'currentCost')])[1] | 450 |
    And I enter "delivery" currency
    When I enter new detail data :
      | (//form//input[contains(@name, 'rate')])[1]        | 100 |
      | (//form//input[contains(@name, 'currentCost')])[2] | 450 |
    And I enter "customs" currency
    When I enter new detail data :
      | (//form//input[contains(@name, 'rate')])[2]        | 1   |
      | (//form//input[contains(@name, 'currentCost')])[3] | 450 |
    And I enter "commission" currency
    When I enter new detail data :
      | (//form//input[contains(@name, 'rate')])[3] | 95 |
    And I press form button "Сохранить"
    Then I see message "Данные успешно сохранены!"
    Then I have entered in "admin/warehouses" page