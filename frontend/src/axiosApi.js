import axios from 'axios';
import {BASE_URL, USER_REFRESH_TOKEN_URL} from "./config";
import {logOutEmployee} from "./store/actions/usersActions";
import store from "./store/configurateStore";

const axiosApi = axios.create({
  withCredentials: true,
  baseURL: BASE_URL,
});

axiosApi.interceptors.request.use(config => {
  config.headers = {
    Authorization: `Bearer ${localStorage.getItem('gdt_token')}`,
  }
  return config;
}, error => {
  return Promise.reject({error});
});

axiosApi.interceptors.response.use((config) => {
  return config;
}, async error => {
  const originalRequest = error.config;
  if (error.response.status === 401 && error.config && !error.config._isRetry) {
    originalRequest._isRetry = true;
    try {
      const {data} = await axios.get(BASE_URL + USER_REFRESH_TOKEN_URL, {withCredentials: true});
      localStorage.setItem('gdt_token', data.accessToken);
      return axiosApi.request(originalRequest);
    } catch (err) {
      if (err.response.status === 401){
        const {dispatch} = store
        dispatch(logOutEmployee());
      } else {
        console.log('Проблема с токеном доступа');
      }
    }
  }

  return Promise.reject(error);
})

export default axiosApi;