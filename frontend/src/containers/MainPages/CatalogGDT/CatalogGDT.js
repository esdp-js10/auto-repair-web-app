import React, {useEffect, useState} from 'react';
import {
  CardMedia,
  Chip,
  CircularProgress,
  Container,
  Grid,
  IconButton,
  Stack,
  TablePagination,
  useMediaQuery
} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {fetchGDTs, fetchSearchOptions} from "../../../store/actions/GDTsActions";
import ModalUI from "../../../components/UI/ModalUI/ModalUI";
import {AnimatePresence, motion} from "framer-motion";
import InfoGDT from "./InfoGDT/InfoGDT";
import CardGDT from "./CardGDT/CardGDT";
import InpSearchGDT from "./InpSearchGDT/InpSearchGDT";
import {useLocation} from "react-router-dom";
import {historyPush} from "../../../store/actions/historyActions";
import LayersClearTwoToneIcon from '@mui/icons-material/LayersClearTwoTone';
import theme from "../../../theme";

const CatalogGDT = () => {
  const dispatch = useDispatch();
  const {search} = useLocation();
  const GDTs = useSelector(state => state.GDT.data);
  const fetchLoading = useSelector(state => state.GDT.fetchLoading);
  const searchOptions = useSelector(state => state.GDT.searchOptions);
  const [currentModalContent, setCurrentModalContent] = useState({});
  const [selectedOptions, setSelectedOptions] = useState(null);
  const [itemsPerPage, setItemsPerPage] = React.useState(12);
  const [page, setPage] = React.useState(0);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const isDownSm = useMediaQuery(theme.breakpoints.down('sm'));

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeItemsPerPage = (event) => {
    setItemsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const onOptionRemove = (option) => {
    setSelectedOptions(prev => {
      const newOptions = [...prev];
      newOptions.splice(selectedOptions.indexOf(option), 1);
      return newOptions;
    })
  };

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };

  const onSlideHandler = (i, el) => {
    if (isDownSm) return;
    const src = el.props.src;
    setCurrentModalContent({src, type: 'image'});
    openModal();
  };
  const onCardHandler = (e, data) => {
    setCurrentModalContent({data, type: 'info'});
    openModal();
  };

  useEffect(() => {
    setPage(0)
  }, [dispatch, search])

  useEffect(() => {

    dispatch(fetchGDTs(search + (search ? "&" : "?") + `page=${page + 1}&itemsPerPage=${itemsPerPage}`));
  }, [dispatch, search, page, itemsPerPage])

  useEffect(() => {
    if (!searchOptions) {
      dispatch(fetchSearchOptions());
    }
    if (searchOptions) {
      const initParams = [...new URLSearchParams(search)];
      const checkedOptions = [];
      initParams.forEach(arr => {
        const checkedOption = searchOptions.find(o => (o.group === arr[0]) && (o.value === arr[1]));
        if (checkedOption) {
          checkedOptions.push(checkedOption);
        }
      })
      setSelectedOptions(checkedOptions);
    }

  }, [dispatch, searchOptions])

  useEffect(() => {
    if (!selectedOptions) return;
    let queryStr = `?`
    selectedOptions.forEach((option, i) => {
      if (i > 0) {
        queryStr += `&${option.group}=${option.value}`
      } else {
        queryStr += `${option.group}=${option.value}`
      }
    })

    dispatch(historyPush(queryStr));

  }, [dispatch, selectedOptions])

  return (
    <Stack my={3}>
      <Container sx={{
        background: '#f9f9ff',
        border: '1px solid gainsboro',
        minHeight: '90vh',
        mb: 2,
        display: "flex",
        flexDirection: "column"
      }}>
        <Stack minHeight={170}>
          <Grid container justifyContent={'space-between'} my={2} spacing={2}>
            <Grid item flexGrow={3} sm={12} md={6}>
              <Grid container width={"100%"} justifyContent={"flex-start"} alignItems={"center"} flexWrap={'nowrap'}>
                <Grid item flexGrow={1} xs={11}>
                  <InpSearchGDT
                    searchOptions={searchOptions || []}
                    setSelectedOptions={setSelectedOptions}
                  />
                </Grid>
                <Grid item ml={1}>
                  <IconButton onClick={() => {
                    if (selectedOptions?.length < 1) return;
                    setSelectedOptions([])
                  }}>
                    <LayersClearTwoToneIcon/>
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
            <Grid item sm={12} md={6}>
              <TablePagination
                component="div"
                count={GDTs?.totalDocs || 0}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={itemsPerPage}
                onRowsPerPageChange={handleChangeItemsPerPage}
                labelRowsPerPage={'Кол-во :'}
                rowsPerPageOptions={[12, 36, 60]}
              />
            </Grid>
          </Grid>
          <Grid container justifyContent={"space-between"}>
            <Grid item container mb={2} spacing={1} xs={12} sm={10}>
              {selectedOptions?.length > 0 && (
                selectedOptions.map(option => (
                  <Grid item key={option._id}>
                    <Chip
                      label={option.value}
                      variant="contained"
                      sx={{background: 'white', border: '1px solid lightgrey'}}
                      onClick={() => onOptionRemove(option)}
                      onDelete={() => onOptionRemove(option)}
                    />
                  </Grid>
                ))
              )}
            </Grid>
            {fetchLoading && (
              <Grid container item xs={12} sm={2} justifyContent={"center"}>
                <CircularProgress color="inherit"/>
              </Grid>
            )}
          </Grid>

        </Stack>
        <Stack flexGrow={10} sx={{borderTop: '1px dotted gainsboro', borderBottom: '1px dotted gainsboro'}} pb={2}>
          <AnimatePresence>
            <Grid container
                  spacing={2}
                  mt={1}
                  justifyContent={'center'}
            >
              {GDTs?.docs?.length > 0 && selectedOptions && (
                <>
                  {GDTs.docs.map(GDT => (
                    <Grid item key={GDT._id} width={'100%'} xs={12} sm={12} md={6} lg={4}>
                      <motion.div
                        initial={{opacity: 0}}
                        animate={{opacity: 1}}
                        exit={{opacity: 0}}
                        transition={{times: [0, 0.1, 0.9, 1]}}
                        layout
                      >
                        <CardGDT GDT={GDT}
                                 onCardHandler={onCardHandler}
                                 onSlideHandler={onSlideHandler}
                        />
                      </motion.div>
                    </Grid>
                  ))}
                </>
              )}
            </Grid>
          </AnimatePresence>

        </Stack>
        <TablePagination
          component="div"
          count={GDTs?.totalDocs || 0}
          page={page}
          onPageChange={handleChangePage}
          rowsPerPage={itemsPerPage}
          onRowsPerPageChange={handleChangeItemsPerPage}
          labelRowsPerPage={'Кол-во :'}
          rowsPerPageOptions={[12, 36, 60]}
        />
      </Container>
      <ModalUI
        maxWidth={currentModalContent.type === 'image' ? 1000 : 850}
        open={isModalOpen}
        onClose={closeModal}
        style={{padding: 0}}
        maxHeight={'80vh'}
        variant={'image'}

      >
        {currentModalContent.type === 'image' && (
          <Stack position={'relative'}>
            <CardMedia
              component={'img'}
              height={'100%'}
              sx={{maxHeight: 800, objectFit: 'cover'}}
              alt={'pic'}
              src={currentModalContent.src}
            />
          </Stack>
        )}
        {currentModalContent.type === 'info' && (
          <InfoGDT GDT={currentModalContent.data}/>
        )}
      </ModalUI>
    </Stack>
  );
};


export default CatalogGDT;

