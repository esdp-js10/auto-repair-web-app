import React, {useState} from 'react';
import {Carousel} from "react-responsive-carousel";
import {Box, Card, CardContent, CardMedia, Grid, Typography} from "@mui/material";
import no_img from "../../../../assets/images/mainPagePictures/no_img.png";
import {makeStyles, styled} from "@mui/styles";
import {BASE_URL} from "../../../../config";
import {useDispatch, useSelector} from "react-redux";
import {ADMIN_ROLE} from "../../../../constants/userRoles";
import {fetchGDTRequest} from "../../../../store/actions/GDTsActions";
import Tabs, {tabsClasses} from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import ReadMoreOutlinedIcon from '@mui/icons-material/ReadMoreOutlined';
import ModeEditTwoToneIcon from '@mui/icons-material/ModeEditTwoTone';
import IconButton from "@mui/material/IconButton";
import CardTable from "../CardTable/CardTable";

const CatalogCardTabs = styled(Tabs)(({theme}) => ({
  '& .MuiTabs-indicator': {
    backgroundColor: theme.palette.primary.light,
  },
  '& 	.MuiTab-root': {
    color: theme.palette.primary.light,
  },
}))

const CardGDT = ({GDT, onSlideHandler, onCardHandler}) => {
  const s = useStyles();
  const dispatch = useDispatch();
  const fetchHandler = () => {
    dispatch(fetchGDTRequest(GDT._id));
  }
  const [preparedGDT] = useState(() => {
    const resData = {...GDT, cars: {}};
    GDT.cars.forEach(item => {
      if (!resData.cars[item.brandAuto]) {
        resData.cars[item.brandAuto] = [];
      }
      resData.cars[item.brandAuto].push(item)
    })
    return resData;
  })
  const [tabs] = useState(() => Object.keys(preparedGDT?.cars))
  const [tabVal, setTabVal] = useState(0);
  const [tableRows, setTableRows] = useState(preparedGDT.cars[tabs[tabVal]]);

  const onTabChange = (event, newValue) => {
    setTableRows(preparedGDT.cars[tabs[newValue]]);
    setTabVal(newValue);
  };


  const profile = useSelector(state => state.users.profile);
  return (
    <Card className={s.cardWrapper} sx={{minWidth: 300, height: 550, position: 'relative'}}>
      <CardContent>
        <Grid container sx={{display: "flex", justifyContent: "center", alignItems: "center", whiteSpace: 'nowrap'}}
              textAlign={"center"}>
          <Grid item xs={6}>
            <Typography variant="subtitle1" color="text.secondary"
                        sx={{overflow: 'hidden', textOverflow: 'ellipsis'}}>
              Гидротрансформатор
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h5" textAlign={"center"} ml={1} sx={{overflow: 'hidden', textOverflow: 'ellipsis'}}>
              {GDT.serialNo}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
      <Carousel autoplay
                showStatus={false}
                showThumbs={false}
                showIndicators={false}
                onClickItem={onSlideHandler}
      >
        {(GDT?.images?.length > 0) ? (
          GDT.images.map((img, i) => (
            <CardMedia
              component={'img'}
              height={220}
              key={GDT._id + i + 'image'}
              src={BASE_URL + '/' + img}
              sx={{objectFit: 'contain'}}
              alt="pic"
            />
          ))
        ) : (
          <CardMedia
            component={'img'}
            height={180}
            src={no_img}
            sx={{objectFit: 'contain'}}
            alt="pic"
          />
        )}
      </Carousel>
      <Box sx={{
        backgroundColor: 'rgba(232,232,232,0.2)',
        borderTop: '1px solid gainsboro',
        borderBottom: '1px solid gainsboro'
      }}>
        <CatalogCardTabs
          value={tabVal}
          onChange={onTabChange}
          variant="scrollable"
          scrollButtons
          sx={{
            [`& .${tabsClasses.scrollButtons}`]: {
              '&.Mui-disabled': {opacity: 0.3},
            },
          }}
        >
          {GDT.cars.length > 0 && (
            tabs.map((brand, i) => <Tab sx={{minWidth: '50%'}} key={GDT._id + brand + i} label={brand}/>)
          )}
        </CatalogCardTabs>
      </Box>
      <CardTable height={210} rows={tableRows}/>
      {profile?.role === ADMIN_ROLE && (
        <IconButton
          sx={{position: 'absolute', right: 0, bottom: 45}}
          color={'info'}
          size={"small"}
          onClick={fetchHandler}
        >
          <ModeEditTwoToneIcon/>
        </IconButton>
      )}
      <IconButton
        sx={{position: 'absolute', right: 5, bottom: 5}}
        color={'info'}
        size={"small"}
        onClick={(e) => {
          onCardHandler(e, GDT)
        }}
      >
        <ReadMoreOutlinedIcon/>
      </IconButton>
    </Card>
  );
};


const useStyles = makeStyles(() => ({
  cardWrapper: {
    minWidth: 275,
    '& .slide': {
      cursor: 'pointer',
    },
    '& .MuiCardContent-root': {
      padding: '15px 10px',
    },
    '& .MuiCardActions-root': {
      justifyContent: 'right',
      paddingTop: 0,
    },
  }
}))


export default CardGDT;