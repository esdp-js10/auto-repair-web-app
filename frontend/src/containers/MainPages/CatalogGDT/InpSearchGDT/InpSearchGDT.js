import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

const InpSearchGDT = ({searchOptions, setSelectedOptions}) => {
  const [value, setValue] = React.useState(null);
  const [inputValue, setInputValue] = React.useState('');

  const onAutocompleteChange = (event, newValue, reason) => {
    setValue(null);
    setSelectedOptions(prev => {
      if (!newValue) return prev;
      const uniqueOptions = [...new Map([...prev, newValue].map(option => [option['value'], option])).values()]
      if (uniqueOptions.length > 9) {
        uniqueOptions.shift();
      }
      return uniqueOptions;
    });
    if (reason === 'selectOption') {
      setInputValue('')
    }
  }

  const onAutocompleteInpChange = (event, newInputValue, reason) => {
    if (reason === 'reset') {
      setInputValue('')
    } else {
      setInputValue(newInputValue)
    }
  }

  return (
    <Autocomplete
      value={value}
      onChange={onAutocompleteChange}
      inputValue={inputValue}
      onInputChange={onAutocompleteInpChange}
      options={searchOptions}
      groupBy={(option) => option?.group}
      getOptionLabel={(option) => option?.value}
      renderInput={(params) => <TextField {...params} label="Введите фильтр поиска"/>}
    />
  );
};

export default InpSearchGDT;