import React from 'react';
import {Grid, Stack, TableCell, Typography} from "@mui/material";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import ConfirmationNumberIcon from '@mui/icons-material/ConfirmationNumber';
import CommentIcon from '@mui/icons-material/Comment';
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles(theme => ({
  text: {
    paddingLeft: '10px',
    paddingRight: '10px'
  },
  typography: {
    paddingTop: '20px',
  }
}));

const InfoGDT = ({GDT}) => {
  const classes = useStyles();

  return (
    <Stack pb={2} pt={4} px={1} spacing={2}
           position={'relative'}
    >
      <TableContainer sx={{maxHeight: '75vh'}}>
        <Table sx={{minWidth: 500}} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="right">Бренд</TableCell>
              <TableCell align="right">Объем</TableCell>
              <TableCell align="right">Модель</TableCell>
              <TableCell align="right">Год</TableCell>
              <TableCell align="right">Коробка</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {GDT.cars?.map((car) => (
              <TableRow
                key={car._id}
                sx={{'&:last-child td, &:last-child th': {border: 0}}}
              >

                <TableCell component="th" scope="row">
                  {car.brandAuto}
                </TableCell>
                <TableCell align="right">{car.engineVolume}</TableCell>
                <TableCell align="right">{car.model}</TableCell>
                <TableCell align="right">{car.year.start} - {car.year.end}</TableCell>
                <TableCell align="right">{car.transmission}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Typography variant={'body1'} display={'flex'} alignItems={"center"} className={classes.typography}>
        <ConfirmationNumberIcon/>
        <strong className={classes.text}>Серийный номер: </strong>{GDT.serialNo}
      </Typography>
      <Grid container>
        <Grid item>
          <Typography variant={'body1'} display={'flex'} alignItems={"center"} className={classes.typography}>
            <CommentIcon/>
            <strong className={classes.text}>Описание: </strong>
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant={'body1'} display={'flex'} alignItems={"center"} className={classes.typography}>
            {GDT.description}
          </Typography>
        </Grid>
      </Grid>
    </Stack>
  );
};

export default InfoGDT;