import React, {useEffect, useState} from 'react';
import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Grid,
  MenuItem,
  TextField,
  Typography
} from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import {useStyles} from "./RepairPage.styles";
import {useDispatch, useSelector} from "react-redux";
import {fetchTypesDetailsRequest} from "../../../store/actions/typesDetailsActions";
import {useParams} from "react-router-dom";
import {changeOrderStatus, fetchOrderRequest} from "../../../store/actions/ordersActions";
import {fetchPartNumbersDetailsRequest} from "../../../store/actions/partNumbersDetailsActions";
import {
  addExtras,
  addNewReservedItem,
  changeExtras,
  changeReservedItem,
  fetchAvailableParts,
  fetchRepairOrderData,
  removeExtras,
  removeReservedItem,
  submitItems,
  cleanRepairPageFormState, changeExtraStatus,
} from "../../../store/actions/repairerOrdersActions";

const statuses = [
  //{_id: 2, title: 'Новый'},
  {_id: 3, title: 'Принято'},
  {_id: 4, title: 'Выполнено'},
  {_id: 5, title: 'Отклонено'}
];

const RepairPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {id} = useParams();

  const {order, fetchOrderLoading} = useSelector(state => state.orders);
  const availableParts = useSelector(state => state.repairerOrders.availableParts);
  const availableUniqueTypes = useSelector(state => state.repairerOrders.availableUniqueTypes);
  const reservedDetails = useSelector(state => state.repairerOrders.reservedDetails);
  const extraDetails = useSelector(state => state.repairerOrders.extraDetails);
  const extraWorks = useSelector(state => state.repairerOrders.extraWorks);

  const [comment, setComment] = useState('');
  const checkedDetail = useSelector(state => state.repairerOrders.checkedDetail);
  const checkedWork = useSelector(state => state.repairerOrders.checkedWork);

  const error = useSelector(state => state.repairerOrders.error);
  /** Todo: RepairPage tasks
   *  5) send notification to user after changing status of work
   *  7) change 'completedPrice' and 'quantity' in DetailIntheWarehouse after order is completed.
   *  */

  useEffect(() => {
    dispatch(fetchOrderRequest(id));
    dispatch(fetchPartNumbersDetailsRequest());
    dispatch(fetchTypesDetailsRequest());
    dispatch(fetchAvailableParts(id));
    dispatch(fetchRepairOrderData(id));
    return () => {
      dispatch(cleanRepairPageFormState());
    }
  }, [dispatch, id]);

  const handleCommentField = e => {
    setComment(e.target.value);
  }

  const handleFieldChange = e => {
    dispatch(changeOrderStatus(e.target.value));
  };

  const add = field => {
    dispatch(addExtras({field}));
  };

  const remove = (field, indexOfItem) => {
    dispatch(removeExtras({field, indexOfItem}));
  }

  const detailsChangeHandler = (changingCurrentIndex, name, value, field) => {
    dispatch(changeExtras({changingCurrentIndex, name, value, field}));
  };

  const reservedDetailsChangeHandler = (changingCurrentIndex, nameOfField, valueToChange) => {
    dispatch(changeReservedItem({
      changingCurrentIndex,
      nameOfField,
      valueToChange
    }));
  };

  const addDetailItem = () => {
    dispatch(addNewReservedItem());
  };

  const removeDetailItem = (itemIndex) => {
    dispatch(removeReservedItem(itemIndex));
  };

  const setChecked = e => {
    const {name, checked} = e.target;
    dispatch(changeExtraStatus({
      name,
      checked,
    }));
  }

  const onSubmit = e => {
    e.preventDefault();
    dispatch(submitItems({
      order: id,
      parts: reservedDetails,
      comment,
      extraWorks,
      extraDetails,
      status: order.status,
    }));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }

  return (
    <div className="container">
      {fetchOrderLoading ?
        <Grid container justifyContent="center" alignItems="center">
          <Grid item>
            <CircularProgress/>
          </Grid>
        </Grid>
        :
        <Grid
          container
          direction="column"
          spacing={3}
          component="form"
          onSubmit={onSubmit}
          noValidate
        >
          <Grid item xs={12} mt={3}>
            <Typography variant="h5" fullwidth="true" textAlign="left">Заказ № {order?.orderNumber}</Typography>
          </Grid>
          <Grid item xs={12}>
            {reservedDetails.map((reservedDetail, i) => (
              <Grid container
                    key={i}
                    flexdirection="row"
                    justifyContent="start"
                    alignItems="center"
                    spacing={2}
                    mb={2}>
                <Grid item xs={12} md={4} lg={4}>
                  <TextField
                    select
                    required
                    fullWidth
                    label="Тип"
                    name="type"
                    color={'action1'}
                    value={reservedDetail.type}
                    onChange={e => reservedDetailsChangeHandler(i, 'type', e.target.value)}
                    error={Boolean(getFieldError(`reserved.${i}.type`))}
                    helperText={getFieldError(`reserved.${i}.type`)}
                  >
                    {availableUniqueTypes.map((option) => (
                      <MenuItem key={option._id} value={option._id}>
                        {option.type}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
                <Grid item xs={12} md={4} lg={3}>
                  <TextField
                    select
                    required
                    fullWidth
                    label="Парт номер"
                    name="partNumber"
                    color={'action1'}
                    value={reservedDetail.partNumber}
                    onChange={e => reservedDetailsChangeHandler(i, 'partNumber', e.target.value)}
                    error={Boolean(getFieldError(`reserved.${i}.partNumber`))}
                    helperText={getFieldError(`reserved.${i}.partNumber`)}
                  >

                    {(reservedDetail.type !== '') &&
                    availableParts
                      .filter(partNumber => partNumber.type._id === reservedDetail.type)
                      .map((option) => (
                        <MenuItem key={option.partNumber._id} value={option.partNumber._id}>
                          {option.partNumber.partNumber}
                        </MenuItem>
                      ))}
                  </TextField>
                </Grid>
                <Grid item xs={12} md={2} lg={3}>
                  <TextField
                    type="number"
                    required
                    fullWidth
                    label="Количество"
                    name="reservedQuantity"
                    value={reservedDetail.reservedQuantity}
                    onChange={e => reservedDetailsChangeHandler(i, 'reservedQuantity', e.target.value)}
                    error={Boolean(getFieldError(`reserved.${i}.reservedQuantity`))}
                    helperText={getFieldError(`reserved.${i}.reservedQuantity`)}
                  />
                </Grid>
                <Grid item xs={6} md={1} lg={1}>
                  <Button
                    className={classes.btn}
                    variant="outlined"
                    fullWidth
                    onClick={addDetailItem}
                  >
                    <AddCircleOutlineIcon fontSize="large"/>
                  </Button>
                </Grid>
                <Grid item xs={6} md={1} lg={1}>
                  <Button
                    className={classes.btn}
                    variant="outlined"
                    fullWidth
                    onClick={() => removeDetailItem(i)}
                  >
                    <DeleteOutlineIcon fontSize="large"/>
                  </Button>
                </Grid>
              </Grid>
            ))}
          </Grid>

          <Grid item xs={12}>
            <FormControlLabel
              label="Дополнительные детали"
              control={<Checkbox
                checked={checkedDetail}
                name="checkedDetail"
                onChange={e => setChecked(e)}
                inputProps={{'aria-label': 'controlled'}}
              />}
            />
            {checkedDetail && extraDetails.map((d, i) => (
              <Grid key={i} container flexdirection="row" spacing={2} mt={1} justifyContent="start" item xs={12}>
                <Grid item xs={12} md={8}>
                  <TextField
                    fullWidth
                    name="title"
                    label="Дополнительные детали"
                    value={d.title}
                    onChange={e => detailsChangeHandler(i, 'title', e.target.value, 'extraDetails')}
                  />
                </Grid>
                <Grid item xs={12} md={2} pl={1}>
                  <TextField
                    name="price"
                    label="Цена"
                    value={d.price}
                    onChange={e => detailsChangeHandler(i, 'price', e.target.value, 'extraDetails')}
                  />
                </Grid>
                <Button onClick={() => add('extraDetails')}>
                  <AddCircleOutlineIcon color="action" fontSize="large"/>
                </Button>
                <Button onClick={() => remove('extraDetails', i)}>
                  <DeleteOutlineIcon fontSize="large"/>
                </Button>
              </Grid>
            ))}
          </Grid>

          <Grid item xs={12}>
            <FormControlLabel
              label="Дополнительные работы"
              control={<Checkbox
                checked={checkedWork}
                name="checkedWork"
                onChange={e => setChecked(e)}
                inputProps={{'aria-label': 'controlled'}}
              />}
            />
            {checkedWork && extraWorks.map((d, i) => (
              <Grid key={i} container flexdirection="row" mt={1} spacing={2} justifyContent="start" item xs={12}>
                <Grid item md={8} xs={12}>
                  <TextField
                    fullWidth
                    name="title"
                    label="Дополнительные работы"
                    value={d.title}
                    onChange={e => detailsChangeHandler(i, 'title', e.target.value, 'extraWorks')}
                  />
                </Grid>
                <Grid item md={2} xs={12} pl={1}>
                  <TextField
                    name="price"
                    label="Цена"
                    value={d.price}
                    onChange={e => detailsChangeHandler(i, 'price', e.target.value, 'extraWorks')}
                  />
                </Grid>
                <Button onClick={() => add('extraWorks')}>
                  <AddCircleOutlineIcon color="action" fontSize="large"/>
                </Button>
                <Button onClick={() => remove('extraWorks', i)}>
                  <DeleteOutlineIcon fontSize="large"/>
                </Button>
              </Grid>
            ))}
          </Grid>

          <Grid item lg={6} md={4} sm={12} xs={4}>
            <TextField
              fullWidth
              multiline
              rows={5}
              type="text"
              label="Комментарий"
              name="comment"
              value={comment}
              onChange={handleCommentField}
            />
          </Grid>

          <Grid item xs={12}>
            <Grid container flexdirection="row" spacing={1} justifyContent="start" item xs={12}>
              <Grid item xs={12} md={4}>
                <TextField
                  select
                  label="Статус"
                  name="status"
                  fullWidth
                  color={'action1'}
                  value={order?.status}
                  onChange={handleFieldChange}
                >
                  {statuses.map((option, i) => (
                    <MenuItem key={i} value={option.title}>
                      {option.title}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container flexdirection="row" mt={3} justifyContent="start" item xs={12}>
              <Grid item xs={4}>
                <Button
                  fullWidth
                  variant="contained"
                  color="secondary"
                  type="submit"
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      }
    </div>
  );
};

export default RepairPage;