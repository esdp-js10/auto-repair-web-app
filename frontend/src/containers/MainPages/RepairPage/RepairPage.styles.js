const {makeStyles} = require("@mui/styles");

export const useStyles = makeStyles({
  btn: {
    display: 'block',
  },
});