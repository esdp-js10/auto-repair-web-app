import React, {useEffect} from 'react';
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {fetchWarehousesByStatus} from "../../../../store/actions/warehousesActions";
import {fetchWorkshopsRequest} from "../../../../store/actions/workshopActions";
import {fetchEmployeesByRole} from "../../../../store/actions/employeesActions";

const Warehouses = () => {
	const dispatch = useDispatch();
	const activeWarehouses = useSelector(state => state.warehouses.fetchWarehousesByStatus);
	const workshops = useSelector(state => state.workshops.workshops);
	const allWarehouseman = useSelector(state => state.employees.employeesByRole);

	useEffect(() => {
		dispatch(fetchWarehousesByStatus('active'));
		dispatch(fetchWorkshopsRequest());
		dispatch(fetchEmployeesByRole('warehouseman'));
	}, [dispatch]);

	return (
		<div className="container" style={{marginTop: '25px'}}>
			<TableContainer component={Paper}>
				<Table aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell>Название</TableCell>
							<TableCell align="right">Мастерская</TableCell>
							<TableCell align="right">Складовщики</TableCell>
						</TableRow>
					</TableHead>
						{activeWarehouses?.map(warehouse => (
							<TableBody key={warehouse.name}>
								<TableRow sx={{'&:last-child td, &:last-child th': {border: 0}}}>
									<TableCell component="th" scope="row">
										{warehouse.name}
									</TableCell>
									<TableCell align="right">
										{workshops?.map(workshop => {
											if (workshop.warehouse._id === warehouse._id) {
												return workshop.name
											}
											return '';
										})}
									</TableCell>
									<TableCell align="right">
										{allWarehouseman?.map(warehouseman => {
											if (warehouseman.warehouse === warehouse._id) {
												return warehouseman.name
											}

											return '';
										})}
									</TableCell>
								</TableRow>
							 </TableBody>
							))}
				</Table>
			</TableContainer>
		</div>
	);
};

export default Warehouses;