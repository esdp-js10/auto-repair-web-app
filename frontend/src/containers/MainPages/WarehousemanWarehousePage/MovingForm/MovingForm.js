import React, {useState} from 'react';
import {Grid, TextField, Typography} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import {useDispatch, useSelector} from "react-redux";
import {movingDetails} from "../../../../store/actions/detailsActions";
import ButtonWithProgress from "../../../../components/UI/ButtonWithProgress/ButtonWithProgress";

const MovingForm = ({checkedDetails, warehouses, fromWarehouse, setOpenPopup, setSelectionModel, setSelectedRows}) => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.details.movingDetailsLoading);

  const selectedDetails = checkedDetails
    .map(({partNumber, quantity}) => ({partNumber, quantity}))
    .map(d => {
      return {...d, sendQuantity: '', error: false, errorText: ''}
    });
  const [details, setDetails] = useState(selectedDetails);
  const [warehouseError, setWarehouseError] = useState(false);

  const [state, setState] = useState({
    details: '',
    fromWarehouse: fromWarehouse,
    toWarehouse: '',
  });

  const successHandler = () => {
    setOpenPopup(false);
    setSelectedRows([]);
    setSelectionModel([]);
  };

  const changeDetails = (i, name, value) => {
    setDetails(prevState => {
      const detailCopy = {
        ...prevState[i],
        [name]: value,
      };

      return prevState.map((detail, index) => {
        if (i === index) {
          return detailCopy;
        }

        return detail;
      });
    });
  };

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setState(prevState => ({...prevState, [name]: value}));
  };

  const submitFormHandler = e => {
    e.preventDefault();
    setWarehouseError(false);

    setDetails(prevState => {
      return prevState.map(d => ({
        ...d,
        error: false,
        errorText: '',
      }));
    });

    if (!state.toWarehouse) {
      return setWarehouseError(true);
    }

    let hasError = false;

    details.forEach((detail, i) => {
      if (Number(detail.sendQuantity) <= 0) {
        hasError = true;

        setDetails(prevState => {
          const detailCopy = {
            ...prevState[i],
            error: true,
            errorText: 'Введенное число должно быть больше нуля'
          };

          return prevState.map((detail, index) => {
            if (i === index) {
              return detailCopy;
            }

            return detail;
          });
        });
      }

      if (Number(detail.sendQuantity) > Number(detail.quantity)) {
        hasError = true;

        setDetails(prevState => {
          const detailCopy = {
            ...prevState[i],
            error: true,
            errorText: `Максимально допустимое количество ${detail.quantity}`
          };

          return prevState.map((detail, index) => {
            if (i === index) {
              return detailCopy;
            }

            return detail;
          });
        });
      }
    });

    const sendDetailData = details.map(detail => {
      return {
        partNumber: detail.partNumber._id,
        quantity: detail.sendQuantity,
      }
    });

    const newState = {};

    Object.keys(state).forEach(key => {
      if (key === 'details') {
        return newState[`${key}`] = JSON.stringify(sendDetailData);
      }
      newState[`${key}`] = state[key];
    });

    if (hasError === false) {
      dispatch(movingDetails({
        data: newState,
        successCallback: successHandler,
      }));
    }
  };

  return (
    <Grid
      container
      component="form"
      onSubmit={submitFormHandler}
      spacing={2}
      noValidate
    >
      {details.map((detail, i) => (
        <Grid item container key={i} spacing={2} mb={2} justifyContent="start" alignItems="center">
          <Grid item xs={5}>
            <Typography variant="body1">Парт номер: {details[i].partNumber.partNumber}</Typography>
          </Grid>
          <Grid item xs={5}>
            <TextField
              type="number"
              required
              fullWidth
              label="Количество"
              name="sendQuantity"
              value={details[i].sendQuantity}
              onChange={e => changeDetails(i, 'sendQuantity', e.target.value)}
              error={details[i].error}
              helperText={details[i].error === false ? '' : details[i].errorText}
            />
          </Grid>
        </Grid>
      ))}
      <Grid item xs={10}>
        <TextField
          type="text"
          select
          fullWidth
          label="Склад-получатель"
          name="toWarehouse"
          value={state.toWarehouse}
          onChange={inputChangeHandler}
          error={warehouseError}
          helperText={warehouseError === false ? '' : 'Это обязательное поле'}
        >
          {warehouses.filter(w => w._id !== fromWarehouse).map(warehouse => (
            <MenuItem
              key={warehouse._id}
              value={warehouse._id}
            >
              {warehouse.name}
            </MenuItem>
          ))}
        </TextField>
      </Grid>
      <Grid item xs={12} mt={4}>
        <ButtonWithProgress
          type="submit"
          variant="contained"
          style={{float: 'right'}}
          loading={loading}
          disabled={loading}
        >
          Потвердить
        </ButtonWithProgress>
      </Grid>
    </Grid>
  );
};

export default MovingForm;