import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMigrationHistory} from "../../../../store/actions/detailsActions";
import {Box, CircularProgress, Divider, Grid, List, ListItem, ListItemText, Typography} from "@mui/material";
import {StyledDataGrid} from "../WarehousemanWarehousePage.styles";
import {CustomPagination} from "../WarehousemanWarehousePage";
import MUIDatePicker from "../DatePicker/DatePicker";
import moment from "moment";

const MigrationHistory = () => {
	const dispatch = useDispatch();
	const data = useSelector(state => state.details.migrationHistory);
	const loading = useSelector(state => state.details.migrationHistoryLoading);
	const [dateValue, setDateValue] = useState([null, null]);

	useEffect(() => {
		dispatch(fetchMigrationHistory());
	}, [dispatch]);

	const columns = [
		{ field: 'date',
			headerName: 'Дата',
			sortable: false,
			minWidth: 100,
			valueGetter: params => {return params.row.date.split('T')[0]},
		},
		{ field: 'fromWarehouse',
			headerName: 'Склад-получатель',
			sortable: false,
			minWidth: 160,
			valueGetter: params => {return params.row.toWarehouse.name},
		},
		{ field: 'toWarehouse',
			headerName: 'Склад-отправитель',
			sortable: false,
			minWidth: 160,
			valueGetter: params => {return params.row.fromWarehouse.name},
		},
		{ field: 'detailType',
			headerName: 'Тип',
			sortable: false,
			minWidth: 140,
			renderCell: params => (
				<List sx={{width: '100%'}} component='nav' aria-label="mailbox folders">
					<Divider/>
					{params.row.details.map(d => (
						<ListItem key={d.partNumber._id} divider>
							<ListItemText>{d.partNumber.type.type}</ListItemText>
						</ListItem>
					))}
				</List>
			)
		},
		{ field: 'detailPartNumber',
			headerName: 'Парт номер',
			sortable: false,
			minWidth: 140,
			renderCell: params => (
				<List sx={{width: '100%'}} component='nav' aria-label="mailbox folders">
					<Divider/>
					{params.row.details.map(d => (
						<ListItem key={d.partNumber._id} divider>
							<ListItemText>{d.partNumber.partNumber}</ListItemText>
						</ListItem>
					))}
				</List>
			)
		},
		{ field: 'detailQuantity',
			headerName: 'Количество',
			sortable: false,
			minWidth: 110,
			renderCell: params => (
				<List sx={{width: '100%'}} component='nav' aria-label="mailbox folders">
					<Divider/>
					{params.row.details.map(d => (
						<ListItem key={d.partNumber._id} divider>
							<ListItemText>{d.quantity}</ListItemText>
						</ListItem>
					))}
				</List>
			)
		},
		{ field: 'employee',
			headerName: 'Сотрудник операционист',
			sortable: false,
			minWidth: 200,
			renderCell: params => (
				<div>
					<p>{params.row.employee.name}</p>
					<Typography color="textSecondary" variant="body2">{params.row.employee.email}</Typography>
				</div>
			),
		},
	];

	return (
		<div className="container">
			<MUIDatePicker dateValue={dateValue} setDateValue={setDateValue}/>
			{loading ?
				<Grid container justifyContent="center" alignItems="center" mt={2}>
					<Grid item>
						<CircularProgress/>
					</Grid>
				</Grid> :
				<Box sx={{ height: 580, width: '100%', marginTop: '25px'}}>
					<StyledDataGrid
						pageSize={4}
						rowsPerPageOptions={[4]}
						components={{
							Pagination: CustomPagination,
						}}
						rows={data.filter(d => {
							if (dateValue[0] !== null && dateValue[1] !== null) {
								return d.date >= moment(dateValue[0]).format() && d.date <= moment(dateValue[1]).format();
							}

							return d;
						})}
						getRowId={rows => rows._id}
						columns={columns}
						disableColumnMenu
						hideFooterSelectedRowCount={true}
					/>
				</Box>
			}
		</div>
	);
};

export default MigrationHistory;