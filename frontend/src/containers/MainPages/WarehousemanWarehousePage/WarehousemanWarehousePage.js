import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link, useParams} from "react-router-dom";
import {fetchWarehousesRequest} from "../../../store/actions/warehousesActions";
import {fetchDetailsInTheWarehouse} from "../../../store/actions/detailsActions";
import {
	Autocomplete,
	Box,
	Button,
	CircularProgress,
	Divider,
	Grid,
	Pagination,
	PaginationItem,
	TextField,
	Typography
} from "@mui/material";
import {gridPageCountSelector, gridPageSelector, useGridApiContext, useGridSelector} from "@mui/x-data-grid";
import Popup from "../../../components/UI/Popup/Popup";
import MovingForm from "./MovingForm/MovingForm";
import {StyledDataGrid, useStyles} from "./WarehousemanWarehousePage.styles";
import MenuItem from "@mui/material/MenuItem";
import {fetchTypesDetailsRequest} from "../../../store/actions/typesDetailsActions";
import {fetchPartNumbersDetailsRequest} from "../../../store/actions/partNumbersDetailsActions";
import {W_WAREHOUSES_INCOME_PATH} from "../../../constants/paths";
import {FiFilter} from "react-icons/fi";

export const CustomPagination = () => {
	const apiRef = useGridApiContext();
	const page = useGridSelector(apiRef, gridPageSelector);
	const pageCount = useGridSelector(apiRef, gridPageCountSelector);

	return (
		<Pagination
			color="primary"
			variant="outlined"
			shape="rounded"
			page={page + 1}
			count={pageCount}
			renderItem={(props2) => <PaginationItem {...props2} disableRipple />}
			onChange={(event, value) => apiRef.current.setPage(value - 1)}
		/>
	);
};

const WarehousemanWarehousePage = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const params = useParams();
	const employee = useSelector(state => state.users.profile);
	const myDetails = useSelector(state => state.details.myDetailsInTheWarehouse);
	const fetchDetailsLoading = useSelector(state => state.details.fetchDetailsInTheWarehouseLoading);
	const fetchDetailsError = useSelector(state => state.details.fetchDetailsInTheWarehouseError);
	const warehouses = useSelector(state => state.warehouses.warehouses);
	const [selectedRows, setSelectedRows] = useState([]);
	const [openPopup, setOpenPopup] = useState(false);
	const [selectionModel, setSelectionModel] = useState([]);
	const types = useSelector(state => state.typesDetails.types);
	const partNumbers = useSelector(state => state.partNumbersDetails.partNumbers);
	const [filter, setFilter] = useState({
		type: '',
		partNumber: '',
	});

	useEffect(() => {
		if (employee?.role === 'warehouseman' || employee?.role === 'admin') {
			if (params.id) {
				dispatch(fetchDetailsInTheWarehouse(params.id));
			}
			dispatch(fetchWarehousesRequest());
			dispatch(fetchTypesDetailsRequest());
			dispatch(fetchPartNumbersDetailsRequest());
		}
	}, [dispatch, employee, params]);

	const columns = [
		{ field: 'type',
			headerName: 'Тип',
			sortable: false,
			flex: 1,
			maxWidth: 140,
			minWidth: 80,
			valueGetter: params => {return params.row.type.type},
		},
		{ field: 'partNo',
			headerName: 'Парт номер',
			sortable: false,
			flex: 1,
			maxWidth: 160,
			minWidth: 110,
			valueGetter: params => {return params.row.partNumber.partNumber},
		},
		{ field: 'quantity',
			headerName: 'Количество',
			sortable: false,
			flex: 1,
			maxWidth: 160,
			minWidth: 110,
		},
		{ field: 'price',
			headerName: 'Цена',
			sortable: false,
			flex: 1,
			maxWidth: 120,
			minWidth: 70,
			valueGetter: params => {return params.row.completedPrice},
		},
	];

	return (
		<div className="container">
			{fetchDetailsLoading ?
				<Grid container justifyContent="center" alignItems="center">
					<Grid item>
						<CircularProgress/>
					</Grid>
				</Grid> :
			<>
				<Grid container alignItems="center" spacing={2} mt={3} pb={3}>
					<Grid item container alignItems="center" spacing={2} lg={10} md={10} sm={10} xs={12}>
						<Grid item lg={0.5} md={0.5} sm={1} xs={1}>
							<FiFilter className={classes.filterIcon}/>
						</Grid>
						<Grid item lg={3} md={3} sm={3} xs={4.75}>
							<TextField
								type="text"
								select
								fullWidth
								label="Типы"
								name="type"
								value={filter.type}
								onChange={e => setFilter(prev => ({...prev, type: e.target.value}))}
							>
								<MenuItem value=''>Пустое значение</MenuItem>
								{types.map(type => (
									<MenuItem
										key={type._id}
										value={type._id}
									>
										{type.type}
									</MenuItem>
								))}
							</TextField>
						</Grid>
						<Grid item lg={3} md={3} sm={3} xs={4.75}>
							<Autocomplete
								name="partNumber"
								value={filter.type ?
									filter.partNumber ?
										(filter.type !== filter.partNumber.type) ?
											setFilter(prev => ({...prev, partNumber: ''}))
											: filter.partNumber
										: ''
									: filter.partNumber}
								options={filter.type !== '' ? partNumbers?.filter(partNo => partNo.type === filter.type).map(p => p):
									partNumbers?.map(p => p)
								}
								getOptionLabel={option => option.partNumber ? option.partNumber : ''}
								isOptionEqualToValue={(option, value) => {
									if (value === '') {
										return true;
									} else if (value === option) {
										return true;
									}
								}}
							  onChange={(e, selectedObject) => {
							  	if (selectedObject !== null) {
									  setFilter(prev => ({...prev, partNumber: selectedObject}));
								  } else {
									  setFilter(prev => ({...prev, partNumber: ''}));
								  }
							  }}
								renderInput={params =>
									<TextField {...params} label="Парт номер" />
								}
							/>
						</Grid>
					</Grid>
					{((employee?.role === 'warehouseman' && (params.id && (employee.warehouse === params.id))) ||
						employee?.role === 'admin') &&
						<Grid item lg={2} md={2} sm={2} xs={12}>
							<Button
								variant="contained"
								color="success"
								component={Link}
								to={W_WAREHOUSES_INCOME_PATH}
							>
								Приход
							</Button>
						</Grid>
					}
				</Grid>
				<Divider/>
				<Box sx={{ height: 400, width: '100%', margin: '25px 0'}}>
					<StyledDataGrid
						checkboxSelection={((employee?.role === 'warehouseman' && (params.id && (employee.warehouse === params.id)))
							|| employee?.role === 'admin')}
						pageSize={5}
						rowsPerPageOptions={[5]}
						components={{
							Pagination: CustomPagination,
						}}
						rows={myDetails.filter(detail => {
							if (filter.type === '') return detail;
							if (detail.partNumber.type === filter.type) return detail;
							return '';
						}).filter(detail => {
							if (filter.partNumber === '') return detail;
							if (detail.partNumber._id === filter.partNumber._id) return detail;
							return '';
						})}
						getRowId={rows => rows._id}
						columns={columns}
						disableColumnMenu
						hideFooterSelectedRowCount={true}
						onSelectionModelChange={ids => {
							setSelectionModel(ids);
							const selectedIDs = new Set(ids);
							const selectedRows = myDetails.filter(detail =>
								selectedIDs.has(detail._id.toString()),
							);
							setSelectedRows(selectedRows);
						}}
						selectionModel={selectionModel}
					/>
					{((employee?.role === 'warehouseman' && (params.id && (employee.warehouse === params.id))) ||
						employee?.role === 'admin') &&
						<Button
							variant="outlined"
							color="secondary"
							onClick={() => setOpenPopup(true)}
							disabled={(selectedRows.length === 0)}
							className={classes.movingBtn}
						>
							Переместить
						</Button>
					}
					<Popup
						title="Перемещение деталей"
						openPopup={openPopup}
						setOpenPopup={setOpenPopup}
					>
						{employee?.warehouse && (
							<MovingForm
								warehouses={warehouses}
								fromWarehouse={employee.warehouse}
								checkedDetails={selectedRows}
								setOpenPopup={setOpenPopup}
								setSelectedRows={setSelectedRows}
								setSelectionModel={setSelectionModel}
							/>
						)}
					</Popup>
				</Box>
			</>
			}
			{fetchDetailsError && <Typography variant="h5">Доступ запрещен!</Typography>}
		</div>
	);
};

export default WarehousemanWarehousePage;