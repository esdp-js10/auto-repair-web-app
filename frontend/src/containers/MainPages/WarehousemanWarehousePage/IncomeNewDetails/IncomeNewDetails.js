import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTypesDetailsRequest} from "../../../../store/actions/typesDetailsActions";
import {fetchPartNumbersDetailsRequest} from "../../../../store/actions/partNumbersDetailsActions";
import {addDetailRequest} from "../../../../store/actions/detailsActions";
import AccountsDetailForm from "../../../../components/AccountsDetailForm/AccountsDetailForm";

const IncomeNewDetails = () => {
	const dispatch = useDispatch();
	const employee = useSelector(state => state.users.profile);
	const types = useSelector(state => state.typesDetails.types);
	const partNumbers = useSelector(state => state.partNumbersDetails.partNumbers);
	const fetchTypesLoading = useSelector(state => state.typesDetails.fetchTypesDetailsLoading);
	const fetchPartNumbersLoading = useSelector(state => state.partNumbersDetails.fetchPartNumbersDetailsLoading);
	const error = useSelector(state => state.details.addDetailError);
	const loading =  useSelector(state => state.details.addDetailLoading);

	useEffect(() => {
		dispatch(fetchTypesDetailsRequest());
		dispatch(fetchPartNumbersDetailsRequest());
	}, [dispatch]);

	const onSubmit = data => {
		if (employee?.role) {
			dispatch(addDetailRequest({
				id: `?warehouse=${employee.warehouse}`,
				data,
				role: employee.role,
			}));
		}
	};

	return (
		<div className="container">
			<AccountsDetailForm
				typeOfAction={'income'}
				types={types}
				partNumbers={partNumbers}
				fetchTypesLoading={fetchTypesLoading}
				fetchPartNumbersLoading={fetchPartNumbersLoading}
				onSubmit={onSubmit}
				error={error}
				loading={loading}
			/>
		</div>
	);
};

export default IncomeNewDetails;