import React from 'react';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {TextField, Typography} from "@mui/material";
import {DateRangePicker} from "@mui/lab";
import Box from "@mui/material/Box";

const MUIDatePicker = ({dateValue, setDateValue}) => {
	return (
		<Box mt={4} sx={{display: 'flex', alignItems: 'center', justifyContent: 'start'}}>
			<Typography variant="subtitle2" mr={2}>Данные</Typography>
			<LocalizationProvider dateAdapter={AdapterDateFns}>
				<DateRangePicker
					startText="с"
					endText="по"
					value={dateValue}
					onChange={(newValue) => {
						setDateValue(newValue);
					}}
					renderInput={(startProps, endProps) => (
						<React.Fragment>
							<TextField {...startProps} />
							<Box sx={{ mx: 2 }}/>
							<TextField {...endProps} />
						</React.Fragment>
					)}
				/>
			</LocalizationProvider>
		</Box>
	);
};

export default MUIDatePicker;