import React from 'react';
import RegisterForm from "../RegisterForm/RegisterForm";
import {updateUser} from "../../../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";

const EditUserProfile = () => {
	const dispatch = useDispatch();
	const error = useSelector(state => state.users.updateUserError);
	const loading = useSelector(state => state.users.updateUserLoading);
	const user = useSelector(state => state.users.profile);

	const submitHandler = data => {
		dispatch(updateUser({data, id: user._id}));
	};

	return (
		<div className="container">
			<RegisterForm
				handleSubmit={submitHandler}
				errSignUp={error}
				loadingSubmit={loading}
				user={user}
			/>
		</div>
	);
};

export default EditUserProfile;