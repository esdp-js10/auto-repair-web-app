import React from 'react';
import EmployeeForm from "../../../../../components/AdminOffice/EmployeeForm/EmployeeForm";
import {useDispatch, useSelector} from "react-redux";
import {createEmployee} from "../../../../../store/actions/employeesActions";
import {Container, Typography} from "@mui/material";

const AddEmployee = () => {
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.profile);
	const error = useSelector(state => state.employees.createEmployeeError);
	const loading  = useSelector(state => state.employees.createEmployeeLoading);

	const onSubmit = newEmployeeData => {
		dispatch(createEmployee(newEmployeeData));
	};

	return (user?.role === 'admin') ?
		<EmployeeForm onSubmit={onSubmit} error={error} loading={loading}/> :
		<Container style={{textAlign: 'center'}}>
			<Typography component="h1" variant="h4">
				Доступ запрещен! У вас нет прав для доступа.
			</Typography>
		</Container>
};

export default AddEmployee;