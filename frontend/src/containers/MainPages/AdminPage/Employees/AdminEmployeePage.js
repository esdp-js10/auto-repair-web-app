import React, {useEffect, useState} from 'react';
import {Button} from "@mui/material";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchEmployees} from "../../../../store/actions/employeesActions";
import Box from "@mui/material/Box";
import EmployeeTable from "../../../../components/AdminOffice/EmployeeTable/EmployeeTable";
import EmployeeFilters from "../../../../components/AdminOffice/EmployeeFilters/EmployeeFilters";
import {fetchWorkshopsRequest} from "../../../../store/actions/workshopActions";

const AdminEmployeePage = () => {
	const dispatch = useDispatch();
	const employees = useSelector(state => state.employees.employees);
	const loading = useSelector(state => state.employees.fetchEmployeesLoading);
	const workshops = useSelector(state => state.workshops.workshops);
	const [search, setSearch] = useState('');
	const [sortByRole, setSortByRole] = useState('');
	const [sortByStatus, setSortByStatus] = useState('');
	const [sortByWorkshop, setSortByWorkshop] = useState('');

	useEffect(() => {
		dispatch(fetchEmployees());
		dispatch(fetchWorkshopsRequest());
	}, [dispatch]);

	return (
		<div className="container">
			<Box pt={3} mb={3} style={{textAlign: 'end'}}>
				<Button
					component={Link}
					to={'/admin/employees/add'}
					variant="contained"
					style={{backgroundColor: "#4355FB"}}
				>
					Создать
				</Button>
			</Box>
			<EmployeeFilters
				search={search}
				setSearch={setSearch}
				sortByRole={sortByRole}
				setSortByRole={setSortByRole}
				sortByStatus={sortByStatus}
				setSortByStatus={setSortByStatus}
				workshops={workshops}
				sortByWorkshop={sortByWorkshop}
				setSortByWorkshop={setSortByWorkshop}
			/>
			<EmployeeTable
				loading={loading}
				employees={employees}
				search={search}
				sortByRole={sortByRole}
				sortByStatus={sortByStatus}
				sortByWorkshop={sortByWorkshop}
			/>
		</div>
	);
};

export default AdminEmployeePage;