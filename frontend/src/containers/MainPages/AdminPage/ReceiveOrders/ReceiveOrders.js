import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, MenuItem, TextField, Typography} from "@mui/material";
import {styled} from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {makeStyles} from "@mui/styles";
import {Link} from "react-router-dom";
import moment from 'moment';
import {useDispatch, useSelector} from "react-redux";
import {changeStatusRequest, fetchOrdersRequest, fetchOrderRequest, fetchOrderSuccess} from "../../../../store/actions/ordersActions";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import c from './ReceiveOrder.module.css';
import ModalUI from "../../../../components/UI/ModalUI/ModalUI";

const statuses = [
  {_id: 1, title: 'Все', text: 'all'},
  {_id: 2, title: 'Новый', text: 'new'},
  {_id: 3, title: 'Принято', text: 'submitted'},
  {_id: 4, title: 'Выполнено', text: 'done'},
  {_id: 5, title: 'Отклонено', text: 'rejected'}
];

const days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']
const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];


const useStyles = makeStyles(() => ({
  orderPageContainer: {
    margin: '40px auto 50px',
  },
  label: {
    textAlign: 'center',
    fontFamily: '\'Montserrat\', sans-serif',
    marginTop: '30px'
  },
  orderTable: {
    marginBottom: '50px'
  },
  modalComment: {
    boxShadow: '0 0 0 1px black',
    padding: '4px 4px',
    borderRadius: '1px',
    margin: '15px 0 0',
  }
}))

const ReceiveOrders = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {profile} = useSelector(state => state.users);
  const {orders, fetchOrdersLoading, order, fetchOrderLoading} = useSelector(state => state.orders);
  const [open, setOpen] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [state, setState] = useState({
    status: "Все"
  });

  let allOrders = orders;

  if (state.status !== "Все") {
    allOrders = orders.filter(o => o.status === state.status);
  } else if (moment(startDate).format('DD/MM/yyyy') !== moment(new Date()).format('DD/MM/yyyy')) {
    const ordersAfterFilter = [];
    const untilThisDay = moment(startDate).format('DD/MM/yyyy');

    ordersAfterFilter.push(
      ...allOrders.filter(order => Number(moment(order.date).format('DD/MM/yyyy').split('/')[2]) < Number(untilThisDay.split('/')[2])),
      ...allOrders.filter(order => Number(moment(order.date).format('DD/MM/yyyy').split('/')[2]) === Number(untilThisDay.split('/')[2]) && Number(moment(order.date).format('DD/MM/yyyy').split('/')[1]) < Number(untilThisDay.split('/')[1])),
      ...allOrders.filter(order => Number(moment(order.date).format('DD/MM/yyyy').split('/')[2]) === Number(untilThisDay.split('/')[2]) && Number(moment(order.date).format('DD/MM/yyyy').split('/')[1]) === Number(untilThisDay.split('/')[1]) && Number(moment(order.date).format('DD/MM/yyyy').split('/')[0]) <= Number(untilThisDay.split('/')[0])),
    );
    allOrders = ordersAfterFilter;
  }

  const handleOpen = (index) => {
    dispatch(fetchOrderRequest(index));
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    dispatch(fetchOrderSuccess(''));
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };


  useEffect(() => {
    dispatch(fetchOrdersRequest());
  }, [dispatch]);

  const StyledTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({theme}) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


  const changeStatusHandler = (id, index) => {
    if (orders[index]?.status === 'Новый') {
      dispatch(changeStatusRequest({
        data: {status: 'Принято'}, id
      }))
    }
  }


  const locale = {
    localize: {
      day: n => days[n],
      month: n => months[n]
    },
    formatLong: {
      date: () => 'mm/dd/yyyy'
    }
  }

  const onChangeDate = date => {
    setStartDate(date);
  }

  return fetchOrdersLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <>
      <div className="container">
        <div className={classes.orderPageContainer}>
          <Typography variant="h4" className={classes.label}>Заказы</Typography>
          <div className={classes.orderPageContainer}>
            <Grid container justifyContent="space-between" alignItems="flex-start" spacing={2}>
              <Grid item container xs={12} sm={8} md={6} justifyContent="space-between" alignItems="flex-start"
                    spacing={2}>
                <Grid item xs={12} sm={8} md={6}
                >
                  <TextField
                    type="text"
                    select
                    fullWidth
                    required
                    color={'primary'}
                    label="Статус работы"
                    name="status"
                    value={state.status}
                    onChange={inputChangeHandler}
                  >
                    {statuses.map(status => (
                      <MenuItem
                        key={status._id}
                        value={status.title}
                      >
                        {status.title}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
                <Grid item xs={12} sm={8} md={5}>
                  <DatePicker
                    className={c.Datepicker}
                    locale={locale}
                    selected={startDate}
                    onChange={(date) => onChangeDate(date)}
                    dateFormat="dd/MM/yyyy"
                  />
                </Grid>
              </Grid>
              <Grid item xs={12} sm={4} md={3}>
                <Button
                  className={c.Button}
                  variant={"contained"}
                  component={Link} to="/orders/add"
                  fullWidth
                >
                  Оформить заказ
                </Button>
              </Grid>
            </Grid>
          </div>
        </div>
        <TableContainer component={Paper} className={classes.orderTable}>
          <Table sx={{minWidth: 700}} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Номер заказа</StyledTableCell>
                <StyledTableCell>Номер ГТД</StyledTableCell>
                <StyledTableCell>Дата</StyledTableCell>
                <StyledTableCell>Автор</StyledTableCell>
                <StyledTableCell>Статус</StyledTableCell>
                {profile?.role === "admin" && <StyledTableCell>Мастерская</StyledTableCell>}
                <StyledTableCell/>
              </TableRow>
            </TableHead>
            <TableBody>
              {allOrders.map((row, i) => (
                <StyledTableRow key={i}>
                  <StyledTableCell>
                    {row.orderNumber}
                  </StyledTableCell>
                  <StyledTableCell>{row?.gdt_id?.serialNo}</StyledTableCell>
                  <StyledTableCell>{moment(row.date).format('DD/MM/yyyy')}</StyledTableCell>
                  <StyledTableCell>{row.customer?.name}</StyledTableCell>
                  <StyledTableCell>{row.status}</StyledTableCell>
                  {profile?.role === "admin" && <StyledTableCell>{row.workshop?.name}</StyledTableCell>}
                  <StyledTableCell sx={profile.role === 'admin' ? {maxWidth: 230} : {maxWidth: 170}}>
                    <Grid container justifyContent="space-between" spacing={1}>
                      <Grid item>
                        <Button
                          component={row.status === "Новый" ? Button : Link}
                          to={'/repairer/orders/edit/' + row._id}
                          onClick={() => changeStatusHandler(row._id, i)}
                          variant="contained"
                          color="primary">{row.status === "Новый" ? "Принять" : "Редактировать"}
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button ml={1} mt={1} variant="contained" color="primary"
                                onClick={() => handleOpen(row._id)}>Подробнее</Button>
                      </Grid>
                    </Grid>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <ModalUI open={open} onClose={handleClose} maxWidth={400}>
          {fetchOrderLoading ? (
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
          ) : (
            <>
              <Typography variant="h6">Номер заказа: {order?.orderNumber}</Typography>
              <Typography variant="h6">Номер ГТД: {order?.gdt_id?.serialNo}</Typography>
              <Typography variant="h6">Дата: {moment(order?.date).format('L')}</Typography>
              <Typography variant="h6">Автор: {order?.customer?.name}</Typography>
              <Typography variant="h6">Статус: {order?.status}</Typography>
              <Typography variant="h6">Мастеркая: {order?.workshop?.name}</Typography>
              <Typography variant="h6">Комментарий: {order?.description}</Typography>
              {order?.master?.length > 0 && (
                <>
                  <Typography variant="h6" textAlign="center"><strong>Комментарий мастера: </strong></Typography>
                  {order?.master?.map((c, i) => (<Typography key={i} className={classes.modalComment} variant="body1">{ i+1 + ': ' + c?.title}</Typography>))}
                </>
              )}
            </>
          )}
        </ModalUI>
      </div>
    </>
  );
};

export default ReceiveOrders;
