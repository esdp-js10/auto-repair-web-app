import React, {useState} from 'react';
import {Avatar, Box, Button, Container, Grid, Paper, Stack, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {deleteUser, userStatusChange} from "../../../../store/actions/usersActions";
import {BASE_URL} from "../../../../config";
import noAvatar from "../../../../assets/images/mainPagePictures/avatar_placeholder.png";
import {makeStyles} from "@mui/styles";
import {LoadingButton} from "@mui/lab";
import ModalUI from "../../../../components/UI/ModalUI/ModalUI";

const RegClientsRequestsPage = () => {
  const s = useStyles();
  const dispatch = useDispatch();
  const regRequests = useSelector(state => state.users.reqRequests);
  const modalAcceptBtnLoading = useSelector(state => state.users.userStatusChangeLoading);
  const modalDeclineBtnLoading = useSelector(state => state.users.deleteUserLoading);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentModalUser, setCurrentModalUser] = useState(null);
  const openModal = (userData) => {
    setCurrentModalUser(userData);
    setIsModalOpen(true);
  };
  const closeModal = () => setIsModalOpen(false);

  const modalAcceptHandler = () => {
    dispatch(userStatusChange({
      id: currentModalUser?._id,
      successCallback: closeModal,
    }));
  };

  const modalDeclineHandler = () => {
    dispatch(deleteUser({
      id: currentModalUser?._id,
      successCallback: closeModal,
    }));
  };

  return (
    <Container sx={{mt: 6}}>
      <Stack spacing={3} my={2} width={'100%'} maxWidth={1200}>
        <Grid container spacing={{xs: 3, sm: 1}}>
          <Grid item xs={12} mb={3}>
            <Typography variant={"h5"}>
              Заявки на регистрацию:
            </Typography>
          </Grid>
          {regRequests?.map(user => (
            <Grid item
                  xs={12}
                  key={user._id}
            >
              <Grid container
                    alignItems={'center'}
                    component={Paper}
                    py={2} px={1}
                    className={s.paperWrapper}
              >
                <Grid item xs={12} sm={3} px={1} my={{xs: 1, sm: 0}}>
                  {user.creation_date}
                </Grid>
                <Grid item xs={12} sm={3} px={1} my={{xs: 1, sm: 0}}>
                  {user.name}
                </Grid>
                <Grid item xs={12} sm={3} px={1} my={{xs: 1, sm: 0}}>
                  {user.phone}
                </Grid>
                <Grid item xs={12} sm={3} px={1} my={{xs: 1, sm: 0}}
                      className={s.moreBtn}
                >
                  <Button onClick={() => openModal(user)}
                          variant={"contained"}
                          fullWidth sx={{maxWidth: 150}}
                  >
                    Подробнее
                  </Button>
                </Grid>
              </Grid>
              <ModalUI
                maxWidth={500}
                open={isModalOpen}
                onClose={closeModal}
              >
                <Typography variant={'h4'} mt={0} textAlign={"center"}
                >
                  Данные пользователя
                </Typography>
                <Box sx={{padding: '20px 0'}}>
                  <Avatar sx={{width: 150, height: 150, margin: '0 auto'}}
                          src={currentModalUser?.avatar ? BASE_URL + currentModalUser.avatar : noAvatar}
                  >
                    A
                  </Avatar>
                </Box>
                <Stack px={{sm: 5}}>
                  <Stack className={s.modalContent} spacing={2}>
                    <Typography variant={"body2"}>
                      <span>Имя : </span>
                      {currentModalUser?.name}
                    </Typography>
                    <Typography variant={"body2"}>
                      <span>Телефон : </span>
                      {currentModalUser?.phone}
                    </Typography>
                    <Typography variant={"body2"}>
                      <span>Адрес : </span>
                      {currentModalUser?.address}
                    </Typography>
                    <Typography variant={"body2"}>
                      <span>Почта : </span>
                      {currentModalUser?.email}
                    </Typography>
                  </Stack>
                </Stack>
                <Grid container>
                  <Grid item xs={12} sm={6} pr={{sm: 1}} pb={{xs: 1}}>
                    <LoadingButton variant={"contained"}
                                   loading={modalAcceptBtnLoading}
                                   color={"success"}
                                   fullWidth
                                   onClick={modalAcceptHandler}
                    >
                      Принять
                    </LoadingButton>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <LoadingButton variant={"contained"}
                                   loading={modalDeclineBtnLoading}
                                   fullWidth
                                   color={"error"}
                                   onClick={modalDeclineHandler}
                    >
                      Отклонить
                    </LoadingButton>
                  </Grid>
                </Grid>
              </ModalUI>
            </Grid>
          ))
          }
        </Grid>
      </Stack>
    </Container>
  );
};


const useStyles = makeStyles((theme) => ({
  modalContent: {
    marginBottom: '25px',
    '& span': {
      fontWeight: 700,
    },
    '& .MuiTypography-root': {
      fontSize: '18px',
    },
    [theme.breakpoints.up("sm")]: {
      minWidth: 350,
    },
  },
  paperWrapper: {
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
    },
  },
  moreBtn: {
    display: 'flex',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  modal: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '90vw',
    maxWidth: 700,
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #000',
    boxShadow: 24,
  },
}))

export default RegClientsRequestsPage;