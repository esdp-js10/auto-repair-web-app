import React, {useState} from 'react';
import {Button, Grid, Typography} from "@mui/material";
import WarehouseEditForm from "../../../../../components/UI/Forms/WarehouseEditForm/WarehouseEditForm";
import {useStyles} from "../Modal.styles";
import {Link} from "react-router-dom";
import ModalUI from "../../../../../components/UI/ModalUI/ModalUI";

const WarehouseItem = ({name, id, status}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Grid container spacing={3}
          justifyContent={"space-between"}
          alignItems={"center"}
          className={"warehouse"}
    >
      <ModalUI
        open={open}
        onClose={handleClose}
        maxWidth={650}
      >
        <WarehouseEditForm
          id={id}
          name={name}
          modalClose={handleClose}
          status={status}
        />
      </ModalUI>
      <Grid item xs={12} sm={"auto"} textAlign={"center"}>
        <Typography variant={"body1"}>
          {name}
        </Typography>
      </Grid>

      {status ?
        <Grid item xs={12} sm={"auto"} textAlign={"center"}>
          <Typography
            className={classes.status}
            style={{
              backgroundColor: 'green'
            }}
          >
            Активный
          </Typography>
        </Grid> :
        <Grid item xs={12} sm={"auto"} textAlign={"center"}>
          <Typography
            className={classes.status}
            style={{
              backgroundColor: 'red'
            }}
          >
            Неактивный
          </Typography>
        </Grid>}

      <Grid item container spacing={1} xs={12} sm={"auto"}>
        <Grid item xs={6} sm={'auto'}>
          <Button variant={"contained"} fullWidth component={Link} to={`/admin/warehouses/income?warehouse=${id}`}>
            Приход
          </Button>
        </Grid>
        <Grid item xs={6} sm={'auto'}>
          <Button variant={"contained"}
                  fullWidth
                  onClick={handleOpen}
          >
            Редактировать
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default WarehouseItem;