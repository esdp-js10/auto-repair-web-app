import React, {useEffect, useState} from 'react';
import InpSearch from "../../../../components/UI/Inputs/InpSearch/InpSearch";
import {Button, CircularProgress, Grid, Paper, Stack} from "@mui/material";
import WarehouseItem from "./Warehouse/WarehouseItem";
import Box from "@mui/material/Box";
import WarehouseAddForm from "../../../../components/UI/Forms/WarehouseAddForm/WarehouseAddForm";
import {useDispatch, useSelector} from "react-redux";
import {fetchWarehousesRequest} from "../../../../store/actions/warehousesActions";
import ModalUI from "../../../../components/UI/ModalUI/ModalUI";

const WarehouseMainPage = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const warehouses = useSelector(state => state.warehouses.warehouses);
  const fetchLoading = useSelector(state => state.warehouses.fetchWarehousesLoading);
  const [value, setValue] = useState('');

  useEffect(() => {
    dispatch(fetchWarehousesRequest());
  }, [dispatch]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const filteredWarehouses = warehouses.filter(warehouse => {
    return warehouse.name.toLowerCase().includes(value.toLowerCase())
  })

  return (
    <Stack spacing={3} my={2} width={'100%'}>
      <ModalUI
        open={open}
        onClose={handleClose}
        maxWidth={650}
      >
        <WarehouseAddForm
          modalClose={handleClose}
        />
      </ModalUI>
      <Grid container
            alignItems={"center"}
            justifyContent={"space-between"}
            flexWrap={"wrap-reverse"}
            spacing={2}
      >
        <Grid item xs={12} sm={6}>
          <InpSearch
            type="text"
            placeholder={"Поиск склада"}
            onChange={(event) => setValue(event.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={5} md={3}>
          <Button variant={"contained"}
                  fullWidth
                  onClick={handleOpen}
          >
            Создать склад
          </Button>
        </Grid>
      </Grid>

      {fetchLoading ? (
          <Grid container justifyContent="center" alignItems="center">
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>
        ) : filteredWarehouses.map(warehouse => (
        <Box component={Paper} p={2}
          key={warehouse._id}
        >
          <WarehouseItem
            id={warehouse._id}
            name={warehouse.name}
            status={warehouse.status}
            setOpenModal={setOpen}
          />
        </Box>
      ))
      }
    </Stack>
  );
};



export default WarehouseMainPage;