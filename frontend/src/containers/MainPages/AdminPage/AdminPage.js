import React from 'react';
import WarehouseMainPage from "./Warehouses/WarehouseMainPage";
import {Container, Stack} from "@mui/material";
import Box from "@mui/material/Box";

const AdminPage = () => {
  return (
    <Box component={Container} py={4}>
      <Stack spacing={3} alignItems={"center"}>
        <WarehouseMainPage/>
      </Stack>
    </Box>
  );
};

export default AdminPage;