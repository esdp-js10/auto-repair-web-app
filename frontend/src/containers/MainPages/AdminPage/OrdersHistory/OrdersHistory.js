import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, Typography} from "@mui/material";
import {styled} from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {makeStyles} from "@mui/styles";
import moment from 'moment';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrdersHistoryRequest, fetchOrderRequest, fetchOrderSuccess} from "../../../../store/actions/ordersActions";
import ModalUI from "../../../../components/UI/ModalUI/ModalUI";


const useStyles = makeStyles(() => ({
  orderPageContainer: {
    margin: '40px auto 50px',
  },
  label: {
    textAlign: 'center',
    fontFamily: '\'Montserrat\', sans-serif',
    marginTop: '30px'
  },
  orderTable: {
    marginBottom: '50px'
  },
  modalComment: {
    boxShadow: '0 0 0 1px black',
    padding: '4px 4px',
    borderRadius: '1px',
    margin: '15px 0 0',
  }
}))

const ReceiveOrders = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {profile} = useSelector(state => state.users);
  const {ordersHistory, fetchOrdersHistoryLoading, order, fetchOrderLoading} = useSelector(state => state.orders);
  const [open, setOpen] = useState(false);

  const handleOpen = (index) => {
    dispatch(fetchOrderRequest(index));
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    dispatch(fetchOrderSuccess(null));
  };


  useEffect(() => {
    dispatch(fetchOrdersHistoryRequest());
  }, [dispatch]);

  const StyledTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({theme}) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


  return fetchOrdersHistoryLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <>
      <div className="container">
        <div className={classes.orderPageContainer}>
          <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
            <Grid item xs={12} sm={5} md={6}>
              <Typography variant="h4">Выполненные заказы</Typography>
            </Grid>
          </Grid>
        </div>
        <TableContainer component={Paper} className={classes.orderTable}>
          <Table sx={{minWidth: 700}} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Номер заказа</StyledTableCell>
                <StyledTableCell>Номер ГТД</StyledTableCell>
                <StyledTableCell>Дата</StyledTableCell>
                <StyledTableCell>Автор</StyledTableCell>
                <StyledTableCell>Статус</StyledTableCell>
                {profile?.role === "admin" && <StyledTableCell>Мастерская</StyledTableCell>}
                <StyledTableCell/>
              </TableRow>
            </TableHead>
            <TableBody>
              {ordersHistory.map((row, i) => (
                <StyledTableRow key={i}>
                  <StyledTableCell>
                    {row.orderNumber}
                  </StyledTableCell>
                  <StyledTableCell>{row?.gdt_id?.serialNo}</StyledTableCell>
                  <StyledTableCell>{moment(row.date).format('L')}</StyledTableCell>
                  <StyledTableCell>{row.customer?.name}</StyledTableCell>
                  <StyledTableCell>{row.status}</StyledTableCell>
                  {profile?.role === "admin" && <StyledTableCell>{row.workshop?.name}</StyledTableCell>}
                  <StyledTableCell sx={profile.role === 'admin' ? {maxWidth: 230} : {maxWidth: 170}}>
                    <Grid container justifyContent="space-between" spacing={1}>
                      <Grid item>
                        <Button ml={1} mt={1} variant="contained" color="primary"
                                onClick={() => handleOpen(row._id)}>Подробнее</Button>
                      </Grid>
                    </Grid>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <ModalUI open={open} onClose={handleClose} maxWidth={400}>
          {fetchOrderLoading ? (
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
          ) : (
            <>
              <Typography variant="h6">Номер заказа: {order?.orderNumber}</Typography>
              <Typography variant="h6">Номер ГТД: {order?.gdt_id?.serialNo}</Typography>
              <Typography variant="h6">Дата: {moment(order?.date).format('L')}</Typography>
              <Typography variant="h6">Автор: {order?.customer?.name}</Typography>
              <Typography variant="h6">Статус: {order?.status}</Typography>
              <Typography variant="h6">Мастеркая: {order?.workshop?.name}</Typography>
              <Typography variant="h6">Комментарий: {order?.description}</Typography>
              {order?.master?.length > 0 && (
                <>
                  <Typography variant="h6" textAlign="center"><strong>Комментарий мастера: </strong></Typography>
                  {order?.master?.map((c, i) => (<Typography key={i} className={classes.modalComment} variant="body1">{ i+1 + ': ' + c?.title}</Typography>))}
                </>
              )}
            </>
          )}
        </ModalUI>
      </div>
    </>
  );
};

export default ReceiveOrders;
