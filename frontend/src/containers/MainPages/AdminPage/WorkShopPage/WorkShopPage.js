import React, {useEffect} from 'react';
import InpSearch from "../../../../components/UI/Inputs/InpSearch/InpSearch";
import {Button, CircularProgress, Grid, Paper, Stack} from "@mui/material";
import Box from "@mui/material/Box";
import WorkshopItem from "../../../../components/UI/StatusSwitch/WorkshopItem/WorkshopItem";
import {Link} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import {fetchWorkshopsRequest} from "../../../../store/actions/workshopActions";

const WorkshopPage = () => {
  const dispatch = useDispatch();
  const {fetchWorkshopsLoading, workshops} = useSelector(state => state.workshops);

  useEffect(() => {
    dispatch(fetchWorkshopsRequest());
  }, [dispatch]);

  return fetchWorkshopsLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <Stack className="container" maxWidth={950} spacing={2} my={2} xs={12} lg={10}>
      <Grid mt={2} container
            alignItems={"center"}
            justifyContent={"space-between"}
            flexWrap={"wrap-reverse"}
            spacing={2}
      >
        <Grid item xs={12} sm={6} md={4}>
          <InpSearch
            fullWidth
            onSubmit={(text) => {
              console.log(text)
            }}
            placeholder={"Название мастерской"}
          />
        </Grid>
        <Grid item xs={12} sm={5} md={3}>
          <Button
            component={Link}
            sx={{padding: '10px 0'}}
            to={"/admin/workshops/add"}
            variant={"contained"}
            fullWidth
          >
            Создать мастерскую
          </Button>
        </Grid>
      </Grid>
      {workshops.map((w, i) => (
        <Box key={i}  component={Paper} p={3}>
          <WorkshopItem id={w._id} name={w.name} status={w.status}/>
        </Box>
      ))}
    </Stack>
  );
};

export default WorkshopPage;