import React, {useEffect, useState} from 'react';
import {Grid, Stack, TextField, Typography} from "@mui/material";
import ButtonWithProgress from "../../../../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@mui/styles";
import {
  editPartNumberRequest,
  fetchPartNumbersOfDetailsRequest
} from "../../../../../store/actions/partNumbersDetailsActions";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(4),
    marginLeft: theme.spacing(10),
  },
  alert: {
    marginTop: theme.spacing(5),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(0, 0, 0),
  },
}));

const EditPartNumber = ({ partNumber, modalClose}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.partNumbersDetails.editPartNumberLoading);
  const error = useSelector(state => state.partNumbersDetails.editPartNumberError);
  const [state, setState] = useState({
    type: partNumber.type._id,
    partNumber: partNumber.partNumber,
    sizeIn: partNumber.sizeIn,
    sizeOut: partNumber.sizeOut,
    material: partNumber.material
  });

  useEffect(() => {
    return () => {
      dispatch(fetchPartNumbersOfDetailsRequest({_id: partNumber.type._id}));
    }
  }, []);

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };


  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(editPartNumberRequest({_id: partNumber._id, partNumberData: {...state}, successCallback: modalClose}));
  };



  return (
    <Stack spacing={3} maxWidth={700} width={'100%'}>
      <Typography variant={"h4"} textAlign={"center"}>
        Редактирование типа детали
      </Typography>
      <Grid
        container
        direction="column"
        component="form"
        className={classes.root}
        autoComplete="off"
        noValidate
      >
        <Grid item xs={4} pb={2}>
          <TextField fullWidth color={'primary'}
                     label="Парт номер"
                     name="partNumber"
                     type="text"
                     value={state.partNumber}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('partNumber'))}
                     helperText={getFieldError('partNumber')}
          />
        </Grid>
        <Grid item xs={4} pb={2}>
          <TextField fullWidth color={'primary'}
                     label="Size In"
                     name="sizeIn"
                     type="text"
                     value={state.sizeIn}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('sizeIn'))}
                     helperText={getFieldError('sizeIn')}
          />
        </Grid>
        <Grid item xs={4} pb={2}>
          <TextField fullWidth color={'primary'}
                     label="Size Out"
                     name="sizeOut"
                     type="text"
                     value={state.sizeOut}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('sizeOut'))}
                     helperText={getFieldError('sizeOut')}
          />
        </Grid>
        <Grid item xs={4} pb={2}>
          <TextField fullWidth color={'primary'}
                     label="Материал"
                     name="material"
                     type="text"
                     value={state.material}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('material'))}
                     helperText={getFieldError('material')}
          />
        </Grid>
        <Grid item xs={4} pb={2}>
          <ButtonWithProgress
            type="submit"
            fullWidth
            variant="contained"
            color={'action1'}
            className={classes.submit}
            onClick={submitFormHandler}
            loading={loading}
            disabled={loading}
          >
            Сохранить
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </Stack>
  );
};

export default EditPartNumber;