import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, CircularProgress, Container, Grid, Paper, Stack} from "@mui/material";
import InpSearch from "../../../../components/UI/Inputs/InpSearch/InpSearch";
import Box from "@mui/material/Box";
import {fetchTypesDetailsRequest} from "../../../../store/actions/typesDetailsActions";
import DetailItem from "./DetailItem/DetailItem";
import CreateDetailForm from "./CreateDetailForm/CreateDetailForm";
import ModalUI from "../../../../components/UI/ModalUI/ModalUI";

const DetailPage = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const details = useSelector(state => state.typesDetails.types);
  const fetchLoading = useSelector(state => state.typesDetails.fetchTypesDetailsLoading);
  const [value, setValue] = useState('');

  useEffect(() => {
    dispatch(fetchTypesDetailsRequest());
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const filteredDetails = details.filter(detail => {
    return detail.type.toLowerCase().includes(value.toLowerCase())
  })
  return (
    <Container>
      <Stack spacing={3} my={2} width={'100%'}>
        <ModalUI
          open={open}
          onClose={handleClose}
          maxWidth={650}
        >
          <CreateDetailForm
            modalClose={handleClose}
          />
        </ModalUI>
        <Grid container
              alignItems={"center"}
              justifyContent={"space-between"}
              flexWrap={"wrap-reverse"}
              spacing={2}
        >
          <Grid item xs={12} sm={5} md={3}>
          <Button variant={"contained"}
                  fullWidth
                  onClick={handleOpen}
          >
            Создать деталь
          </Button>
        </Grid>
          <Grid item xs={12} sm={6}>
            <InpSearch
              type="text"
              placeholder={"Поиск деталей"}
              onChange={(event) => setValue(event.target.value)}
            />
          </Grid>

        </Grid>

        {fetchLoading ? (
          <Grid container justifyContent="center" alignItems="center">
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>
        ) : filteredDetails.map(detail => (
          <Box component={Paper} p={2}
               key={detail._id}
          >
            <DetailItem
              id={detail._id}
              detail={detail}
            />
          </Box>
        ))
        }
      </Stack>
    </Container>
  );
};

export default DetailPage;