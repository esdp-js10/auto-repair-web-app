import React, {useEffect, useState} from 'react';
import {Grid, Stack, TextField, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {useDispatch, useSelector} from "react-redux";
import ButtonWithProgress from "../../../../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {createDetailFailure, createDetailRequest} from "../../../../../store/actions/typesDetailsActions";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2)
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
const CreateDetailForm = ({modalClose}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.typesDetails.createDetailError);
  const loading = useSelector(state => state.typesDetails.createDetailLoading);
  const [state, setState] = useState({
    type: ""
  });

  useEffect(() => {
    return () => {
      dispatch(createDetailFailure(null));
    }
  }, []);

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const submitFormHandler = async e => {
    e.preventDefault();
    await dispatch(createDetailRequest({data: {...state}, successCallback: modalClose}));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }

  return (
    <Stack spacing={3} maxWidth={700} width={'100%'}>
      <Typography variant={"h4"} textAlign={"center"}>
        Создать запчасть
      </Typography>
      <Grid
        container
        direction="column"
        component="form"
        className={classes.root}
        autoComplete="off"
        onSubmit={submitFormHandler}
        noValidate
      >
        <Grid item xs={12}>
          <TextField fullWidth color={'action1'}
                     label="Название"
                     name="type"
                     type="text"
                     value={state.type}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('type'))}
                     helperText={getFieldError('type')}
          />
        </Grid>
        <Grid item xs={12}>
          <ButtonWithProgress
            type="submit"
            fullWidth
            variant="contained"
            color={'action1'}
            className={classes.submit}
            loading={loading}
            disabled={loading}
          >
            Создать
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </Stack>
  );
};

export default CreateDetailForm;