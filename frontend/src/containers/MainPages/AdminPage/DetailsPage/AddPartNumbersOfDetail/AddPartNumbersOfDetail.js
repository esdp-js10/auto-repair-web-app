import React, {useEffect, useState} from 'react';
import {useStyles} from "../../../../../components/AccountsDetailForm/AccountsDetailForm.styles";
import {Button, Container, Grid, TextField, Typography} from "@mui/material";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import {useDispatch, useSelector} from "react-redux";
import {useLocation} from "react-router-dom";
import {addPartNumberFailure, addPartNumberRequest} from "../../../../../store/actions/partNumbersDetailsActions";
import {fetchTypesDetailRequest} from "../../../../../store/actions/typesDetailsActions";

const AddPartNumbersOfDetail = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const detail = useSelector(state => state.typesDetails.type);
  const error = useSelector(state => state.partNumbersDetails.addPartNumberError);
  const location = useLocation();
  const [partNumbers, setPartNumbers] = useState([{
    partNumber: '',
    sizeIn: '',
    sizeOut: '',
    material: '',
  }]);

  useEffect(() => {
    return () => {
      dispatch(addPartNumberFailure(null));
    }
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchTypesDetailRequest(location.search));
  }, [location.search]);

  const addPartNumber = i => {
    const copyPartNumbers = [...partNumbers];
    copyPartNumbers.splice(
      i+1,
      0,
      {partNumber: '', sizeIn: '', sizeOut: '', material: ''}
    );
    setPartNumbers(copyPartNumbers);
  };

  const deletePartNumber = i => {
    const newPartNumbers = [...partNumbers];

    if (newPartNumbers.length !== 1) {
      newPartNumbers.splice(i, 1);
    } else {
      Object.keys(newPartNumbers[i]).forEach(key => {
        newPartNumbers[i][key] = ''
      });
    }

    setPartNumbers(newPartNumbers);
  };

  const submitFormHandler = async e => {
    e.preventDefault();

    await dispatch(addPartNumberRequest({id: location.search, data: JSON.stringify(partNumbers)}))
  };
  const inputChangeHandler = (i, name, value) => {
    setPartNumbers(prevState => {
      const partNumberCopy = {
        ...prevState[i],
        [name]: value,
      };

      return prevState.map((partNumber, index) => {
        if (i === index) {
          return partNumberCopy;
        }

        return partNumber;
      });
    });
  };


  return (
    <Container>
      <div className={classes.formContainer}>
        <Grid item xs={12} sm={"auto"}>
          <Typography variant={"h4"}>
            {detail.type}
          </Typography>
        </Grid>
        <Grid
          container
          component="form"
          onSubmit={submitFormHandler}
          spacing={2}
          noValidate
          mb={4}
        >
          <Grid item xs={10} mb={5}>
            {partNumbers.map((partNumber, i) => {
              return (
                <Grid container key={i} justifyContent="start" alignItems="center" spacing={2} mb={2}>
                  <Grid item lg={2} md={4} sm={6} xs={12}>
                    <TextField
                      type="text"
                      required
                      fullWidth
                      label="Парт номер"
                      name="partNumber"
                      value={partNumbers[i].partNumber}
                      onChange={e => inputChangeHandler(i, 'partNumber', e.target.value)}
                      error={Boolean(error?.[i]?.partNumber?.message)}
                      helperText={error?.[i]?.partNumber?.message}

                    />
                  </Grid>
                  <Grid item lg={2} md={4} sm={6} xs={12}>
                    <TextField
                      type="text"
                      required
                      fullWidth
                      label="Size in"
                      name="sizeIn"
                      value={partNumbers[i].sizeIn}
                      onChange={e => inputChangeHandler(i, 'sizeIn', e.target.value)}
                      error={Boolean(error?.[i]?.sizeIn?.message)}
                      helperText={error?.[i]?.sizeIn?.message}
                    />
                  </Grid>
                  <Grid item lg={2} md={4} sm={6} xs={12}>
                    <TextField
                      type="text"
                      required
                      fullWidth
                      label="SizeOut"
                      name="sizeOut"
                      value={partNumbers[i].sizeOut}
                      onChange={e => inputChangeHandler(i, 'sizeOut', e.target.value)}
                      error={Boolean(error?.[i]?.sizeOut?.message)}
                      helperText={error?.[i]?.sizeOut?.message}
                    />
                  </Grid>
                  <Grid item lg={2} md={4} sm={6} xs={12}>
                    <TextField
                      type="text"
                      required
                      fullWidth
                      label="Материал"
                      name="material"
                      value={partNumbers[i].material}
                      onChange={e => inputChangeHandler(i, 'material', e.target.value)}
                      error={Boolean(error?.[i]?.material?.message)}
                      helperText={error?.[i]?.material?.message}
                    />
                  </Grid>

                  <Grid item lg={1} md={2} sm={3} xs={6}>
                    <Button onClick={() => deletePartNumber(i)}  variant="outlined" fullWidth className={classes.btn}>
                      <DeleteOutlineIcon style={{fontSize: '35px', paddingTop: '6px'}}/>
                    </Button>
                  </Grid>
                  <Grid item lg={1} md={2} sm={3} xs={6}>
                    <Button id="addBtn" onClick={() => addPartNumber(i)} variant="outlined" fullWidth className={classes.btn}>
                      <AddOutlinedIcon style={{fontSize: '35px', paddingTop: '6px'}}/>
                    </Button>
                  </Grid>
                </Grid>
              )
            })}
          </Grid>
          <Grid item xs={12}>
            <Button type="submit" variant="contained">Сохранить</Button>
          </Grid>
        </Grid>
      </div>

    </Container>
  );
};

export default AddPartNumbersOfDetail;