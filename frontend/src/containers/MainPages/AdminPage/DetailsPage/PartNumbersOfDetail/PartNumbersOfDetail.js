import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPartNumbersOfDetailsRequest} from "../../../../../store/actions/partNumbersDetailsActions";
import {Button, Grid, TableCell, Typography} from "@mui/material";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import ModalUI from "../../../../../components/UI/ModalUI/ModalUI";
import EditPartNumber from "../EditPartNumber/EditPartNumber";

const PartNumbersOfDetail = ({id, detail}) => {
  const dispatch = useDispatch();
  const partNumbersOfDetail = useSelector(state => state.partNumbersDetails.partNumbersOfDetail);
  const [open, setOpen] = useState(false);
  const [selectedPartNumber, setSelectedPartNumber] = useState(null);

  const handleOpen = (partId) => {
    setSelectedPartNumber(partId);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    dispatch(fetchPartNumbersOfDetailsRequest({_id: id}));
  }, [dispatch]);

  return (
    <>
      <Grid container
            justifyContent={"space-between"}
            alignItems={"center"}
      >
        <ModalUI
          open={open}
          onClose={handleClose}
          maxWidth={750}

        >
          <EditPartNumber
            partNumber={selectedPartNumber}
            modalClose={handleClose}
          />
        </ModalUI>
        <Grid item xs={12} sm={"auto"}>
          <Typography variant={"h5"}>
            Парт номера
          </Typography>
          <Typography variant={"h6"}>
            {detail.type}
          </Typography>
        </Grid>

        <TableContainer sx={{maxHeight: '75vh'}}>
          <Table sx={{minWidth: 500}} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="right">Парт номер</TableCell>
                <TableCell align="right">Size in</TableCell>
                <TableCell align="right">Size out</TableCell>
                <TableCell align="right">Материал</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {partNumbersOfDetail.map((partNumber) => (
                <TableRow
                  key={partNumber._id}
                  sx={{'&:last-child td, &:last-child th': {border: 0}}}
                >

                  <TableCell component="th" scope="row">
                    {partNumber.partNumber}
                  </TableCell>
                  <TableCell align="right">{partNumber.sizeIn}</TableCell>
                  <TableCell align="right">{partNumber.sizeOut}</TableCell>
                  <TableCell align="right">{partNumber.material}</TableCell>
                  <TableCell>
                    <Button
                      fullWidth
                      onClick={() => handleOpen(partNumber)}
                    >
                      <EditOutlinedIcon/>
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </>
  );
};

export default PartNumbersOfDetail;