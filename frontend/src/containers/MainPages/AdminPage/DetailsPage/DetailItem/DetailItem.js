import React, {useState} from 'react';
import {Button, Grid, Typography} from "@mui/material";
import {Link} from "react-router-dom";
import PartNumbersOfDetail from "../PartNumbersOfDetail/PartNumbersOfDetail";
import EditDetailForm from "../EditDetailForm/EditDetailForm";
import ModalUI from "../../../../../components/UI/ModalUI/ModalUI";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";

const DetailItem = ({detail, id}) => {
  const [open, setOpen] = useState(false);
  const [currentModalContent, setCurrentModalContent] = useState({});
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onPartNumbersHandler = () => {
    setCurrentModalContent({type: 'partNumbers'});
    handleOpen();
  };

  const onEditTypeHandler = () => {
    setCurrentModalContent({type: 'editType'});
    handleOpen();
  }

  return (
    <Grid container spacing={3}
          justifyContent={"space-between"}
          alignItems={"center"}
    >
      <ModalUI
        open={open}
        onClose={handleClose}
        maxWidth={650}
      >
        {currentModalContent?.type === 'partNumbers' && (
          <PartNumbersOfDetail
            id={id}
            detail={detail}
          />
        )}
        {currentModalContent?.type === 'editType' && (
          <EditDetailForm
            id={id}
            type={detail.type}
            modalClose={handleClose}
          />
        )}
      </ModalUI>


      <Grid item xs={12} sm={"auto"} textAlign={"center"}>
        <Typography variant={"body1"}>
          {detail.type}
        </Typography>
      </Grid>

      <Grid item container spacing={1} xs={12} sm={"auto"}>
        <Grid item xs={12} sm={'auto'}>
          <Button variant={"contained"}
                  fullWidth
                  component={Link} to={`addPartNumbers?type=${id}`}
          >
            Добавить парт номер
          </Button>
        </Grid>
        <Grid item xs={12} sm={'auto'}>
          <Button variant={"contained"}
                  fullWidth
                  onClick={(e) => {
                    onPartNumbersHandler(e)
                  }}
          >
            Подробнее
          </Button>
        </Grid>
        <Grid item xs={12} sm={'auto'}>
          <Button
                  fullWidth
                  onClick={(e) => {onEditTypeHandler(e)}}
          >
            <EditOutlinedIcon/>
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default DetailItem;