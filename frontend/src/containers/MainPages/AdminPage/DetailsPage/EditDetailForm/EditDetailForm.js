import React, {useState} from 'react';
import {makeStyles} from "@mui/styles";
import {useDispatch, useSelector} from "react-redux";
import {editTypeDetailRequest} from "../../../../../store/actions/typesDetailsActions";
import {Grid, Stack, TextField, Typography} from "@mui/material";
import ButtonWithProgress from "../../../../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(4),
    marginLeft: theme.spacing(10),
  },
  alert: {
    marginTop: theme.spacing(5),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(0, 0, 0),
  },
}));
const EditDetailForm = ({id, type, modalClose}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.typesDetails.editTypeDetailLoading);
  const error = useSelector(state => state.typesDetails.editTypeDetailError);
  const [state, setState] = useState({
    type: type,
  });

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(editTypeDetailRequest({_id: id, typeDetail: {...state}, successCallback: modalClose}));
  };

  return (
    <Stack spacing={3} maxWidth={700} width={'100%'}>
      <Typography variant={"h4"} textAlign={"center"}>
        Редактирование типа детали
      </Typography>
      <Grid
        container
        direction="column"
        component="form"
        className={classes.root}
        autoComplete="off"
        noValidate
      >
        <Grid item xs={4} pb={2}>
          <TextField fullWidth color={'primary'}
                     label="Название"
                     name="type"
                     type="text"
                     value={state.type}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('type'))}
                     helperText={getFieldError('type')}
          />
        </Grid>
        <Grid item xs={4} pb={2}>
          <ButtonWithProgress
            type="submit"
            fullWidth
            variant="contained"
            color={'action1'}
            className={classes.submit}
            onClick={submitFormHandler}
            loading={loading}
            disabled={loading}
          >
            Сохранить
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </Stack>
  );
};

export default EditDetailForm;