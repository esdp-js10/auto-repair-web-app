import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {ADMIN_ROLE, WAREHOUSEMAN_ROLE} from "../../../constants/userRoles";
import {checkAuth, fetchRegRequests} from "../../../store/actions/usersActions";
import {fetchWarehousesByStatus} from "../../../store/actions/warehousesActions";

const GlobalAppRequests = () => {
  const dispatch = useDispatch();
  const profile = useSelector(state => state.users.profile);

  useEffect(() => {
    if (profile?.role === ADMIN_ROLE) {
      dispatch(fetchRegRequests());
      const intervalRegList = setInterval(() => {
        dispatch(fetchRegRequests());
      }, 4000)
      return () => {
        clearInterval(intervalRegList);
      }
    }
    if (profile?.role === WAREHOUSEMAN_ROLE) {
      dispatch(fetchWarehousesByStatus('active'));
    }
  }, [dispatch, profile?.role]);

  useEffect(()=>{
    if (localStorage.getItem('gdt_token')){
      dispatch(checkAuth())
    }
  }, [dispatch])

  return null;
};

export default GlobalAppRequests;