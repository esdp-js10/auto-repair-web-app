import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, MenuItem, TextField, Typography} from "@mui/material";
import {styled} from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {Link} from "react-router-dom";
import moment from 'moment';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrderRequest, fetchOrdersRequest, fetchOrderSuccess} from "../../store/actions/ordersActions";
import useStyles from './OrderPage.styles';
import ModalUI from "../../components/UI/ModalUI/ModalUI";


const statuses = [
  {_id: 1, title: 'Все', text: 'all'},
  {_id: 2, title: 'Новый', text: 'new'},
  {_id: 3, title: 'Принято', text: 'submitted'},
  {_id: 4, title: 'Выполнено', text: 'done'},
  {_id: 5, title: 'Отклонено', text: 'rejected'}
];

const OrderPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {orders, fetchOrdersLoading, order, fetchOrderLoading } = useSelector(state => state.orders);
  const [open, setOpen] = useState(false);

  const handleOpen = (i) => {
    setOpen(true);
    dispatch(fetchOrderRequest(i));
  };

  const handleClose = () => {
    setOpen(false);
    dispatch(fetchOrderSuccess(null));
  };


  const [state, setState] = useState({
    status: "Все"
  });

  let allOrders = orders;

  if(state.status !== "Все"){
     allOrders = orders.filter(o => o.status === state.status);
  }


  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  useEffect(() => {
    dispatch(fetchOrdersRequest());
  }, [dispatch]);

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  return fetchOrdersLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <>
      <div className="container">
        <Typography variant="h4" className={classes.label}>Заказы</Typography>
        <div className={classes.orderPageContainer}>
          <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
            <Grid item xs={12} sm={5} md={3}
            >
              <TextField
                type="text"
                select
                fullWidth
                required
                color={'primary'}
                label="Статус работы"
                name="status"
                value={state.status}
                onChange={inputChangeHandler}
              >
                {statuses.map(status => (
                  <MenuItem
                    key={status._id}
                    value={status.title}
                  >
                    {status.title}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={5} md={3}>
              <Button
                variant={"contained"}
                component={Link} to="/orders/add"
                fullWidth
              >
                Оформить заказ
              </Button>
            </Grid>
          </Grid>
        </div>
        <TableContainer component={Paper} className={classes.orderTable}>
          <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Заказ</StyledTableCell>
                <StyledTableCell align="right">Номер ГТД</StyledTableCell>
                <StyledTableCell align="right">Дата</StyledTableCell>
                <StyledTableCell align="right">Статус</StyledTableCell>
                <StyledTableCell align="right">Мастерская</StyledTableCell>
                <StyledTableCell align="right">Автор</StyledTableCell>
                <StyledTableCell align="right" />
              </TableRow>
            </TableHead>
            <TableBody>
              {allOrders.map((row, i) => (
                <StyledTableRow key={i}>
                  <StyledTableCell component="th" scope="row">
                    {row.orderNumber}
                  </StyledTableCell>
                  <StyledTableCell align="right">{row?.gdt_id?.serialNo}</StyledTableCell>
                  <StyledTableCell align="right">{moment(row.date).format('L')}</StyledTableCell>
                  <StyledTableCell align="right">{row.status}</StyledTableCell>
                  <StyledTableCell align="right">{row.workshop?.name}</StyledTableCell>
                  <StyledTableCell align="right">{row.customer?.name}</StyledTableCell>
                  <StyledTableCell align="right"><Button variant="contained" color="primary"  onClick={() => handleOpen( row._id)}>Подробнее</Button></StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <ModalUI open={open} onClose={handleClose} maxWidth={400}>
          {fetchOrderLoading ? (
              <Grid container justifyContent="center" alignItems="center">
                <Grid item>
                  <CircularProgress/>
                </Grid>
              </Grid>
          ) : (
            <>
              <Typography variant="h6">Номер заказа: {order?.orderNumber}</Typography>
              <Typography variant="h6">Номер ГТД: {order?.gdt_id?.serialNo}</Typography>
              <Typography variant="h6">Дата: {moment(order?.date).format('L')}</Typography>
              <Typography variant="h6">Автор: {order?.customer?.name}</Typography>
              <Typography variant="h6">Статус: {order?.status}</Typography>
              <Typography variant="h6">Мастеркая: {order?.workshop?.name}</Typography>
              <Typography variant="h6">Комментарий: {order?.description}</Typography>
              {order?.master?.length > 0 && (
                <>
                  <Typography variant="h6" textAlign="center"><strong>Комментарий мастера: </strong></Typography>
                  {order?.master?.map((c, i) => (<Typography key={i} className={classes.modalComment} variant="body1">{ i+1 + ': ' + c?.title}</Typography>))}
                </>
              )}
            </>
            )}
        </ModalUI>
      </div>
    </>
  );
};

export default OrderPage;
