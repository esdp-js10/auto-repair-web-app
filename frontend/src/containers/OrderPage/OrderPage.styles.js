import {makeStyles} from "@mui/styles";

export default makeStyles(() => ({
  orderPageContainer: {
    margin: '40px auto 50px',
  },
  label: {
    textAlign: 'center',
    fontFamily: '\'Montserrat\', sans-serif',
    marginTop: '30px'
  },
  orderTable: {
    marginBottom: '50px'
  },
  modalComment: {
    boxShadow: '0 0 0 1px black',
    padding: '4px 4px',
    borderRadius: '1px',
    margin: '15px 0 0',
  }
}));