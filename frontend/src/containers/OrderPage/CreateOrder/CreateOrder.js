import React, {useEffect, useState} from 'react';
import {Autocomplete, CircularProgress, Grid, TextField, Typography} from "@mui/material";
import ButtonWithProgress from "../../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {makeStyles} from "@mui/styles";
import {useDispatch, useSelector} from "react-redux";
import MenuItem from "@mui/material/MenuItem";
import {createOrderRequest} from "../../../store/actions/ordersActions";
import {fetchWorkshopsRequest} from "../../../store/actions/workshopActions";
import {fetchGtdsRequest} from "../../../store/actions/GDTsActions";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    textAlign: 'center',
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    padding: '10px'
  },
  formContainer: {
    maxWidth: '500px',
    backgroundColor: '#FEFEFE',
    border: '1px solid #c1c1c1',
    borderRadius: '8px',
    padding: '0 30px',
    margin: '0 auto',
  },
  inputForm: {
    width: '400px',
  },
  label: {
    textAlign: 'center',
    fontFamily: '\'Montserrat\', sans-serif',
    marginTop: '50px',
  }
}));

const CreateOrder = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {workshops} = useSelector(state => state.workshops);
  const {createOrderLoading} = useSelector(state => state.orders);
  const {profile} = useSelector(state => state.users);
  const GDTs = useSelector(state => state.GDT.gtds);
  const [value, setValue] = useState('');
  const [inputValue, setInputValue] = useState('');
  const [state, setState] = useState({
    serialNo: "",
    description: "",
    workshop: "",
  });

  useEffect(() => {
    dispatch(fetchWorkshopsRequest());
    dispatch(fetchGtdsRequest());
  }, [dispatch]);

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const submitFormHandler = async e => {
    e.preventDefault();
    let link = '/orders';
    const workshop = workshops.filter(w => w.name === state.workshop);
    if(profile?.role){
      link = '/repairer/orders';
    }

    const gdt_id = GDTs.find(gdt => gdt.serialNo === value);

    await dispatch(createOrderRequest({
      data: {...state, gdt_id, workshop: workshop[0]},
      link: link,
    }));
  };

  return createOrderLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) :(
    <div className="container">
      <Typography variant="h4" className={classes.label}>Оформить заказ</Typography>
      <div className={classes.formContainer}>
        <Grid
          container
          direction="column"
          spacing={2}
          component="form"
          className={classes.root}
          autoComplete="off"
          onSubmit={submitFormHandler}
          noValidate
        >
          <Grid item xs={12}>
            <Autocomplete
              value={value}
              name="serialNo"
              inputValue={inputValue}
              onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
              }}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
              id="controllable-states-demo"
              options={GDTs?.map(o => o.serialNo)}
              renderInput={(params, i) => <TextField key={i} {...params} label="Серийный номер ГТД"/>}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              select
              label="Мастерской"
              fullWidth
              color={'action1'}
              onChange={inputChangeHandler}
              name="workshop"
              value={state.workshop}
            >
              {workshops.map((option) => (
                <MenuItem key={option._id} value={option.name}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              multiline
              rows={4}
              color={'primary'}
              type="text"
              label="Комментарий"
              name="description"
              value={state.description}
              onChange={inputChangeHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              loading={createOrderLoading}
              disabled={createOrderLoading}
            >
              Оформить заказ
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CreateOrder;