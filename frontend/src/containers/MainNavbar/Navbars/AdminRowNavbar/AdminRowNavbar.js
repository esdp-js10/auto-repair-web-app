import React from 'react';
import NavbarRowGrid from "../../../../components/MuiStyled/NavbarRowGrid/NavbarRowGrid";
import {Badge, Grid} from "@mui/material";
import {NavLink} from "react-router-dom";
import NavLinkMenuBtn from "../../../../components/UI/Menu/NavLinkMenuBtn/NavLinkMenuBtn";
import MenuItem from "@mui/material/MenuItem";
import {useSelector} from "react-redux";
import {
  A_CLIENTS_REG_REQ_PATH, A_WAREHOUSES_DETAILS_PATH,
  A_EMPLOYEES_PATH,
  A_NEWS_ADD_PATH, A_WAREHOUSES_PATH,
  A_WORKSHOPS_ADD_PATH, A_WORKSHOPS_HISTORY_PATH,
  A_WORKSHOPS_PATH, A_GDT_FORM, CATALOG_PATH
} from "../../../../constants/paths";

const AdminRowNavbar = () => {
  const regRequests = useSelector(state => state.users.reqRequests);

  return (
    <NavbarRowGrid container spacing={2}>
      <Grid item mr={2}>
        <Badge badgeContent={regRequests ? regRequests.length : 0} color="error">
          <NavLink to={A_CLIENTS_REG_REQ_PATH}>
            Заявки
          </NavLink>
        </Badge>
      </Grid>
      <Grid item>
        <NavLinkMenuBtn
          btnText={'Каталог'}
          to={CATALOG_PATH}
        >
          <MenuItem component={NavLink} to={A_GDT_FORM}>
            Создание ГДТ
          </MenuItem>
          <MenuItem component={NavLink} to={A_NEWS_ADD_PATH}>
            Создание новостей
          </MenuItem>
        </NavLinkMenuBtn>
      </Grid>
      <Grid item mr={2}>
        <NavLink to={A_EMPLOYEES_PATH}>
          Сотрудники
        </NavLink>
      </Grid>
      <Grid item>
        <NavLinkMenuBtn
          btnText={'Мастерские'}
          to={A_WORKSHOPS_PATH}
        >
          <MenuItem component={NavLink} to={A_WORKSHOPS_ADD_PATH}>
            Создание мастерской
          </MenuItem>
          <MenuItem component={NavLink} to={A_WORKSHOPS_HISTORY_PATH}>
            История заказов
          </MenuItem>
        </NavLinkMenuBtn>
      </Grid>

      <Grid item>
        <NavLinkMenuBtn
          btnText={'Склады'}
          to={A_WAREHOUSES_PATH}
        >
          <MenuItem component={NavLink} to={A_WAREHOUSES_DETAILS_PATH}>
            Детали
          </MenuItem>
        </NavLinkMenuBtn>
      </Grid>
    </NavbarRowGrid>
  );
};

export default AdminRowNavbar;