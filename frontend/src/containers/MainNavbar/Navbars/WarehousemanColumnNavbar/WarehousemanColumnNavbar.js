import React from 'react';
import {Link, useLocation} from "react-router-dom";
import NavbarList from "../../../../components/MuiStyled/NavbarList/NavbarList";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import NavbarAccordion from "../../../../components/MuiStyled/NavbarAccordion/NavbarAccordion";
import {
  W_WAREHOUSES_PATH,
  W_WAREHOUSES_TRANSFERS_PATH,
  W_WAREHOUSES_UPCOMING_PATH
} from "../../../../constants/paths";
import {useSelector} from "react-redux";

const WarehousemanColumnNavbar = ({burgerClose}) => {
  const location = useLocation();
  const activeWarehouses = useSelector(state => state.warehouses.fetchWarehousesByStatus);
  const profile = useSelector(state => state.users.profile);
  const [selectedIndex, setSelectedIndex] = React.useState(location.pathname);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    burgerClose();
  };

  return (
    <NavbarList>
      <NavbarAccordion panelText={'warehouses'}
                       linkTo={W_WAREHOUSES_PATH}
                       linkText={"Склады"}
                       selected={selectedIndex === W_WAREHOUSES_PATH}
                       onSelectClick={(event) => handleListItemClick(event, W_WAREHOUSES_PATH)}
      >
        <NavbarList>
          {activeWarehouses && (
            activeWarehouses.map(warehouse => {
              return (
                <ListItemButton
                  key={warehouse._id + 'nav'}
                  selected={selectedIndex === W_WAREHOUSES_PATH + warehouse._id}
                  onClick={(event) => handleListItemClick(event, W_WAREHOUSES_PATH + warehouse._id)}
                  component={Link} to={W_WAREHOUSES_PATH + warehouse._id}
                >
                  <ListItemText primary={profile.warehouse === warehouse._id ? 'Ваш ' + warehouse.name : warehouse.name}/>
                </ListItemButton>
              )
            })

          )}
        </NavbarList>
      </NavbarAccordion>
      <ListItemButton
        selected={selectedIndex === W_WAREHOUSES_UPCOMING_PATH}
        onClick={(event) => handleListItemClick(event, W_WAREHOUSES_UPCOMING_PATH)}
        component={Link} to={W_WAREHOUSES_UPCOMING_PATH}
      >
        <ListItemText primary="История приходов"/>
      </ListItemButton>
      <ListItemButton
        selected={selectedIndex === W_WAREHOUSES_TRANSFERS_PATH}
        onClick={(event) => handleListItemClick(event, W_WAREHOUSES_TRANSFERS_PATH)}
        component={Link} to={W_WAREHOUSES_TRANSFERS_PATH}
      >
        <ListItemText primary="История перемещений"/>
      </ListItemButton>
    </NavbarList>
  );
};

export default WarehousemanColumnNavbar;