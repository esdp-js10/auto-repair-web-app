import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import {Link, useLocation} from "react-router-dom";
import NavbarList from "../../../../components/MuiStyled/NavbarList/NavbarList";
import {CATALOG_PATH, LOGIN_PATH, ORDERS_ADD_PATH, ORDERS_PATH} from "../../../../constants/paths";
import NavbarAccordion from "../../../../components/MuiStyled/NavbarAccordion/NavbarAccordion";

const UserColumnNavbar = ({isRegistred, burgerClose}) => {
  const location = useLocation();
  const [selectedIndex, setSelectedIndex] = React.useState(location.pathname);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    burgerClose();
  };

  return (
    <NavbarList>
      {!isRegistred && (
        <ListItemButton
          selected={selectedIndex === LOGIN_PATH}
          onClick={(event) => handleListItemClick(event, LOGIN_PATH)}
          component={Link} to='/login' sx={{color: "inherit"}}
        >
          <ListItemText primary="Войти"/>
        </ListItemButton>
      )}
      <ListItemButton
        selected={selectedIndex === '/'}
        onClick={(event) => handleListItemClick(event, '/')}
        component={Link} to='/'
      >
        <ListItemText primary="Главная"/>
      </ListItemButton>
      <ListItemButton
        selected={selectedIndex === CATALOG_PATH}
        onClick={(event) => handleListItemClick(event, CATALOG_PATH)}
        component={Link} to={CATALOG_PATH}
      >
        <ListItemText primary="Каталог"/>
      </ListItemButton>

      {isRegistred && (
        <>
          <NavbarAccordion panelText={'orders'}
                           linkTo={ORDERS_PATH}
                           linkText={"Заказы"}
                           selected={selectedIndex === ORDERS_PATH}
                           onSelectClick={(event) => handleListItemClick(event, ORDERS_PATH)}
          >
            <NavbarList>
              <ListItemButton
                selected={selectedIndex === ORDERS_ADD_PATH}
                onClick={(event) => handleListItemClick(event, ORDERS_ADD_PATH)}
                component={Link} to={ORDERS_ADD_PATH}
              >
                <ListItemText primary="Оформить заказ"/>
              </ListItemButton>
            </NavbarList>
          </NavbarAccordion>
        </>
      )}
    </NavbarList>
  );
};

export default UserColumnNavbar;