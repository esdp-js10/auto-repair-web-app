import React from 'react';
import {Link, useLocation} from "react-router-dom";
import NavbarList from "../../../../components/MuiStyled/NavbarList/NavbarList";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import {ORDERS_ADD_PATH, R_ORDERS_HISTORY_PATH, R_ORDERS_PATH} from "../../../../constants/paths";

const RepairerColumnNavbar = ({burgerClose}) => {
  const location = useLocation();
  const [selectedIndex, setSelectedIndex] = React.useState(location.pathname);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    burgerClose();
  };

  return (
    <NavbarList>
      <ListItemButton
        selected={selectedIndex === R_ORDERS_PATH}
        onClick={(event) => handleListItemClick(event, R_ORDERS_PATH)}
        component={Link} to={R_ORDERS_PATH}
      >
        <ListItemText primary="Заказы"/>
      </ListItemButton>
      <ListItemButton
        selected={selectedIndex === {ORDERS_ADD_PATH}}
        onClick={(event) => handleListItemClick(event, ORDERS_ADD_PATH)}
        component={Link} to={ORDERS_ADD_PATH}
      >
        <ListItemText primary="Создать заказ"/>
      </ListItemButton>
      <ListItemButton
        selected={selectedIndex === R_ORDERS_HISTORY_PATH}
        onClick={(event) => handleListItemClick(event, R_ORDERS_HISTORY_PATH)}
        component={Link} to={R_ORDERS_HISTORY_PATH}
      >
        <ListItemText primary="История заказов"/>
      </ListItemButton>
    </NavbarList>
  );
};

export default RepairerColumnNavbar;