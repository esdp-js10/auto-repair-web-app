import React from 'react';
import {NavLink} from "react-router-dom"
import {Grid} from "@mui/material";
import NavbarRowGrid from '../../../../components/MuiStyled/NavbarRowGrid/NavbarRowGrid'
import {CATALOG_PATH, ORDERS_ADD_PATH, ORDERS_PATH} from "../../../../constants/paths";
import MenuItem from "@mui/material/MenuItem";
import NavLinkMenuBtn from "../../../../components/UI/Menu/NavLinkMenuBtn/NavLinkMenuBtn";

const UserRowNavbar = ({isRegistred}) => {
  return (
    <NavbarRowGrid container spacing={3}>
      <Grid item>
        <NavLink to={'/'}>
          Главная
        </NavLink>
      </Grid>
      <Grid item>
        <NavLink to={CATALOG_PATH}>
          Каталог
        </NavLink>
      </Grid>
      {isRegistred && (
        <Grid item>
        <NavLinkMenuBtn
          btnText={'Заказы'}
          to={ORDERS_PATH}
        >
          <MenuItem component={NavLink} to={ORDERS_ADD_PATH}>
            Оформить заказ
          </MenuItem>
        </NavLinkMenuBtn>
        </Grid>
      )}
    </NavbarRowGrid>
  );
};

export default UserRowNavbar;