import React from 'react';
import NavbarRowGrid from "../../../../components/MuiStyled/NavbarRowGrid/NavbarRowGrid";
import {Grid} from "@mui/material";
import {NavLink} from "react-router-dom";
import NavLinkMenuBtn from "../../../../components/UI/Menu/NavLinkMenuBtn/NavLinkMenuBtn";
import MenuItem from "@mui/material/MenuItem";
import {
  W_WAREHOUSES_PATH,
  W_WAREHOUSES_TRANSFERS_PATH,
  W_WAREHOUSES_UPCOMING_PATH
} from "../../../../constants/paths";
import {useSelector} from "react-redux";

const WarehousemanRowNavbar = () => {
  const profile = useSelector(state => state.users.profile);
  const activeWarehouses = useSelector(state => state.warehouses.fetchWarehousesByStatus);
  return (
    <NavbarRowGrid container spacing={3}>
      <Grid item>
        <NavLinkMenuBtn
          btnText={'Склады'}
          to={W_WAREHOUSES_PATH}
        >
          {activeWarehouses && (
            activeWarehouses.map(warehouse => {
              return (
                <MenuItem component={NavLink} to={W_WAREHOUSES_PATH  + warehouse._id} key={warehouse._id + 'nav'}>
                  {profile.warehouse === warehouse._id ? 'Ваш ' + warehouse.name : warehouse.name}
                </MenuItem>
              )
            }))}
        </NavLinkMenuBtn>
      </Grid>
      <Grid item>
        <NavLink to={W_WAREHOUSES_UPCOMING_PATH}>
          История приходов
        </NavLink>
      </Grid>
      <Grid item>
        <NavLink to={W_WAREHOUSES_TRANSFERS_PATH}>
          История перемещений
        </NavLink>
      </Grid>
    </NavbarRowGrid>
  );
};

export default WarehousemanRowNavbar;