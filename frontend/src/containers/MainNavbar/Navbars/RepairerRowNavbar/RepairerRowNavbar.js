import React from 'react';
import {Grid} from "@mui/material";
import {NavLink} from "react-router-dom";
import NavbarRowGrid from "../../../../components/MuiStyled/NavbarRowGrid/NavbarRowGrid";
import {ORDERS_ADD_PATH, R_ORDERS_HISTORY_PATH, R_ORDERS_PATH} from "../../../../constants/paths";

const RepairerRowNavbar = () => {
  return (
    <NavbarRowGrid container spacing={3}>
      <Grid item>
        <NavLink to={R_ORDERS_PATH} end>
          Заказы
        </NavLink>
      </Grid>
      <Grid item>
        <NavLink to={ORDERS_ADD_PATH}>
          Создать заказ
        </NavLink>
      </Grid>
      <Grid item>
        <NavLink to={R_ORDERS_HISTORY_PATH}>
          История заказов
        </NavLink>
      </Grid>
    </NavbarRowGrid>
  );
};

export default RepairerRowNavbar;