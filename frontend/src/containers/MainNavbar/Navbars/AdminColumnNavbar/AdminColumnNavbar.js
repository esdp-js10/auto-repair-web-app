import React from 'react';
import NavbarList from "../../../../components/MuiStyled/NavbarList/NavbarList";
import ListItemButton from "@mui/material/ListItemButton";
import {Link, useLocation} from "react-router-dom";
import ListItemText from "@mui/material/ListItemText";
import NavbarAccordion from "../../../../components/MuiStyled/NavbarAccordion/NavbarAccordion";
import {
  A_CLIENTS_REG_REQ_PATH,
  A_EMPLOYEES_PATH,
  A_GDT_FORM,
  A_NEWS_ADD_PATH,
  A_WAREHOUSES_DETAILS_PATH,
  A_WAREHOUSES_PATH,
  A_WORKSHOPS_ADD_PATH,
  A_WORKSHOPS_HISTORY_PATH,
  A_WORKSHOPS_PATH,
  CATALOG_PATH
} from "../../../../constants/paths";
import {Avatar, ListItemIcon} from "@mui/material";
import {useSelector} from "react-redux";

const AdminColumnNavbar = ({burgerClose}) => {
  const regRequests = useSelector(state => state.users.reqRequests);
  const location = useLocation();
  const [selectedIndex, setSelectedIndex] = React.useState(location.pathname);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    burgerClose();
  };

  return (
    <NavbarList>
      <ListItemButton
        selected={selectedIndex === A_CLIENTS_REG_REQ_PATH}
        onClick={(event) => handleListItemClick(event, A_CLIENTS_REG_REQ_PATH)}
        component={Link} to={A_CLIENTS_REG_REQ_PATH}
      >
        <ListItemText primary="Заявки"/>
        {regRequests?.length > 0 && (
          <ListItemIcon>
            <Avatar sx={{bgcolor: 'error.main'}}>{regRequests?.length}</Avatar>
          </ListItemIcon>
        )}
      </ListItemButton>
      <NavbarAccordion panelText={'catalog'}
                       linkTo={CATALOG_PATH}
                       linkText={"Каталог"}
                       selected={selectedIndex === CATALOG_PATH}
                       onSelectClick={(event) => handleListItemClick(event, CATALOG_PATH)}
      >
        <NavbarList>
          <ListItemButton
            selected={selectedIndex === A_GDT_FORM}
            onClick={(event) => handleListItemClick(event, A_GDT_FORM)}
            component={Link} to={A_GDT_FORM}
          >
            <ListItemText primary="Создание ГДТ"/>
          </ListItemButton>
          <ListItemButton
            selected={selectedIndex === A_NEWS_ADD_PATH}
            onClick={(event) => handleListItemClick(event, A_NEWS_ADD_PATH)}
            component={Link} to={A_NEWS_ADD_PATH}
          >
            <ListItemText primary="Создание новостей"/>
          </ListItemButton>
        </NavbarList>
      </NavbarAccordion>
      <ListItemButton
        selected={selectedIndex === A_EMPLOYEES_PATH}
        onClick={(event) => handleListItemClick(event, A_EMPLOYEES_PATH)}
        component={Link} to={A_EMPLOYEES_PATH}
      >
        <ListItemText primary="Сотрудники"/>
      </ListItemButton>
      <NavbarAccordion panelText={'workshop'}
                       linkTo={A_WORKSHOPS_PATH}
                       linkText={"Мастерские"}
                       selected={selectedIndex === A_WORKSHOPS_PATH}
                       onSelectClick={(event) => handleListItemClick(event, A_WORKSHOPS_PATH)}
      >
        <NavbarList>

          <ListItemButton
            selected={selectedIndex === A_WORKSHOPS_ADD_PATH}
            onClick={(event) => handleListItemClick(event, A_WORKSHOPS_ADD_PATH)}
            component={Link} to={A_WORKSHOPS_ADD_PATH}
          >
            <ListItemText primary="Создание мастерской"/>
          </ListItemButton>
          <ListItemButton
            selected={selectedIndex === A_WORKSHOPS_HISTORY_PATH}
            onClick={(event) => handleListItemClick(event, A_WORKSHOPS_HISTORY_PATH)}
            component={Link} to={A_WORKSHOPS_HISTORY_PATH}
          >
            <ListItemText primary="История заказов"/>
          </ListItemButton>
        </NavbarList>
      </NavbarAccordion>
      <NavbarAccordion panelText={'warehouses'}
                       linkTo={A_WAREHOUSES_PATH}
                       linkText={"Склады"}
                       selected={selectedIndex === A_WAREHOUSES_PATH}
                       onSelectClick={(event) => handleListItemClick(event, A_WAREHOUSES_PATH)}
      >
        <NavbarList>
          <ListItemButton
            selected={selectedIndex === A_WAREHOUSES_DETAILS_PATH}
            onClick={(event) => handleListItemClick(event, A_WAREHOUSES_DETAILS_PATH)}
            component={Link} to={A_WAREHOUSES_DETAILS_PATH}
          >
            <ListItemText primary="Детали"/>
          </ListItemButton>
        </NavbarList>
      </NavbarAccordion>

    </NavbarList>
  );
};

export default AdminColumnNavbar;