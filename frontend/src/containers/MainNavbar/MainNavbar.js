import React, {useCallback, useState} from 'react';
import {Button, Divider, Grid, Stack, useMediaQuery} from "@mui/material";
import {Link} from "react-router-dom";
import logo from "../../assets/images/mainPagePictures/logo.png";
import {useSelector} from "react-redux";
import {makeStyles, useTheme} from "@mui/styles";
import InpSearchMain from "../../components/UI/Inputs/InpSearchMain/InpSearchMain";
import BurgerMenu from "../../components/UI/Menu/BurgerMenu/BurgerMenu";
import ProfileMenu from "./ProfileMenu/ProfileMenu";
import UserRowNavbar from "./Navbars/UserRowNavbar/UserRowNavbar";
import AdminRowNavbar from "./Navbars/AdminRowNavbar/AdminRowNavbar";
import RepairerRowNavbar from "./Navbars/RepairerRowNavbar/RepairerRowNavbar";
import WarehousemanRowNavbar from "./Navbars/WarehousemanRowNavbar/WarehousemanRowNavbar";
import UserColumnNavbar from "./Navbars/UserColumnNavbar/UserColumnNavbar";
import AdminColumnNavbar from "./Navbars/AdminColumnNavbar/AdminColumnNavbar";
import RepairerColumnNavbar from "./Navbars/RepairerColumnNavbar/RepairerColumnNavbar";
import WarehousemanColumnNavbar from "./Navbars/WarehousemanColumnNavbar/WarehousemanColumnNavbar";

const NavigationMenu = () => {
  const s = useStyles();
  const theme = useTheme();
  const isMdDown = useMediaQuery(theme.breakpoints.down('md'));
  const isXsmUp = useMediaQuery(theme.breakpoints.up('xsm'));
  const profile = useSelector((state) => state.users.profile);
  const [isBurgerOpen, setIsBurgerOpen] = useState(false);

  const closeBurgerHandler = useCallback(()=>{
    setIsBurgerOpen(false)
  }, []);

  const openBurgerHandler = useCallback(()=>{
    setIsBurgerOpen(true)
  }, []);

  return (
    <Grid container
          py={1}
          className={s.navWrapper}
    >
      {!isMdDown && (
        <Grid item>
          <Grid container spacing={1}
                alignItems={"center"}
                flexWrap={'nowrap'}
                textAlign={"center"}
                whiteSpace={"nowrap"}
          >
            {(!profile || !profile?.role) ? (
              <>
                <Grid item>
                  <UserRowNavbar isRegistred={Boolean(profile)}/>
                </Grid>
                <Grid item mx={2} flexShrink={10}>
                  <InpSearchMain placeholder="Код проверки"/>
                </Grid>
              </>
            ) : (
              <Grid item>
                {(profile?.role === 'admin') && (<AdminRowNavbar/>)}
                {(profile?.role === 'repairer') && (<RepairerRowNavbar/>)}
                {(profile?.role === 'warehouseman') && (<WarehousemanRowNavbar/>)}
              </Grid>
            )}
            <Grid item>
              <Divider orientation="vertical" variant="middle"
                       sx={{height: 15, width: 1, backgroundColor: "gainsboro"}}
              />
            </Grid>
            <Grid item>
              {!profile && (
                <Button component={Link}
                        to='/login'
                        sx={{color: "inherit", fontSize: 16}}
                >
                  Войти
                </Button>
              )}
              {profile && (
                <ProfileMenu closeBurger={closeBurgerHandler}/>
              )}
            </Grid>
          </Grid>
        </Grid>
      )}
      {isMdDown && (
        <Grid container item sx={{position: "relative"}} pr={'40px'} justifyContent={"flex-end"}>
          <Grid item mr={2} minHeight={45}>
            {isXsmUp && (!profile || !profile?.role) && (<InpSearchMain placeholder="Код проверки" sx={{maxWidth: 150}}/>)}
          </Grid>
          <Grid item>
            <BurgerMenu isOpen={isBurgerOpen}
                        onClose={closeBurgerHandler}
                        onOpen={openBurgerHandler}
            >
              <Stack mb={4}>
                {profile && (
                  <ProfileMenu closeBurger={closeBurgerHandler}/>
                )}
              </Stack>
              {(!profile || !profile?.role)
                ? (<UserColumnNavbar isRegistred={Boolean(profile)} burgerClose={closeBurgerHandler}/>)
                : (
                  <>
                    {profile?.role === 'admin' && (<AdminColumnNavbar burgerClose={closeBurgerHandler}/>)}
                    {profile?.role === 'repairer' && (<RepairerColumnNavbar burgerClose={closeBurgerHandler}/>)}
                    {profile?.role === 'warehouseman' && (<WarehousemanColumnNavbar burgerClose={closeBurgerHandler}/>)}
                  </>
                )}
            </BurgerMenu>
          </Grid>
        </Grid>
      )}
      <Grid item sx={{minWidth: 180}}>
        <Button
          component={Link}
          to={'/'}
          startIcon={<img className={s.logoIcon} src={logo} alt="logo"/>}
          className={s.logoText}
        >
          ГДТ Бишкек
        </Button>
      </Grid>
    </Grid>
  );
};

const useStyles = makeStyles(() => ({
  navWrapper: {
    justifyContent: "space-between",
    alignItems: "center",
    flexWrap: 'noWrap',
    flexDirection: "row-reverse",
  },
  logoText: {
    fontStyle: 'italic',
    fontWeight: 500,
    fontSize: '18px',
    lineHeight: '22px',
    color: 'white',
  },
  logoIcon: {
    height: '40px',
    animation: `$logoIconSpin 5500ms linear infinite`,
  },
  "@keyframes logoIconSpin": {
    "0%": {
      transform: "rotate(0deg)",
    },
    "100%": {
      transform: "rotate(360deg)",
    },
  },
}))

export default NavigationMenu;