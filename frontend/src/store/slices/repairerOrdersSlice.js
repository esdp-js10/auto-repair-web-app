import {createSlice} from "@reduxjs/toolkit";

const name = 'repairerOrders';

const repairerOrdersSlice = createSlice({
  name,
  initialState: {
    fetchingReservedDetails: false,
    reservedDetails: [],
    availableParts: [],
    availableUniqueTypes: [],
    extraDetails: [],
    extraWorks: [],
    checkedDetail: false,
    checkedWork: false,
    error: null
  },
  reducers: {
    fetchRepairOrderData(state) {
      state.fetchingReservedDetails = true;
    },
    fetchRepairOrderDataSuccess(state, action) {
      state.fetchingReservedDetails = false;

      // reservedDetails definition
      action.payload.reserved.length > 0
        ? state.reservedDetails = action.payload.reserved
        : state.reservedDetails.push({type: '', partNumber: '', reservedQuantity: 0});

      // extraDetails definition
      if (action.payload.extraDetails.length > 0) {
        state.extraDetails = action.payload.extraDetails;
        state.extraDetails[0].title === '' ? state.checkedDetail = false : state.checkedDetail = true;
      } else {
        state.extraDetails.push({title: '', price: ''});
        state.checkedWork = false;
      }

      // extraWorks definition
      if (action.payload.extraWorks.length > 0) {
        state.extraWorks = action.payload.extraWorks
        state.extraWorks[0].title === '' ? state.checkedWork = false : state.checkedWork = true;
      } else {
        state.extraWorks.push({title: '', price: ''});
        state.checkedWork = false;
      }
    },
    fetchRepairOrderDataFailure(state) {
      state.fetchingReservedDetails = false;
    },
    changeReservedItem(state, action) {
      state.reservedDetails = state.reservedDetails.map((reservedDetail, i) => {
        if (action.payload.changingCurrentIndex === i) {
          reservedDetail[action.payload.nameOfField] = action.payload.valueToChange;
        }
        return reservedDetail;
      });
    },
    changeExtras(state, action) {
      state[action.payload.field] = state[action.payload.field].map((item, i) => {
        if (action.payload.changingCurrentIndex === i) {
          item[action.payload.name] = action.payload.value;
        }
        return item;
      });
    },
    addNewReservedItem(state) {
      state.reservedDetails.push({
        type: '',
        partNumber: '',
        reservedQuantity: 0,
      });
    },
    addExtras(state, action) {
      state[action.payload.field].push({title: '', price: ''});
    },
    removeReservedItem(state, action) {
      if (state.reservedDetails.length === 1) {
        state.reservedDetails = [{
          type: '',
          partNumber: '',
          reservedQuantity: 0,
        }]
      } else {
        state.reservedDetails = state.reservedDetails.filter((reservedDetail, i) => i !== action.payload);
      }
    },
    removeExtras(state, action) {
      if (state[action.payload.field].length === 1) {
        state[action.payload.field] = [{title: '', price: ''}];
      } else {
        state[action.payload.field] = state[action.payload.field].filter((item, i) => i !== action.payload.indexOfItem);
      }
    },
    submitItems(state, action) {

    },
    submitItemsFailureSaga(state, action) {
      state.error = action.payload;
    },
    fetchAvailableParts(state, action) {

    },
    fetchAvailablePartsSuccess(state, action) {
      state.availableParts = action.payload;

      const availableTypes = state.availableParts.map(item => item.type);
      state.availableUniqueTypes = [...new Map(availableTypes.map(item => [item['_id'], item])).values()];
    },
    cleanRepairPageFormState(state) {
      state.reservedDetails = [];
      state.reservedDetails = [];
      state.availableParts = [];
      state.availableUniqueTypes = [];
      state.extraDetails = [];
      state.extraWorks = [];
    },
    changeExtraStatus(state, action) {
      state[action.payload.name] = action.payload.checked;
    },
  }
});

export default repairerOrdersSlice;
