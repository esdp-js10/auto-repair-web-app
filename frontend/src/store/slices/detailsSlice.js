const {createSlice} = require("@reduxjs/toolkit");

const name = "details";

const detailsSlice = createSlice({
	name,
	initialState: {
		myDetailsInTheWarehouse: [],
		migrationHistory: [],
		dataHistory: [],
		addDetailLoading: false,
		addDetailError: null,
		fetchDetailsInTheWarehouseLoading: false,
		fetchDetailsInTheWarehouseError: null,
		movingDetailsLoading: false,
		movingDetailsError: null,
		migrationHistoryLoading: false,
		dataHistoryLoading: false,
	},
	reducers: {
		addDetailRequest(state) {
			state.addDetailLoading = true;
		},
		addDetailSuccess(state) {
			state.addDetailLoading = false;
		},
		addDetailFailure(state, action) {
			state.addDetailLoading = false;
			state.addDetailError = action.payload;
		},
		fetchDetailsInTheWarehouse(state) {
			state.fetchDetailsInTheWarehouseLoading = true;
		},
		fetchDetailsInTheWarehouseSuccess(state, action) {
			state.fetchDetailsInTheWarehouseLoading = false;
			state.myDetailsInTheWarehouse = action.payload;
		},
		fetchDetailsInTheWarehouseFailure(state, action) {
			state.fetchDetailsInTheWarehouseLoading = false;
			state.fetchDetailsInTheWarehouseError = action.payload;
		},
		movingDetails(state) {
			state.movingDetailsLoading = true;
			state.movingDetailsError = null;
		},
		movingDetailsSuccess(state, action) {
			state.movingDetailsLoading = false;
			state.movingDetailsError = null;
			state.myDetailsInTheWarehouse = state.myDetailsInTheWarehouse.map(d => {
				const element = JSON.parse(action.payload).find(item => item.partNumber === d.partNumber._id);

				if (element) {
					return {
						...d,
						quantity: d.quantity - element.quantity,
						completedPrice: (((d.completedPrice * d.quantity) + ((d.quantity - element.quantity) * d.completedPrice)) /
							((d.quantity - element.quantity) + d.quantity)).toFixed(3),
					};
				}
				return d;
			});
			state.movingSuccess = true;
		},
		movingDetailsFailure(state, action) {
			state.movingDetailsLoading = false;
			state.movingDetailsError = action.payload;
		},
		fetchMigrationHistory(state) {
			state.migrationHistoryLoading = true;
		},
		fetchMigrationHistorySuccess(state, action) {
			state.migrationHistoryLoading = false;
			state.migrationHistory = action.payload;
		},
		fetchMigrationHistoryFailure(state) {
			state.migrationHistoryLoading = false;
		},
		fetchDataHistory(state) {
			state.dataHistoryLoading = true;
		},
		fetchDataHistorySuccess(state, action) {
			state.dataHistoryLoading = false;
			state.dataHistory = action.payload;
		},
		fetchDataHistoryFailure(state) {
			state.dataHistoryLoading = false;
		},
	},
});

export default detailsSlice;