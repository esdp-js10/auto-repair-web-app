const {createSlice} = require("@reduxjs/toolkit");

const name = "GDT";

const GDTsSlice = createSlice({
  name,
  initialState: {
    gtds: [],
    data: null,
    GDT: null,
    searchOptions: null,
    fetchLoading: false,
    fetchError: null,
    fetchGTDsLoading: false,
    fetchGTDsError: null,
    fetchGTDLoading: false,
    fetchGTDError: null,
    fetchOptionsLoading: false,
    fetchOptionsError: null,
    createGDTLoading: false,
    createGDTError: null,
    changeLoading: false,
    changeError: null,
  },
  reducers: {
    fetchGDTs(state) {
      state.fetchLoading = true;
      state.fetchError = null;
    },
    fetchGDTsSuccess(state, {payload}) {
      state.data = payload;
      state.fetchLoading = false;
    },
    fetchGDTsFailure(state, {payload}) {
      state.fetchError = payload;
      state.fetchLoading = false;
    },
    fetchGDTRequest(state) {
      state.fetchGDTLoading = true;
      state.fetchGDTError = null;
    },
    fetchGDTSuccess(state, {payload}) {
      state.GDT = payload;
      state.fetchGDTLoading = false;
    },
    fetchGDTFailure(state, {payload}) {
      state.fetchGDTError = payload;
      state.fetchGDTLoading = false;
    },
    fetchSearchOptions(state) {
      state.fetchOptionsLoading = true;
      state.fetchOptionsError = null;
    },
    fetchSearchOptionsSuccess(state, {payload}) {
      state.searchOptions = payload;
      state.fetchOptionsLoading = false;
    },
    fetchSearchOptionsFailure(state, {payload}) {
      state.fetchOptionsError = payload;
      state.fetchOptionsLoading = false;
    },
    fetchGtdsRequest(state){
      state.fetchGTDsLoading = true;
    },
    fetchGtdsSuccess(state, {payload: gtds}){
      state.fetchGTDsLoading = false;
      state.gtds = gtds;
      state.fetchGTDsError = null;
    },
    fetchGtdsFailure(state, {payload: error}){
      state.fetchGTDsLoading = false;
      state.fetchGTDsError = error;
    },
    createGDTRequest(state, action){
      state.createGDTLoading = true;
    },
    createGDTSuccess(state, {payload: GDTData}){
      state.createGDTLoading = false;
      state.createGDTError = null;
      state.gtds = [...state.gtds, GDTData];
    },
    createGDTFailure(state, {payload: error}){
      state.createGDTLoading = false;
      state.createGDTError = error;
    },
    changeGDTRequest(state, action){
      state.changeLoading = true;
      state.changeError = null;
    },
    changeGDTSuccess(state, action){
      state.changeLoading = false;
      state.changeError = null;
    },
    changeGDTFailure(state, {payload: error}){
      state.changeLoading = false;
      state.changeError = error;
    }
  },
});

export default GDTsSlice;
