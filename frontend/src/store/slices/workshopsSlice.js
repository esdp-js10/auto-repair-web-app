import {createSlice} from "@reduxjs/toolkit";

const name = 'workshops';

const workshopsSlice = createSlice({
	name,
	initialState: {
		workshops: [],
		activeWorkshops: [],
		fetchWorkshopsLoading: false,
		fetchWorkshopsError: null,
    createWorkshopLoading: false,
    createWorkshopError: null,
    changeWorkshopLoading: false,
    changeWorkshopError: null,
    changeStatusLoading: false,
    changeStatusError: null,
		fetchWorkshopsByStatusLoading: false,
	},
	reducers: {
		fetchWorkshopsRequest(state) {
			state.fetchWorkshopsLoading = true;
		},
		fetchWorkshopsSuccess(state, {payload: workshopsData}) {
			state.workshops = workshopsData;
			state.fetchWorkshopsLoading = false;
			state.fetchWorkshopsError = null;
		},
		fetchWorkshopsFailure(state, {payload: error}) {
			state.fetchWorkshopsLoading = false;
			state.fetchWorkshopsError = error;
		},
    changeWorkshopRequest(state) {
      state.changeWorkshopLoading = true;
    },
    changeWorkshopSuccess(state, {payload: workshopData}) {
      state.workshops = state.workshops.map(w => w.id === workshopData.id ? workshopData : w);
      state.changeWorkshopLoading = false;
      state.changeWorkshopError = null;
    },
    changeWorkshopFailure(state, {payload: error}) {
      state.changeWorkshopLoading = false;
      state.changeWorkshopError = error;
    },
    createWorkshopRequest(state) {
      state.createWorkshopLoading = true;
    },
    createWorkshopSuccess(state, {payload: workshopData}) {
      state.workshops =[...state.workshops, workshopData];
      state.createWorkshopLoading = false;
      state.createWorkshopError = null;
    },
    createWorkshopFailure(state, {payload: error}) {
      state.createWorkshopLoading = false;
      state.createWorkshopError = error;
    },
    changeStatusWorkshopRequest(state){
      state.changeStatusLoading = true;
    },
    changeStatusWorkshopFailure(state, {payload: workshopData}){
      state.changeStatusLoading = false;
      state.workshops = state.workshops.map(w => w.id === workshopData.id ? workshopData : w);
      state.changeStatusError = null;
    },
    changeStatusWorkshopSuccess(state, {payload: error}){
      state.changeStatusLoading = false;
      state.changeStatusError = error;
    },
		fetchWorkshopsByStatus(state) {
			state.fetchWorkshopsByStatusLoading = true;
		},
		fetchWorkshopsByStatusSuccess(state, action) {
			state.fetchWorkshopsByStatusLoading = false;
			state.activeWorkshops = action.payload;
		},
		fetchWorkshopsByStatusFailure(state) {
			state.fetchWorkshopsByStatusLoading = false;
		},
	}
});

export default workshopsSlice;