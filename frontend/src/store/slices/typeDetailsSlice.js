import {createSlice} from "@reduxjs/toolkit";

const name = 'typeDetails';

const typeDetailsSlice = createSlice({
	name,
	initialState: {
		types: [],
    type: {},
		fetchTypesDetailsLoading: false,
    createDetailError: null,
    createDetailLoading: false,
    editTypeDetailLoading: false,
    editTypeDetailError: null,
	},
	reducers: {
		fetchTypesDetailsRequest(state) {
			state.fetchTypesDetailsLoading = true;
		},
		fetchTypesDetailsSuccess(state, action) {
			state.types = action.payload;
			state.fetchTypesDetailsLoading = false;
		},
		fetchTypesDetailsFailure(state) {
			state.fetchTypesDetailsLoading = false;
		},
    fetchTypesDetailRequest(state) {
      state.fetchTypesDetailsLoading = true;
    },
    fetchTypesDetailSuccess(state, action) {
      state.type = action.payload;
      state.fetchTypesDetailsLoading = false;
    },
    fetchTypesDetailFailure(state) {
      state.fetchTypesDetailsLoading = false;
    },
    createDetailRequest(state) {
      state.createDetailLoading = true;
    },
    createDetailSuccess(state) {
      state.createDetailLoading = false;
      state.createDetailError = null;
    },
    createDetailFailure(state, {payload: error}) {
      state.createDetailLoading = false;
      state.createDetailError = error;
    },
    editTypeDetailRequest(state) {
      state.editTypeDetailLoading = true;
    },
    editTypeDetailSuccess(state) {
      state.editTypeDetailLoading = false;
      state.editTypeDetailError = null;
    },
    editTypeDetailFailure(state, {payload: error}) {
      state.editTypeDetailLoading = false;
      state.editTypeDetailError = error;
    },
	}
});

export default typeDetailsSlice;