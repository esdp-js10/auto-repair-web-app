import {createSlice} from "@reduxjs/toolkit";

const name = 'news';

const newsSlice = createSlice({
  name,
  initialState: {
    news: [],
    fetchNewsLoading: false,
    fetchNewsError: null,
    createNewsLoading: false,
    createNewsError: null,
    changeNewsLoading: false,
    changeNewsError: null,
    removeNewsLoading: false,
    removeNewsError: null,
  },
  reducers: {
    fetchNewsRequest(state){
      state.fetchNewsLoading = true;
    },
    fetchNewsSuccess(state, {payload: newsData}){
      state.fetchNewsLoading = false;
      state.news = newsData;
      state.fetchNewsError = null;
    },
    fetchNewsFailure(state, {payload: error}){
      state.fetchNewsLoading = false;
      state.fetchNewsError = error;
    },
    createNewsRequest(state){
      state.createNewsLoading = false;
    },
    createNewsSuccess(state, {payload: latestNews}){
      state.createNewsLoading = false;
      state.news = [...state.news, latestNews];
      state.createNewsError = null;
    },
    createNewsFailure(state, {payload: error}){
      state.createNewsLoading = false;
      state.createNewsError = error;
    },
    changeNewsRequest(state){
      state.changeNewsLoading = true;
    },
    changeNewsSuccess(state, {payload: changedNews}){
      state.changeNewsLoading = false;
      state.news = state.news.map(n => n._id === changedNews._id ? changedNews : n);
      state.changeNewsError = null;
    },
    changeNewsFailure(state, {payload: error}){
      state.changeNewsError = error;
      state.changeNewsLoading = false;
    },
    removeNewsRequest(state){
      state.removeNewsLoading = true;
    },
    removeNewsSuccess(state, {payload: id}){
      state.removeNewsLoading = false;
      state.news = state.news.filter(n => n._id !== id);
      state.removeNewsError = null;
    },
    removeNewsFailure(state, {payload: error}){
      state.removeNewsLoading = false;
      state.removeNewsError = error;
    },
  }
});

export default newsSlice;