const {createSlice} = require("@reduxjs/toolkit");

const name = 'employees';

const employeeSlice = createSlice({
	name,
	initialState: {
		employee: {},
		employees: [],
		employeesByRole: [],
		createEmployeeLoading: false,
		createEmployeeError: null,
		fetchEmployeesLoading: false,
		fetchEmployeesError: null,
		fetchEmployeeLoading: false,
		fetchEmployeeError: null,
		editEmployeeLoading: false,
		editEmployeeError: null,
		changeStatusLoading: false,
		changeStatusError: null,
		fetchEmployeesByRoleLoading: false,
	},
	reducers: {
		createEmployee(state) {
			state.createEmployeeLoading = true;
		},
		createEmployeeSuccess(state) {
			state.createEmployeeLoading = false;
			state.createEmployeeError = null;
		},
		createEmployeeFailure(state, action) {
			state.createEmployeeLoading = false;
			state.createEmployeeError = action.payload;
		},
		fetchEmployees(state) {
			state.fetchEmployeesLoading = true;
		},
		fetchEmployeesSuccess(state, action) {
			state.fetchEmployeesLoading = false;
			state.fetchEmployeesError = null;
			state.employees = action.payload;
		},
		fetchEmployeesFailure(state, action) {
			state.fetchEmployeesLoading = false;
			state.fetchEmployeesError = action.payload;
		},
		fetchEmployee(state) {
			state.fetchEmployeeLoading = true;
		},
		fetchEmployeeSuccess(state, action) {
			state.fetchEmployeeLoading = false;
			state.fetchEmployeeError = null;
			state.employee = action.payload;
		},
		fetchEmployeeFailure(state, action) {
			state.fetchEmployeeLoading = false;
			state.fetchEmployeeError = action.payload;
		},
		editEmployee(state) {
			state.editEmployeeLoading = true;
		},
		editEmployeeSuccess(state) {
			state.editEmployeeLoading = false;
			state.editEmployeeError = null;
		},
		editEmployeeFailure(state, action) {
			state.editEmployeeLoading = false;
			state.editEmployeeError = action.payload;
		},
		changeStatus(state) {
			state.changeStatusLoading = true;
		},
		changeStatusSuccess(state, action) {
			state.changeStatusLoading = false;
			state.employee.status = action.payload.status;
		},
		changeStatusFailure(state, action) {
			state.changeStatusLoading = false;
			state.changeStatusError = action.payload;
		},
		fetchEmployeesByRole(state) {
			state.fetchEmployeesByRoleLoading = true;
		},
		fetchEmployeesByRoleSuccess(state, action) {
			state.employeesByRole = action.payload;
			state.fetchEmployeesByRoleLoading = false;
		},
		fetchEmployeesByRoleFailure(state) {
			state.fetchEmployeesByRoleLoading = false;
		},
	},
});

export default employeeSlice;