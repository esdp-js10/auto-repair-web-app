import {createSlice} from "@reduxjs/toolkit";

const name = 'partNumberDetails';

const partNumberDetailsSlice = createSlice({
	name,
	initialState: {
		partNumbers: [],
    partNumbersOfDetail: [],
		fetchPartNumbersDetailsLoading: false,
    addPartNumberLoading: false,
    addPartNumberError: null,
    editPartNumberLoading: false,
    editPartNumberError: null,
	},
	reducers: {
		fetchPartNumbersDetailsRequest(state) {
			state.fetchPartNumbersDetailsLoading = true;
		},
		fetchPartNumbersDetailsSuccess(state, action) {
			state.partNumbers = action.payload;
			state.fetchPartNumbersDetailsLoading = false;
		},
		fetchPartNumbersDetailsFailure(state) {
			state.fetchPartNumbersDetailsLoading = false;
		},
    fetchPartNumbersOfDetailsRequest(state) {
      state.fetchPartNumbersDetailsLoading = true;
    },
    fetchPartNumbersOfDetailsSuccess(state, action) {
      state.partNumbersOfDetail = action.payload;
      state.fetchPartNumbersDetailsLoading = false;
    },
    fetchPartNumbersOfDetailsFailure(state) {
      state.fetchPartNumbersDetailsLoading = false;
    },
    addPartNumberRequest(state) {
      state.addPartNumberLoading = true;
    },
    addPartNumberSuccess(state) {
      state.addPartNumberLoading = false;
    },
    addPartNumberFailure(state, action) {
      state.addPartNumberLoading = false;
      state.addPartNumberError = action.payload;
    },
    editPartNumberRequest(state) {
      state.editPartNumberLoading = true;
    },
    editPartNumberSuccess(state) {
      state.editPartNumberLoading = false;
      state.editPartNumberError = null;
    },
    editPartNumberFailure(state, {payload: error}) {
      state.editPartNumberLoading = false;
      state.editPartNumberError = error;
    }
	}
});

export default partNumberDetailsSlice;