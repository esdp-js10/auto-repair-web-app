import {createSlice} from "@reduxjs/toolkit";

const name = 'orders';

const ordersSlice = createSlice({
  name,
  initialState: {
    orders: [],
    ordersHistory: [],
    order: null,
    orderCode: null,
    fetchOrdersLoading: false,
    fetchOrdersError: null,
    fetchOrderLoading: false,
    fetchOrderError: null,
    fetchOrderCodeLoading: false,
    fetchOrderCodeError: null,
    fetchOrdersHistoryLoading: false,
    fetchOrdersHistoryError: null,
    createOrderLoading: false,
    createOrderError: null,
    changeOrderLoading: false,
    changeOrderError: null,
    changeStatusLoading: false,
    changeStatusError: null,
    removeOrderLoading: false,
    removeOrderError: null,
  },
  reducers: {
    fetchOrdersRequest(state){
      state.fetchOrdersLoading = true;
    },
    fetchOrdersSuccess(state, {payload: orders}){
      state.fetchOrdersLoading = false;
      state.orders = orders;
      state.fetchOrdersError = null;
    },
    fetchOrdersFailure(state, {payload: error}){
      state.fetchOrdersLoading = false;
      state.fetchOrdersError = error;
    },
    fetchOrderRequest(state){
      state.fetchOrderLoading = true;
    },
    fetchOrderSuccess(state, {payload: order}){
      state.fetchOrderLoading = false;
      state.order = order;
      state.fetchOrderError = null;
    },
    fetchOrderFailure(state, {payload: error}){
      state.fetchOrderLoading = false;
      state.fetchOrderError = error;
    },
    fetchOrderCodeRequest(state){
      state.fetchOrderCodeLoading = true;
    },
    fetchOrderCodeSuccess(state, {payload: order}){
      state.fetchOrderCodeLoading = false;
      state.orderCode = order;
      state.fetchOrderCodeError = null;
    },
    fetchOrderCodeFailure(state, {payload: error}){
      state.fetchOrderCodeLoading = false;
      state.fetchOrderCodeError = error;
    },
    fetchOrdersHistoryRequest(state){
      state.fetchOrdersHistoryLoading = true;
    },
    fetchOrdersHistorySuccess(state, {payload: orders}){
      state.fetchOrdersHistoryLoading = false;
      state.ordersHistory = orders;
      state.fetchOrdersHistoryError = null;
    },
    fetchOrdersHistoryFailure(state, {payload: error}){
      state.fetchOrdersHistoryLoading = false;
      state.fetchOrdersHistoryError = error;
    },
    createOrderRequest(state){
      state.createOrdersLoading = false;
    },
    createOrderSuccess(state, {payload: order}){
      state.createOrderLoading = false;
      state.orders = [...state.orders, order];
      state.createOrderError = null;
    },
    createOrderFailure(state, {payload: error}){
      state.createOrderLoading = false;
      state.createOrderError = error;
    },
    changeOrderRequest(state){
      state.changeOrderLoading = true;
    },
    changeOrderSuccess(state, {payload: changedOrder}){
      state.changeOrderLoading = false;
      state.orders = state.orders.map(n => n._id === changedOrder._id ? changedOrder : n);
      state.changeOrderError = null;
    },
    changeOrderFailure(state, {payload: error}){
      state.changeOrderError = error;
      state.changeOrderLoading = false;
    },
    removeOrderRequest(state){
      state.removeOrderLoading = true;
    },
    removeOrderSuccess(state, {payload: id}){
      state.removeOrderLoading = false;
      state.orders = state.orders.filter(n => n._id !== id);
      state.removeOrderError = null;
    },
    removeOrderFailure(state, {payload: error}){
      state.removeOrderLoading = false;
      state.removeOrderError = error;
    },
    changeStatusRequest(state){
      state.changeStatusLoading = true;
    },
    changeStatusSuccess(state, {payload: changedOrder}){
      state.changeStatusLoading = false;
      state.orders = state.orders.map(o => o._id === changedOrder._id ? changedOrder : o);
    },
    changeStatusFailure(state, {payload: error}){
      state.changeStatusLoading = false;
      state.changeStatusError = error;
    },
    changeOrderStatus(state, action) {
      state.order.status = action.payload;
    }
  }
});

export default ordersSlice;
