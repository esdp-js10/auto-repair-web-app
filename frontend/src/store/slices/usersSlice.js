import {createSlice} from "@reduxjs/toolkit";

const name = 'users';

export const initialState = {
  profile: null,
  reqRequests: null,
  deleteUserLoading: false,
  userStatusChangeLoading: false,
  fetchRegRequestsLoading: false,
  signUpLoading: false,
  logInLoading: false,
  logOutLoading: false,
  errSignUp: null,
  errLogIn: null,
  errLogOut: null,
  errFetchRegRequests: null,
  errUserStatusChange: null,
  errDeleteUser: null,
	updateUserLoading: false,
	updateUserError: null,
};

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    checkAuth(state) {
      state.logInLoading = true;
      state.errLogIn = null;
    },
    registerUser(state) {
      state.signUpLoading = true;
      state.errSignUp = null;
    },
    registerUserSuccess(state) {
      state.signUpLoading = false;
    },
    registerUserFailure(state, {payload}) {
      state.signUpLoading = false;
      state.errSignUp = payload;
    },
    logInUser(state) {
      state.logInLoading = true;
      state.errLogIn = null;
    },
    logInUserSuccess(state, {payload}) {
      state.profile = payload;
      state.logInLoading = false;
    },
    logInUserFailure(state, {payload}) {
      state.logInLoading = false;
      state.errLogIn = payload;
    },
    logOutUser(state) {
      state.logOutLoading = true;
      state.errLogOut = null;
    },
    logOutUserSuccess(state) {
      state.profile = null;
      state.logOutLoading = false;
    },
    logOutUserFailure(state, {payload}) {
      state.logOutLoading = false;
      state.errLogOut = payload;
    },
    logInEmployee(state) {
      state.logInLoading = true;
      state.errLogIn = null;
    },
    logInEmployeeSuccess(state, {payload}) {
      state.profile = payload;
      state.logInLoading = false;
    },
    logInEmployeeFailure(state, {payload}) {
      state.logInLoading = false;
      state.errLogIn = payload;
    },
    logOutEmployee(state) {
      state.logOutLoading = true;
      state.errLogOut = null;
    },
    logOutEmployeeSuccess(state) {
      state.profile = null;
      state.logOutLoading = false;
    },
    logOutEmployeeFailure(state, {payload}) {
      state.logOutLoading = false;
      state.errLogOut = payload;
    },
    fetchRegRequests(state) {
      state.fetchRegRequestsLoading = true;
      state.errFetchRegRequests = null;
    },
    fetchRegRequestsSuccess(state, {payload}) {
      state.reqRequests = payload;
      state.fetchRegRequestsLoading = false;
    },
    fetchRegRequestsFailure(state, {payload}) {
      state.errFetchRegRequests = payload;
      state.fetchRegRequestsLoading = false;
    },
    userStatusChange(state) {
      state.userStatusChangeLoading = true;
      state.errUserStatusChange = null;
    },
    userStatusChangeSuccess(state) {
      state.userStatusChangeLoading = false;
    },
    userStatusChangeFailure(state, {payload}) {
      state.errUserStatusChange = payload;
      state.userStatusChangeLoading = false;
    },
    deleteUser(state) {
      state.deleteUserLoading = true;
      state.errDeleteUser = null;
    },
    deleteUserSuccess(state) {
      state.deleteUserLoading = false;
    },
    deleteUserFailure(state, {payload}) {
      state.errDeleteUser = payload;
      state.deleteUserLoading = false;
    },
	  updateUser(state) {
    	state.updateUserLoading = true;
	  },
	  updateUserSuccess(state, {payload}) {
    	state.updateUserLoading = false;
    	state.updateUserError = null;
    	state.profile = payload;
	  },
	  updateUserFailure(state, {payload}) {
    	state.updateUserLoading = false;
    	state.updateUserError = payload;
	  },
  },
});

export default usersSlice;