import repairerOrdersSlice from "../slices/repairerOrdersSlice";

export const {
  fetchRepairOrderData,
  fetchRepairOrderDataSuccess,
  fetchRepairOrderDataFailure,
  changeReservedItem,
  addNewReservedItem,
  removeReservedItem,
  submitItems,
  fetchAvailableParts,
  fetchAvailablePartsSuccess,
  changeExtras,
  addExtras,
  removeExtras,
  cleanRepairPageFormState,
  changeExtraStatus,
  submitItemsFailureSaga,
} = repairerOrdersSlice.actions;