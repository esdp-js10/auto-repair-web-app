import warehousesSlice from "../slices/warehousesSlice";

export const {
	fetchWarehousesRequest,
	fetchWarehousesSuccess,
	fetchWarehousesFailure,
  createWarehouseRequest,
  createWarehouseSuccess,
  createWarehouseFailure,
  editWarehouseRequest,
  editWarehouseSuccess,
  editWarehouseFailure,
  fetchWarehouseRequest,
  fetchWarehouseSuccess,
  fetchWarehouseFailure,
  fetchFreeWarehousesRequest,
  fetchFreeWarehousesSuccess,
  fetchFreeWarehousesFailure,
  deactivateWarehouseRequest,
  deactivateWarehouseSuccess,
  deactivateWarehouseFailure,
	fetchWarehousesByStatus,
	fetchWarehousesByStatusSuccess,
	fetchWarehousesByStatusFailure,
} = warehousesSlice.actions;