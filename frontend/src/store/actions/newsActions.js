import newsSlice from "../slices/newsSlice";

export const {
  fetchNewsRequest,
  fetchNewsSuccess,
  fetchNewsFailure,
  createNewsRequest,
  createNewsSuccess,
  createNewsFailure,
  changeNewsRequest,
  changeNewsSuccess,
  changeNewsFailure,
  removeNewsRequest,
  removeNewsSuccess,
  removeNewsFailure
} = newsSlice.actions;