import employeeSlice from "../slices/employeesSlice";

export const {
	createEmployee,
	createEmployeeSuccess,
	createEmployeeFailure,
	fetchEmployees,
	fetchEmployeesSuccess,
	fetchEmployeesFailure,
	fetchEmployee,
	fetchEmployeeSuccess,
	fetchEmployeeFailure,
	editEmployee,
	editEmployeeSuccess,
	editEmployeeFailure,
	changeStatus,
	changeStatusSuccess,
	changeStatusFailure,
	fetchEmployeesByRole,
	fetchEmployeesByRoleSuccess,
	fetchEmployeesByRoleFailure,
} = employeeSlice.actions;