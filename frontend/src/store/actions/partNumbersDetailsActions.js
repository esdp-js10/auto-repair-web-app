import partNumberDetailsSlice from "../slices/partNumbersDetailsSlice";

export const {
	fetchPartNumbersDetailsRequest,
	fetchPartNumbersDetailsSuccess,
	fetchPartNumbersDetailsFailure,
  fetchPartNumbersOfDetailsRequest,
  fetchPartNumbersOfDetailsSuccess,
  fetchPartNumbersOfDetailsFailure,
  addPartNumberRequest,
  addPartNumberSuccess,
  addPartNumberFailure,
  editPartNumberSuccess,
  editPartNumberRequest,
  editPartNumberFailure,
} = partNumberDetailsSlice.actions;