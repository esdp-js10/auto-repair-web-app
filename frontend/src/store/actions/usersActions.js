import usersSlice from "../slices/usersSlice";

export const {
  deleteUser,
  deleteUserSuccess,
  deleteUserFailure,
  userStatusChange,
  userStatusChangeSuccess,
  userStatusChangeFailure,
  registerUser,
  registerUserSuccess,
  registerUserFailure,
  logInUser,
  logInUserSuccess,
  logInUserFailure,
  logOutUser,
  logOutUserSuccess,
  logOutUserFailure,
  logInEmployee,
  logInEmployeeSuccess,
  logInEmployeeFailure,
  logOutEmployee,
  logOutEmployeeSuccess,
  logOutEmployeeFailure,
  fetchRegRequests,
  fetchRegRequestsSuccess,
  fetchRegRequestsFailure,
  updateUser,
  updateUserSuccess,
  updateUserFailure,
  checkAuth,
} = usersSlice.actions;