import detailsSlice from "../slices/detailsSlice";

export const {
	addDetailRequest,
	addDetailSuccess,
	addDetailFailure,
	fetchDetailsInTheWarehouse,
	fetchDetailsInTheWarehouseSuccess,
	fetchDetailsInTheWarehouseFailure,
	movingDetails,
	movingDetailsSuccess,
	movingDetailsFailure,
	fetchMigrationHistory,
	fetchMigrationHistorySuccess,
	fetchMigrationHistoryFailure,
	fetchDataHistory,
	fetchDataHistorySuccess,
	fetchDataHistoryFailure,
} = detailsSlice.actions;