import typeDetailsSlice from "../slices/typeDetailsSlice";

export const {
	fetchTypesDetailsRequest,
	fetchTypesDetailsSuccess,
	fetchTypesDetailsFailure,
  fetchTypesDetailRequest,
  fetchTypesDetailSuccess,
  fetchTypesDetailFailure,
  createDetailRequest,
  createDetailFailure,
  createDetailSuccess,
  editTypeDetailRequest,
  editTypeDetailSuccess,
  editTypeDetailFailure,
} = typeDetailsSlice.actions;