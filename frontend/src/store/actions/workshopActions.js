import workshopsSlice from "../slices/workshopsSlice";

export const {
	fetchWorkshopsRequest,
	fetchWorkshopsSuccess,
	fetchWorkshopsFailure,
  createWorkshopRequest,
  createWorkshopSuccess,
  createWorkshopFailure,
  changeWorkshopRequest,
  changeWorkshopSuccess,
  changeWorkshopFailure,
  changeStatusWorkshopRequest,
  changeStatusWorkshopSuccess,
  changeStatusWorkshopFailure,
	fetchWorkshopsByStatus,
	fetchWorkshopsByStatusSuccess,
	fetchWorkshopsByStatusFailure,
} = workshopsSlice.actions;