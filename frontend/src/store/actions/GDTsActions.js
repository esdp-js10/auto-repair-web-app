import GDTsSlice from "../slices/GDTsSlice";

export const {
  fetchGDTs,
  fetchGDTsSuccess,
  fetchGDTsFailure,
  fetchGDTRequest,
  fetchGDTSuccess,
  fetchGDTFailure,
  fetchSearchOptions,
  fetchSearchOptionsSuccess,
  fetchSearchOptionsFailure,
  fetchGtdsRequest,
  fetchGtdsSuccess,
  fetchGtdsFailure,
  createGDTRequest,
  createGDTSuccess,
  createGDTFailure,
  changeGDTRequest,
  changeGDTSuccess,
  changeGDTFailure,
} = GDTsSlice.actions;
