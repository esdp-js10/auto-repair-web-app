import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";
import {
	changeStatus, changeStatusFailure,
	changeStatusSuccess,
	createEmployee,
	createEmployeeFailure,
	createEmployeeSuccess,
	editEmployee,
	editEmployeeFailure,
	editEmployeeSuccess,
	fetchEmployee,
	fetchEmployeeFailure,
	fetchEmployees, fetchEmployeesByRole, fetchEmployeesByRoleFailure, fetchEmployeesByRoleSuccess,
	fetchEmployeesFailure,
	fetchEmployeesSuccess,
	fetchEmployeeSuccess,
} from "../actions/employeesActions";
import {CREATE_EMPLOYEE_URL} from "../../config";

export function* createEmployeesSagas({payload: employeeData}) {
	try {
		yield axiosApi.post(CREATE_EMPLOYEE_URL, employeeData);
		yield put(createEmployeeSuccess());
		yield put(historyPush('/admin/employees'));
		toast.success('Создан новый сотрудник!');
	} catch (error) {
		yield put(createEmployeeFailure(error.response.data));

		if (error.response.data.error) {
			toast.error(error.response.data.error);
		} else {
			toast.error('У вас есть ошибка, пожалуйста проверьте!');
		}
	}
}

export function* fetchEmployeesSagas() {
	try {
		const response = yield axiosApi.get('/employees');
		yield put(fetchEmployeesSuccess(response.data));
	} catch (error) {
		yield put(fetchEmployeesFailure(error));
	}
}

export function* fetchEmployeeSagas({payload: employeeId}) {
	try {
		const response = yield axiosApi.get('/employees' + employeeId);
		yield put(fetchEmployeeSuccess(response.data));
	} catch (error) {
		yield put(fetchEmployeeFailure(error));
		toast.error(error);
	}
}

export function* editEmployeeSagas({payload: employee}) {
	try {
		const response = yield axiosApi.put(`/employees${employee.id}`, employee.data);
		yield put(editEmployeeSuccess());
		yield put(historyPush('/admin/employees'));
		toast.success(response.data);
	} catch (error) {
		yield put(editEmployeeFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

export function* changeEmployeeStatusSagas({payload: employeeID}) {
	try {
		const response = yield axiosApi.put(`/employees/changeStatus/${employeeID}`);
		yield put(changeStatusSuccess(response.data));
		toast.success(response.data.status ? 'Успешно активирован!' : 'Успешно деактивирован!');
	} catch (error) {
		yield put(changeStatusFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

export function* fetchEmployeesByRoleSagas({payload: role}) {
	try {
		const response = yield axiosApi.get(`/employees?role=${role}`);
		yield put(fetchEmployeesByRoleSuccess(response.data));
	} catch (error) {
		yield put(fetchEmployeesByRoleFailure());
	}
}

const employeesSaga = [
	takeEvery(createEmployee, createEmployeesSagas),
	takeEvery(fetchEmployees, fetchEmployeesSagas),
	takeEvery(editEmployee, editEmployeeSagas),
	takeEvery(fetchEmployee, fetchEmployeeSagas),
	takeEvery(changeStatus, changeEmployeeStatusSagas),
	takeEvery(fetchEmployeesByRole, fetchEmployeesByRoleSagas),
];

export default employeesSaga;