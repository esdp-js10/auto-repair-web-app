import {put, takeEvery} from "redux-saga/effects";
import {
  changeGDTFailure,
  changeGDTRequest,
  changeGDTSuccess,
  createGDTFailure,
  createGDTRequest,
  createGDTSuccess,
  fetchGDTFailure,
  fetchGDTRequest,
  fetchGDTs,
  fetchGDTsFailure,
  fetchGDTsSuccess,
  fetchGDTSuccess,
  fetchGtdsFailure,
  fetchGtdsRequest,
  fetchGtdsSuccess,
  fetchSearchOptions,
  fetchSearchOptionsFailure,
  fetchSearchOptionsSuccess
} from "../actions/GDTsActions";
import {GDT_ALL_URL, GDT_SEARCH_OPTIONS_URL, GDT_URL} from "../../config";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {A_GDT_EDIT_FORM, CATALOG_PATH} from "../../constants/paths";
import {historyPush} from "../actions/historyActions";

export function* fetchGDTsSaga({payload}) {
  try {
    const {data} = yield axiosApi.get(GDT_URL + (payload ? payload : ''));
    yield put(fetchGDTsSuccess(data));
  } catch (err) {
    yield put(fetchGDTsFailure(err?.response?.data));
    if (!err.response) toast.error(err.message);
  }
}


export function* fetchSearchOptionsSaga() {
  try {
    const {data} = yield axiosApi.get(GDT_SEARCH_OPTIONS_URL);
    yield put(fetchSearchOptionsSuccess(data));
  } catch (err) {
    yield put(fetchSearchOptionsFailure(err?.response?.data));
    if (!err.response) toast.error(err.message);
  }
}

export function* fetchGtdsSaga() {
  try {
    const {data} = yield axiosApi.get(GDT_ALL_URL);
    yield put(fetchGtdsSuccess(data));
  } catch (error) {
    yield put(fetchGtdsFailure(error?.response?.data));
  }
}

export function* fetchGDTSaga({payload: id}) {
  try {
    const {data} = yield axiosApi.get(GDT_URL + '/one/' + id);
    yield put(fetchGDTSuccess(data));
    yield put(historyPush(A_GDT_EDIT_FORM + id));
  } catch (error) {
    yield put(fetchGDTFailure(error?.response?.data));
  }
}

export function* createGDTSaga({payload: info}) {
  try {
    const {data} = yield axiosApi.post(GDT_URL, info);
    yield put(createGDTSuccess(data));
    yield put(historyPush(CATALOG_PATH));
    toast.success('Успешно создано!');
  } catch (error) {
    toast.warning(error?.response?.data?.message);
    yield put(createGDTFailure(error?.response?.data));
  }
}

export function* changeGDTSaga({payload: info}) {
  try {
    const {data} = yield axiosApi.put(GDT_URL + '/' + info.id, info.data);
    yield put(changeGDTSuccess(data));
    yield put(historyPush(CATALOG_PATH));
    toast.success('Успешно изменено!');
  } catch (error) {
    toast.warning(error?.response?.data?.message);
    yield put(changeGDTFailure(error?.response?.data));
  }
}


const GDTsSagas = [
  takeEvery(fetchGDTs, fetchGDTsSaga),
  takeEvery(fetchGDTRequest, fetchGDTSaga),
  takeEvery(fetchGtdsRequest, fetchGtdsSaga),
  takeEvery(fetchSearchOptions, fetchSearchOptionsSaga),
  takeEvery(createGDTRequest, createGDTSaga),
  takeEvery(changeGDTRequest, changeGDTSaga),
];

export default GDTsSagas;
