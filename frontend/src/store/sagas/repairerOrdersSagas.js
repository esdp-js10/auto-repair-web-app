import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {
  fetchAvailableParts,
  fetchAvailablePartsSuccess,
  fetchRepairOrderData,
  fetchRepairOrderDataFailure,
  fetchRepairOrderDataSuccess,
  submitItems, submitItemsFailureSaga,
} from "../actions/repairerOrdersActions";
import {toast} from "react-toastify";


export function* fetchRepairOrderDataSaga({payload: orderId}) {
  try {
    const response = yield axiosApi.get(`/orderMaster/orderInfo?order=${orderId}`);
    yield put(fetchRepairOrderDataSuccess(response.data));
  } catch (e) {
    yield put(fetchRepairOrderDataFailure());
  }
}

export function* submitItemsSaga({payload: itemsOfOrder}) {
  try {
    yield axiosApi.post(`/orderMaster/changeItems`, itemsOfOrder);
    toast.success('Обновление данных по заказу прошло успешно!');
  }
  catch (e) {
    console.log(e.response.data);
    yield put(submitItemsFailureSaga(e.response.data));
    if (e.response.data.global) {
      toast.error(e.response.data.global);
    } else {
      toast.error('Обновление данных по заказу не произведено!');
    }
  }
}

export function* fetchAvailablePartsSaga ({payload: orderId}) {
  try {
    const response = yield axiosApi.get(`/orderMaster/availableDetails?order=${orderId}`);
    yield put(fetchAvailablePartsSuccess(response.data));
  }
  catch (e) {
    toast.error('Данные из доступных парт-номеров не обновлены');
  }
}

const repairerOrdersSaga = [
  takeEvery(fetchRepairOrderData, fetchRepairOrderDataSaga),
  takeEvery(submitItems, submitItemsSaga),
  takeEvery(fetchAvailableParts, fetchAvailablePartsSaga),
];

export default repairerOrdersSaga;