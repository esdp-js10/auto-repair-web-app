import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {
	createWarehouseFailure,
	createWarehouseRequest,
	createWarehouseSuccess,
	deactivateWarehouseFailure,
	deactivateWarehouseRequest,
	deactivateWarehouseSuccess,
	editWarehouseFailure,
	editWarehouseRequest,
	editWarehouseSuccess,
	fetchWarehouseFailure,
	fetchWarehouseRequest,
	fetchWarehousesFailure,
	fetchWarehousesRequest,
	fetchWarehousesSuccess,
	fetchWarehouseSuccess,
	fetchFreeWarehousesRequest,
	fetchFreeWarehousesSuccess,
	fetchFreeWarehousesFailure,
	fetchWarehousesByStatusSuccess, fetchWarehousesByStatusFailure, fetchWarehousesByStatus,
} from "../actions/warehousesActions";
import {toast} from "react-toastify";

export function* fetchWarehousesSaga() {
	try {
		const response = yield axiosApi.get('/warehouses');
		yield put(fetchWarehousesSuccess(response.data));
	} catch (error) {
		yield put(fetchWarehousesFailure(error.response.data.error));
	}
}

export function* fetchFreeWarehousesSaga() {
  try {
    const response = yield axiosApi.get('/warehouses/freeWarehouses');
    yield put(fetchFreeWarehousesSuccess(response.data));
  } catch (error) {
    yield put(fetchFreeWarehousesFailure(error.response.data.error));
  }
}

export function* fetchWarehouseSaga({payload: warehouseId}) {
  try {
    const response = yield axiosApi.get(`/warehouses?warehouse=${warehouseId}`);
    yield put(fetchWarehouseSuccess(response.data));
  } catch (error) {
    yield put(fetchWarehouseFailure(error.response.data.error));
  }
}

export function* createWarehouseSagas({payload}) {
  try {
    yield axiosApi.post('/warehouses/new', payload.data);
    yield put(createWarehouseSuccess());
    yield put(fetchWarehousesRequest());
	  payload.successCallback();
    toast.success('Склад создан');
  } catch (e) {
    if (e.response.data.error) {
    	toast.error(e.response.data.error)
    } else {
	    toast.error('Не получилось создать склад');
    }

	  yield put(createWarehouseFailure(e.response.data));
  }
}

export function* editWarehouseSagas({payload}) {
  try {
    yield axiosApi.put('/warehouses/changeName/'+ payload._id, payload.warehouse);
    yield put(editWarehouseSuccess());
    yield put(fetchWarehousesRequest());
    toast.success('Название склада измененно');
	  payload.successCallback();
  } catch (e) {
    if (e.response.data.error) {
      toast.error(e.response.data.error)
    } else {
      toast.error('Не получилось создать склад');
    }

    yield put(editWarehouseFailure(e.response.data));

  }
}

export function* deactivateWarehouseSagas({payload: id}) {
  try {
    yield axiosApi.put('/warehouses/changeStatus/' + id, null);
	  yield put(deactivateWarehouseSuccess());
  } catch (e) {
    yield put(deactivateWarehouseFailure(e.response.data));
    toast.error(e.response.data.error);
  }
}

export function* fetchWarehousesByStatusSaga({payload: status}) {
	try {
		const response = yield axiosApi.get(`/warehouses?status=${status}`);
		yield put(fetchWarehousesByStatusSuccess(response.data));
	} catch (error) {
		yield put(fetchWarehousesByStatusFailure());
	}
}

const warehousesSaga = [
	takeEvery(fetchWarehousesRequest, fetchWarehousesSaga),
	takeEvery(fetchFreeWarehousesRequest, fetchFreeWarehousesSaga),
  takeEvery(createWarehouseRequest, createWarehouseSagas),
  takeEvery(fetchWarehouseRequest, fetchWarehouseSaga),
  takeEvery(editWarehouseRequest, editWarehouseSagas),
  takeEvery(deactivateWarehouseRequest, deactivateWarehouseSagas),
	takeEvery(fetchWarehousesByStatus, fetchWarehousesByStatusSaga),
];

export default warehousesSaga;