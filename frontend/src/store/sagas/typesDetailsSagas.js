import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
  createDetailFailure,
  createDetailRequest,
  createDetailSuccess,
  editTypeDetailFailure,
  editTypeDetailRequest,
  editTypeDetailSuccess,
  fetchTypesDetailFailure,
  fetchTypesDetailRequest,
  fetchTypesDetailsFailure,
  fetchTypesDetailsRequest,
  fetchTypesDetailsSuccess,
  fetchTypesDetailSuccess
} from "../actions/typesDetailsActions";

export function* fetchTypesDetailsSagas() {
	try {
		const response = yield axiosApi.get('/typeDetails');
		yield put(fetchTypesDetailsSuccess(response.data));
	} catch (error) {
		yield put(fetchTypesDetailsFailure());
		toast.error(error);
	}
}

export function* fetchTypesDetailSagas({payload: detail}) {
  try {
    const response = yield axiosApi.get(`/typeDetails${detail}`);
    yield put(fetchTypesDetailSuccess(response.data));
  } catch (error) {
    yield put(fetchTypesDetailFailure());
    toast.error(error);
  }
}
export function* createDetailSagas({payload}) {
  try {
    yield axiosApi.post('/typeDetails', payload.data);
    yield put(createDetailSuccess());
    yield put(fetchTypesDetailsRequest());
    payload.successCallback();
    toast.success('Деталь создана');
  } catch (e) {
    if (e.response.data.error) {
      toast.error(e.response.data.error)
    } else {
      toast.error('Не получилось создать деталь');
    }

    yield put(createDetailFailure(e.response.data));
  }
}

export function* editTypeDetailSagas({payload}) {
  try {
    yield axiosApi.put('/typeDetails/changeType/' + payload._id, payload.typeDetail)
    yield put(editTypeDetailSuccess());
    yield put(fetchTypesDetailsRequest());
    toast.success('Название типа детали измененно');
    payload.successCallback();
  } catch (e) {
    if (e.response.data.error) {
      toast.error(e.response.data.error)
    }
    yield put(editTypeDetailFailure(e.response.data));

  }
}

const typesDetailsSaga = [
	takeEvery(fetchTypesDetailsRequest, fetchTypesDetailsSagas),
	takeEvery(fetchTypesDetailRequest, fetchTypesDetailSagas),
	takeEvery(createDetailRequest, createDetailSagas),
	takeEvery(editTypeDetailRequest, editTypeDetailSagas),
];

export default typesDetailsSaga;