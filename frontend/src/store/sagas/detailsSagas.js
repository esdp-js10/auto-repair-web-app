import {put, takeEvery} from "redux-saga/effects";
import {
	addDetailFailure,
	addDetailRequest,
	addDetailSuccess,
	fetchDataHistory,
	fetchDataHistoryFailure,
	fetchDataHistorySuccess,
	fetchDetailsInTheWarehouse,
	fetchDetailsInTheWarehouseFailure,
	fetchDetailsInTheWarehouseSuccess,
	fetchMigrationHistory,
	fetchMigrationHistoryFailure,
	fetchMigrationHistorySuccess,
	movingDetails,
	movingDetailsFailure,
	movingDetailsSuccess,
} from "../actions/detailsActions";
import axiosApi from "../../axiosApi";
import {historyBack, historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* addDetailSaga({payload: detailData}) {
	try {
		yield axiosApi.post(`/details${detailData.id}`, detailData.data);
		yield put(addDetailSuccess());

		if (detailData.role === 'admin') {
			yield put(historyPush('/admin/warehouses'));
		} else {
			yield put(historyBack());
		}

		toast.success('Данные успешно сохранены!');
	} catch (error) {
		yield put(addDetailFailure(error.response.data));
		if (error.response.data.error) {
			toast.error(error.response.data.error);
		} else {
			toast.error('Заполните все обязательные поля!');
		}
	}
}

export function* fetchDetailsInTheWarehouseSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`detailsInTheWarehouses?warehouse=${id}`);
		yield put(fetchDetailsInTheWarehouseSuccess(response.data));
	} catch (error) {
		yield put(fetchDetailsInTheWarehouseFailure(error.response.data.error));
	}
}

export function* movingDetailsSaga({payload}) {
	try {
		yield axiosApi.post(`/details/moving`, payload.data);
		yield put(movingDetailsSuccess(payload.data.details));
		payload.successCallback();
		toast.success('Детали успешно перемещены!');
	} catch (error) {
		yield put(movingDetailsFailure(error.response.data.error));
		toast.error(error.response.data.error);
	}
}

export function* fetchMigrationsHistorySaga() {
	try {
		const response = yield axiosApi.get('accountsDetails/migration');
		yield put(fetchMigrationHistorySuccess(response.data));
	} catch (error) {
		yield put(fetchMigrationHistoryFailure());
	}
}

export function* fetchDataHistorySaga({payload: action}) {
	try {
		const response = yield axiosApi.get(`accountsDetails?typeOfAction=${action}`);
		yield put(fetchDataHistorySuccess(response.data));
	} catch (error) {
		yield put(fetchDataHistoryFailure());
	}
}

const detailsSagas = [
	takeEvery(addDetailRequest, addDetailSaga),
	takeEvery(fetchDetailsInTheWarehouse, fetchDetailsInTheWarehouseSaga),
	takeEvery(movingDetails, movingDetailsSaga),
	takeEvery(fetchMigrationHistory, fetchMigrationsHistorySaga),
	takeEvery(fetchDataHistory, fetchDataHistorySaga),
];

export default detailsSagas;