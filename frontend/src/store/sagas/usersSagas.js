import {put, takeEvery} from "redux-saga/effects";
import {
  checkAuth,
	deleteUser,
	deleteUserFailure,
	deleteUserSuccess,
	fetchRegRequests,
	fetchRegRequestsFailure,
	fetchRegRequestsSuccess,
	logInEmployee,
	logInEmployeeFailure,
	logInEmployeeSuccess,
	logInUser,
	logInUserFailure,
	logInUserSuccess,
	logOutEmployee,
	logOutEmployeeFailure,
	logOutEmployeeSuccess,
	logOutUser,
	logOutUserFailure,
	logOutUserSuccess,
	registerUser,
	registerUserFailure,
	registerUserSuccess, updateUser, updateUserFailure, updateUserSuccess,
	userStatusChange,
	userStatusChangeFailure,
	userStatusChangeSuccess
} from "../actions/usersActions";
import {historyReplace} from "../actions/historyActions";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {
  BASE_URL,
  EMPLOYEES_LOG_IN_URL, USER_REFRESH_TOKEN_URL,
  USER_STATUS_CHANGE_URL, USER_LOG_IN_URL,
  USERS_REG_REQUESTS_URL,
  USERS_URL, USER_REG_URL, USER_LOG_OUT_URL, EMPLOYEE_LOG_OUT_URL
} from "../../config";
import {ADMIN_ROLE, REPAIRER_ROLE, WAREHOUSEMAN_ROLE} from "../../constants/userRoles";
import {A_HOME_PATH, R_HOME_PATH, W_HOME_PATH} from "../../constants/paths";
import axios from "axios";

export function* logInUserSaga({payload: userData}) {
  try {
    const {data} = yield axiosApi.post(USER_LOG_IN_URL, userData);
    localStorage.setItem('gdt_token', data.accessToken);
    yield put(logInUserSuccess(data));
    yield put(historyReplace('/'));
  } catch (err) {
    if (!err.response ) toast.error(err.message);
    if (err?.response?.status === 403) toast.error(err.response?.data?.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logInUserFailure(err.response?.data));
  }
}

export function* registerUserSaga({payload: userData}) {
  try {
    const {data} = yield axiosApi.post(USER_REG_URL, userData);
    localStorage.setItem('gdt_token', data.accessToken);
    yield put(registerUserSuccess());
    yield put(historyReplace('/'));
    toast.warning('Регистрация принята. С вами свяжется администратор для потверждения регистрации');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(registerUserFailure(err.response?.data));
  }
}

export function* checkAuthSaga() {
  try {
    const {data} = yield axios.get(BASE_URL + USER_REFRESH_TOKEN_URL, {withCredentials: true});
    localStorage.setItem('gdt_token', data.accessToken);
    yield put(logInUserSuccess(data));
  } catch (err) {
    yield put(logInUserFailure(err.response?.data));
  }
}

export function* logOutUserSaga({payload: userData}) {
  try {
    yield axiosApi.delete(USER_LOG_OUT_URL, userData);
    localStorage.removeItem('gdt_token');
    yield put(logOutUserSuccess());
    toast.success('Вы успешно вышли из системы');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logOutUserFailure(err.response?.data));
  }
}

export function* logInEmployeeSaga({payload: userData}) {
  try {
    const {data} = yield axiosApi.post(EMPLOYEES_LOG_IN_URL, userData);
    yield put(logInEmployeeSuccess(data));
    switch (data?.role) {
      case ADMIN_ROLE: {
        yield put(historyReplace(A_HOME_PATH));
        break;
      }
      case REPAIRER_ROLE: {
        yield put(historyReplace(R_HOME_PATH));
        break;
      }
      case WAREHOUSEMAN_ROLE: {
        yield put(historyReplace(W_HOME_PATH));
        break;
      }
    }
  } catch (err) {
    if (!err?.response) toast.error(err.message);
    if (err?.response?.status === 403) toast.error(err.response.data.message);
    if (err?.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logInEmployeeFailure(err.response?.data));
  }
}

export function* logOutEmployeeSaga({payload: userData}) {
  try {
    yield axiosApi.delete(EMPLOYEE_LOG_OUT_URL, userData);
    yield put(logOutEmployeeSuccess());
    yield put(historyReplace('/'));
    toast.success('Вы успешно вышли из системы');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(logOutEmployeeFailure(err.response?.data));
  }
}

export function* fetchRegRequestsSaga() {
  try {
    const {data: regRequests} = yield axiosApi.get(USERS_REG_REQUESTS_URL);
    yield put(fetchRegRequestsSuccess(regRequests));
  } catch (err) {
    yield put(fetchRegRequestsFailure(err.response?.data));
  }
}

export function* userStatusChangeSaga({payload}) {
  try {
    yield axiosApi.post(USER_STATUS_CHANGE_URL + payload.id);
    yield put(userStatusChangeSuccess());
    yield put(fetchRegRequests());
    payload.successCallback();
    toast.success('Статус пользователя изменён')
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err?.response?.data?.global) toast.error(err.response.data.global);
    yield put(userStatusChangeFailure(err.response?.data));
  }
}

export function* deleteUserSaga({payload}) {
  try {
    yield axiosApi.delete(USERS_URL + '/' + payload.id);
    yield put(deleteUserSuccess());
    yield put(fetchRegRequests());
    payload.successCallback();
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err?.response?.data?.global) toast.error(err.response.data.global);
    yield put(deleteUserFailure(err.response?.data));
  }
}

export function* updateUserSaga({payload: data}) {
	try {
		const response = yield axiosApi.put(USERS_URL + `?user=${data.id}`, data.data);
    localStorage.setItem('gdt_token', response.data.accessToken);
    yield put(updateUserSuccess(response.data));
		yield put(historyReplace('/'));
		toast.success('Ваши данные успешно изменены!');
	} catch (error) {
		yield put(updateUserFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

const usersSagas = [
  takeEvery(checkAuth, checkAuthSaga ),
  takeEvery(registerUser, registerUserSaga),
  takeEvery(logInUser, logInUserSaga),
  takeEvery(logOutUser, logOutUserSaga),
  takeEvery(logInEmployee, logInEmployeeSaga),
  takeEvery(logOutEmployee, logOutEmployeeSaga),
  takeEvery(fetchRegRequests, fetchRegRequestsSaga),
  takeEvery(userStatusChange, userStatusChangeSaga),
  takeEvery(deleteUser, deleteUserSaga),
	takeEvery(updateUser, updateUserSaga),
];

export default usersSagas;