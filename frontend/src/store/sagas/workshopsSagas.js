import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
	fetchWorkshopsFailure,
	fetchWorkshopsRequest,
	fetchWorkshopsSuccess,
	createWorkshopRequest,
	createWorkshopSuccess,
	createWorkshopFailure,
	changeWorkshopRequest,
	changeWorkshopSuccess,
	changeWorkshopFailure,
	changeStatusWorkshopRequest,
	changeStatusWorkshopSuccess,
	changeStatusWorkshopFailure, fetchWorkshopsByStatusSuccess, fetchWorkshopsByStatusFailure, fetchWorkshopsByStatus
} from "../actions/workshopActions";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";
import {A_WORKSHOPS_PATH} from "../../constants/paths";

export function* fetchWorkshopsSaga() {
	try {
		const {data} = yield axiosApi.get('/workshops');
		yield put(fetchWorkshopsSuccess(data));
	} catch (error) {
		yield put(fetchWorkshopsFailure(error.response.data.error));
	}
}
export function* createWorkshopSaga({payload: newWorkshop}) {
	try {
		const {data} = yield axiosApi.post('/workshops/new', newWorkshop);
		yield put(createWorkshopSuccess(data));
    yield put(historyPush(A_WORKSHOPS_PATH));
	} catch (error) {
    toast.error(error?.response?.data?.errors?.name?.message || error?.response?.data);
		yield put(createWorkshopFailure(error?.response?.data?.errors?.name?.message || error?.response?.data));
	}
}
export function* changeWorkshopSaga({payload}) {
	try {
		const {data} = yield axiosApi.put('/workshops/changeName/' + payload._id, payload.data);
		yield put(changeWorkshopSuccess(data));
    yield put(historyPush(A_WORKSHOPS_PATH));
	} catch (error) {
    toast.error(error?.response?.data?.errors?.name?.message || error?.response?.data);
    yield put(changeWorkshopFailure(error?.response?.data?.errors?.name?.message || error?.response?.data));
	}
}

export function* changeStatusWorkshopSaga({payload: id}) {
	try {
		const {data} = yield axiosApi.put('/workshops/changeStatus/' + id);
		yield put(changeStatusWorkshopSuccess(data));
    yield put(historyPush(A_WORKSHOPS_PATH));
	} catch (error) {
		yield put(changeStatusWorkshopFailure(error.response.data.error));
	}
}

export function* fetchWorkshopsByStatusSaga({payload: status}) {
	try {
		const response = yield axiosApi.get(`/workshops?status=${status}`);
		yield put(fetchWorkshopsByStatusSuccess(response.data));
	} catch (error) {
		yield put(fetchWorkshopsByStatusFailure());
	}
}

const workshopsSagas = [
	takeEvery(fetchWorkshopsRequest, fetchWorkshopsSaga),
	takeEvery(createWorkshopRequest, createWorkshopSaga),
	takeEvery(changeWorkshopRequest, changeWorkshopSaga),
	takeEvery(changeStatusWorkshopRequest, changeStatusWorkshopSaga),
	takeEvery(fetchWorkshopsByStatus, fetchWorkshopsByStatusSaga),
];

export default workshopsSagas;