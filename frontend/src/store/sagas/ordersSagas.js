import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
  fetchOrdersSuccess,
  fetchOrdersRequest,
  fetchOrdersFailure,
  fetchOrderRequest,
  fetchOrderSuccess,
  fetchOrderFailure,
  fetchOrderCodeRequest,
  fetchOrderCodeSuccess,
  fetchOrderCodeFailure,
  fetchOrdersHistoryRequest,
  fetchOrdersHistorySuccess,
  fetchOrdersHistoryFailure,
  createOrderRequest,
  createOrderFailure,
  createOrderSuccess,
  changeOrderSuccess,
  changeOrderFailure,
  changeOrderRequest,
  changeStatusSuccess,
  changeStatusFailure,
  changeStatusRequest,
  removeOrderSuccess,
  removeOrderFailure,
  removeOrderRequest
} from "../actions/ordersActions";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* fetchOrdersSaga() {
  try {
    const {data} = yield axiosApi.get('/orders');
    yield put(fetchOrdersSuccess(data));
  } catch (error) {
    yield put(fetchOrdersFailure(error?.response?.data));
  }
}

export function* fetchOrderSaga({payload}) {
  try {
    const {data} = yield axiosApi.get('/orders/' + payload);
    yield put(fetchOrderSuccess(data));
  } catch (error) {
    yield put(fetchOrderFailure(error?.response?.data));
  }
}

export function* fetchOrderCodeSaga({payload}) {
  try {
    const {data} = yield axiosApi.get('/orders/order/code?code=' + payload);
    yield put(fetchOrderCodeSuccess(data));
  } catch (error) {
    if(error?.response?.status === 401) {
      toast.warning('Вы еще не зарегистрированы!');
      return yield put(historyPush('/login'));
    }
    toast.warning(error?.response?.data?.message || 'Что то произошло не так');
    yield put(fetchOrderCodeFailure(error?.response?.data?.message));
  }
}

export function* fetchOrdersHistorySaga() {
  try {
    const {data} = yield axiosApi.get('/orders/repairer/history');
    yield put(fetchOrdersHistorySuccess(data));
  } catch (error) {
    yield put(fetchOrdersHistoryFailure(error?.response?.data));
  }
}

export function* createOrderSaga({payload: order}) {
  try {
    const {data} = yield axiosApi.post('/orders', order.data);
    yield put(createOrderSuccess(data));
    yield put(historyPush(order.link));
    toast.success('Успешно создано.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(createOrderFailure(error.response.data));
  }
}

export function* changeOrderSaga({payload}) {
  try {
    const {data} = yield axiosApi.put('/orders/' + payload.id, payload.data);
    yield put(changeOrderSuccess(data));
    yield put(historyPush('/repairer/orders'));
    toast.success('Успешно изменено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(changeOrderFailure(error.response.data));
  }
}

export function* removeOrderSaga({payload: id}) {
  try {
    yield axiosApi.delete('/orders/' + id);
    yield put(removeOrderSuccess(id));
    yield put(historyPush('/repairer/orders'));
    toast.success('Успешно удалено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(removeOrderFailure(error.response.data));
  }
}

export function* changeStatusSaga({payload}) {
  try {
    const {data} = yield axiosApi.put('/orders/changeStatus/' + payload.id, payload.data);
    yield put(changeStatusSuccess(data));
    yield put(historyPush('/repairer/orders'));
    toast.success('Заказ успешно принято')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(changeStatusFailure(error.response.data));
  }
}

const ordersSaga = [
  takeEvery(fetchOrdersRequest, fetchOrdersSaga),
  takeEvery(fetchOrderRequest, fetchOrderSaga),
  takeEvery(fetchOrderCodeRequest, fetchOrderCodeSaga),
  takeEvery(fetchOrdersHistoryRequest, fetchOrdersHistorySaga),
  takeEvery(createOrderRequest, createOrderSaga),
  takeEvery(changeOrderRequest, changeOrderSaga),
  takeEvery(removeOrderRequest, removeOrderSaga),
  takeEvery(changeStatusRequest, changeStatusSaga),
];

export default ordersSaga;
