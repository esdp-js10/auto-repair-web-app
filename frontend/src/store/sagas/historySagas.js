import {takeEvery} from "redux-saga/effects";
import {historyBack, historyPush, historyReplace} from "../actions/historyActions";
import History from "../../History";

export function* historyPushSaga({payload}) {
  try {
    History.navigate(payload)
  } catch (err) {
    console.log('historyPushSaga error');
  }
}
export function* historyReplaceSaga({payload}) {
  try {
    History.navigate(payload, {replace: true})
  } catch (err) {
    console.log('historyReplaceSaga error');
  }
}
export function* historyBackSaga() {
  try {
    History.navigate(-1);
  } catch (err) {
    console.log('historyBackSaga error');
  }
}

const historySagas = [
  takeEvery(historyPush, historyPushSaga),
  takeEvery(historyReplace, historyReplaceSaga),
  takeEvery(historyBack, historyBackSaga),
];

export default historySagas;