import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
  addPartNumberFailure,
  addPartNumberRequest,
  addPartNumberSuccess,
  editPartNumberFailure,
  editPartNumberRequest,
  editPartNumberSuccess,
  fetchPartNumbersDetailsFailure,
  fetchPartNumbersDetailsRequest,
  fetchPartNumbersDetailsSuccess,
  fetchPartNumbersOfDetailsFailure,
  fetchPartNumbersOfDetailsRequest,
  fetchPartNumbersOfDetailsSuccess
} from "../actions/partNumbersDetailsActions";
import {historyBack} from "../actions/historyActions";

export function* fetchPartNumbersDetailSaga() {
  try {
    const response = yield axiosApi.get('/partNumbersDetails');
    yield put(fetchPartNumbersDetailsSuccess(response.data));
  } catch (error) {
    yield put(fetchPartNumbersDetailsFailure());
    toast.error(error);
  }
}

export function* fetchPartNumbersOfDetailSaga({payload: typeData}) {
  try {
    const response = yield axiosApi.get(`/partNumbersDetails?type=${typeData._id}`);
    yield put(fetchPartNumbersOfDetailsSuccess(response.data));
  } catch (error) {
    yield put(fetchPartNumbersOfDetailsFailure());
    toast.error(error);
  }
}

export function* addPartNumbers({payload: partNumberData}) {
  try {
    yield axiosApi.post(`/partNumbersDetails/new${partNumberData.id}`, {partNumbers: partNumberData.data});
    yield put(addPartNumberSuccess());
    yield put(historyBack());
    toast.success('Данные успешно сохранены!');
  } catch (e) {
    if(!e.response?.data?.errors) {
      yield put(addPartNumberFailure(e.response?.data));
    }
    toast.error('Заполните все обязательные поля!');
  }
}

export function* editPartNumberSagas({payload}) {
  try {
    yield axiosApi.put(`/partNumbersDetails/edit?partNumberId=${payload._id}`, payload.partNumberData);
    yield put(editPartNumberSuccess());
    yield put(fetchPartNumbersOfDetailsRequest());
    toast.success('Изменения сохраненны');
    payload.successCallback();
  } catch (e) {
    // yield put(editPartNumberFailure(error.response.data));
    // console.log('part number error', error.response.data);
    // toast.error(error.response.data.errors.name.message);
    if (e.response.data.error) {
      toast.error(e.response.data.error)
    }
    yield put(editPartNumberFailure(e.response.data));
  }
}

const partNumbersDetailSaga = [
  takeEvery(fetchPartNumbersDetailsRequest, fetchPartNumbersDetailSaga),
  takeEvery(fetchPartNumbersOfDetailsRequest, fetchPartNumbersOfDetailSaga),
  takeEvery(addPartNumberRequest, addPartNumbers),
  takeEvery(editPartNumberRequest, editPartNumberSagas),
];

export default partNumbersDetailSaga;
