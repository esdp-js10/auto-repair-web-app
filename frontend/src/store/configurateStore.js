import {combineReducers} from "redux";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import {rootSagas} from "./rootSagas";
import usersSlice from "./slices/usersSlice";
import employeesSlice from "./slices/employeesSlice";
import warehousesSlice from "./slices/warehousesSlice";
import workshopsSlice from "./slices/workshopsSlice";
import newsSlice from "./slices/newsSlice";
import typeDetailsSlice from "./slices/typeDetailsSlice";
import partNumbersDetailsSlice from "./slices/partNumbersDetailsSlice";
import detailsSlice from "./slices/detailsSlice";
import ordersSlice from "./slices/ordersSlice";
import GDTsSlice from "./slices/GDTsSlice";
import repairerOrdersSlice from "./slices/repairerOrdersSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  employees: employeesSlice.reducer,
  warehouses: warehousesSlice.reducer,
  workshops: workshopsSlice.reducer,
  news: newsSlice.reducer,
  typesDetails: typeDetailsSlice.reducer,
  partNumbersDetails: partNumbersDetailsSlice.reducer,
  details: detailsSlice.reducer,
  orders: ordersSlice.reducer,
  GDT: GDTsSlice.reducer,
	repairerOrders: repairerOrdersSlice.reducer,
});

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
});

sagaMiddleware.run(rootSagas);

export default store;