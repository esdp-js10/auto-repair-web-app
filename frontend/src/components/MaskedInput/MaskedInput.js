import React from 'react';
import InputMask from "react-input-mask";

const MaskedInput = ({children, name, value, mask, onChange}) => {
	const handleChange = e => {
		onChange({
			...e,
			target: {
				...e.target,
				name,
				value: e.target.value
			}
		});
	};

	return (
		<InputMask
			name={name}
			mask={mask}
			value={value}
			onChange={handleChange}
		>
			{children}
		</InputMask>
	);
};

export default MaskedInput;