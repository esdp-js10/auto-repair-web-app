import React, {useEffect, useState} from 'react';
import {Button, Grid, MenuItem, TextField, Typography} from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import FileInput from "./FileInput";
import {useDispatch, useSelector} from "react-redux";
import {createGDTRequest} from "../../../store/actions/GDTsActions";
import {fetchPartNumbersDetailsRequest} from "../../../store/actions/partNumbersDetailsActions";
import Autocomplete from '@mui/material/Autocomplete';

const transmission = [
  {_id: 2, title: 'Механика'},
  {_id: 3, title: 'Автомат'},
  {_id: 4, title: 'Вариатор'},
  {_id: 5, title: 'Робот'}
];


const GDTForm = () => {
  const dispatch = useDispatch();
  const {partNumbers} = useSelector(state => state.partNumbersDetails)
  const [files, setFiles] = useState([]);
  const [GDT, setGDT] = useState({
    serialNo: '',
    partNoDetails: [],
    description: '',
    images: '',
  })
  const [car, setCar] = useState([{
    brandAuto: '',
    model: '',
    year: {
      start: '',
      end: '',
    },
    engineVolume: '',
    transmission: '',
  }]);

  const addCar = () => {
    setCar(prev => [
      ...prev,
      {
        brandAuto: '',
        model: '',
        year: {
          start: '',
          end: '',
        },
        engineVolume: '',
        transmission: '',
      }
    ])
  };

  useEffect(() => {
    dispatch(fetchPartNumbersDetailsRequest());
  }, [dispatch]);

  const carChangeHandler = (e, i) => {
    const {name, value} = e.target
    setCar(prev => {
      const carCopy = {
        ...prev[i],
        [name]: value,
      };

      return prev.map((d, index) => {
        if (i === index) {
          return carCopy;
        }
        return d;
      })
    });
  };

  const handleFieldChange = event => {
    const {name, value} = event.target
    setGDT(prev => ({
      ...prev, [name]: value
    }))
  };

  const createHandler = e => {
    e.preventDefault();
    const data = {...GDT, cars: JSON.stringify(car), partNoDetails: JSON.stringify(GDT.partNoDetails)};
    const formData = new FormData();

    Object.keys(data).forEach(key => {
      formData.append(key, data[key]);
    });

    for(let i = 0; i < files.length; i++){
      formData.append('images', files[i]);
    }

    dispatch(createGDTRequest(formData));
  }

  const removeCar = i => {
    setCar(prev => prev.filter(p => car.indexOf(p) !== i));
  };

  const uploadFileHandler = (event) => {
    setFiles(event.target.files);
  };

  return (
    <Grid container className="container" flexDirection="column" sx={{margin : "0 auto"}} justifyContent="center" alignItems="center">
      <Grid item xs={12}>
        <Typography textAlign="center" variant="h4">Создание ГДТ</Typography>
      </Grid>
      {car.map((d, i) => (
        <Grid key={i} container flexdirection="row" spacing={2} mt={1} mb={2} justifyContent="center" alignItems="center" item xs={12}>
          <Grid item xs={12}>
            <Typography textAlign="center" variant="h6">{i + 1}-машина</Typography>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              required
              fullWidth
              name="brandAuto"
              label="Марка"
              value={d.brandAuto}
              onChange={e => carChangeHandler(e, i)}
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              required
              fullWidth
              name="model"
              label="Модель"
              value={d.model}
              onChange={e => carChangeHandler(e, i)}
            />
          </Grid>
          <Grid container justifyContent="center" alignItems="center" item xs={12} spacing={2}>
            <Grid item xs={12} sm={6} md={3}>
              <TextField
                required
                fullWidth
                name="year"
                label="Год от"
                value={d.year.start}
                onChange={e => carChangeHandler({target: {name: e.target.name, value: {start: e.target.value, end: d.year.end}}}, i)}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <TextField
                required
                fullWidth
                name="year"
                label="до"
                value={d.year.end}
                onChange={e => carChangeHandler({target: {name: e.target.name, value: {start: d.year.start, end: e.target.value}}}, i)}
              />
            </Grid>
          </Grid>
          <Grid item xs={12} sm={6} md={3} pl={1}>
            <TextField
              required
              fullWidth
              name="engineVolume"
              label="Объем от - до л."
              value={d.engineVolume}
              onChange={e => carChangeHandler(e, i)}
            />
          </Grid>
          <Grid container flexDirection="row" item xs={12} sm={6} md={3} pl={1}>
           <Grid item xs={8}>
             <TextField
               required
               select
               label="Коробка"
               name="transmission"
               fullWidth
               color={'action1'}
               value={d.transmission}
               onChange={e => carChangeHandler(e, i)}
             >
               {transmission.map((option, i) => (
                 <MenuItem key={i} value={option.title}>
                   {option.title}
                 </MenuItem>
               ))}
             </TextField>
           </Grid>
            <Grid container item flexDirection="row"  xs={3} spacing={1}>
              {car.length > 1 && <Grid item xs={6}><Button onClick={() => removeCar(i)}>
                <RemoveCircleOutlineIcon  color="action" fontSize="large"/>
              </Button></Grid>}
              {i === car.length - 1 && <Grid item xs={6}><Button onClick={addCar}>
                <AddCircleOutlineIcon color="action" fontSize="large"/>
              </Button></Grid>}
            </Grid>
          </Grid>
        </Grid>
      ))}
      <Grid container spacing={2} mt={1} alignItems="center" justifyContent="center">
        <Grid item xs={12} md={7} pl={1}>
          <TextField
            fullWidth
            color={'action1'}
            placeholder={'Серийный номер'}
            name="serialNo"
            value={GDT.serialNo}
            onChange={handleFieldChange}
            required
          />
        </Grid>
        <Grid item xs={12} md={7} pl={1}>
          <Autocomplete
            multiple
            id="tags-standard"
            options={partNumbers}
            getOptionLabel={(option) => option.partNumber}
            defaultValue={GDT.partNoDetails}
            value={GDT.partNoDetails}
            onChange={(event, newValue) => handleFieldChange({target: {name: 'partNoDetails', value: newValue}})}
            renderInput={(params) => (
              <TextField
                required
                {...params}
                label="Парт номер"
                placeholder="Парт номер"
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} md={7} pl={1}>
          <TextField
            required
            fullWidth
            color={'action1'}
            placeholder={'Комментарий'}
            name="description"
            value={GDT.description}
            onChange={handleFieldChange}
          />
        </Grid>
        <Grid item xs={12} md={7} pl={1}>
          <FileInput
            label={'Фото'}
            name="images"
            value={GDT.images}
            onChange={uploadFileHandler}
          />
        </Grid>
        <Grid item xs={12} md={7} pl={1}>
          <Button
            fullWidth
            color={'action1'}
            variant="contained"
            sx={{padding: '10px 0'}}
            onClick={createHandler}
          >
            Создать
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default GDTForm;