import React, {useEffect, useState} from 'react';
import {Grid, TextField, Typography} from "@mui/material";
import {useStyles} from './EmployeeForm.styles';
import ButtonWithProgress from "../../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {changeStatus} from "../../../store/actions/employeesActions";
import {fetchWarehousesByStatus} from "../../../store/actions/warehousesActions";
import {fetchWorkshopsByStatus} from "../../../store/actions/workshopActions";
import MaskedInput from "../../MaskedInput/MaskedInput";
import MenuItem from "@mui/material/MenuItem";

const initialState = {
	name: '',
	INN: '',
	phoneNumber: '',
	address: '',
	avatar: null,
	email: '',
	password: '',
	role: '',
	warehouse: '',
	workshop: '',
};

const EmployeeForm = ({onSubmit, employeeData, error, loading}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const [disable, setDisable] = useState(false);
	const [employee, setEmployee] = useState( employeeData || initialState);
	const [passwordsMath, setPasswordsMatch] = useState(false);
	const [confirmPassword, setConfirmPassword] = useState(employeeData ? employeeData.password : '');
	const user = useSelector(state => state.users.profile);
	const warehouses = useSelector(state => state.warehouses.fetchWarehousesByStatus);
	const workshops = useSelector(state => state.workshops.activeWorkshops);
	const changeStatusLoading = useSelector(state => state.employees.changeStatusLoading);
	const roles = [{titleEng: 'warehouseman', titleRu: 'Складовщик'}, {titleEng: 'repairer', titleRu: 'Мастер'}];

	useEffect(() => {
		dispatch(fetchWarehousesByStatus('active'));
		dispatch(fetchWorkshopsByStatus('active'));

		if (employeeData) {
			setDisable(true);
		}
	}, [dispatch, employeeData]);

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setEmployee(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setEmployee(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const changeConfirmPassword = e => {
		setConfirmPassword(e.target.value);
	};

	const submitFormHandler = e => {
		e.preventDefault();

		if (confirmPassword !== employee.password) {
			return setPasswordsMatch(true);
		}

		const formData = new FormData();
		Object.keys(employee).forEach(key => {
			formData.append(key, employee[key]);
		});

		onSubmit(formData);
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	const changeRole = value => {
		setEmployee(prevState => ({...prevState, role: value}));

		if (value === 'warehouseman') {
			setEmployee(prevState => ({...prevState, workshop: ''}));
		}

		if (value === 'repairer') {
			setEmployee(prev => ({
				...prev,
				warehouse: '',
			}));
		}
	};

	const workshopField = (
		<Grid item xs={12}>
			<TextField
				type="text"
				select
				fullWidth
				label="Мастерская"
				name="workshop"
				value={employee.workshop === '' ? '' : employee.workshop}
				onChange={inputChangeHandler}
				defaultValue=""
			>
				<MenuItem value=''>Выберите мастерской</MenuItem>
				{workshops && workshops.map(workshop => (
					<MenuItem
						key={workshop._id}
						value={workshop._id}
					>
						{workshop.name}
					</MenuItem>
				))}
			</TextField>
		</Grid>
	);

	const warehouseField = (
		<Grid item xs={12}>
			<TextField
				type="text"
				select
				fullWidth
				label="Склад"
				name="warehouse"
				value={employee.warehouse === '' ? '' : employee.warehouse}
				onChange={inputChangeHandler}
				defaultValue=""
			>
				<MenuItem value=''>Выберите склад</MenuItem>
				{warehouses && warehouses.map(warehouse => (
					<MenuItem
						key={warehouse._id}
						value={warehouse._id}
					>
						{warehouse.name}
					</MenuItem>
				))}
			</TextField>
		</Grid>
	);

	return (
		<div className="container">
			<div className={classes.root}>
				{employeeData ?
					<Typography component="h1" variant="h5" mb={4} style={{textAlign: 'center'}}>
						Редактирование сотрудника
					</Typography> :
					<Typography component="h1" variant="h5" mb={4} style={{textAlign: 'center'}}>
						Добавление нового сотрудника
					</Typography>
				}
				<div className={classes.formContainer}>
					<Grid
						container
						component="form"
						onSubmit={submitFormHandler}
						spacing={2}
						noValidate
						mb={2}
					>
						<Grid item xs={12}>
							<TextField
								type="text"
								required
								fullWidth
								label="ФИО"
								name="name"
								value={employee.name}
								onChange={inputChangeHandler}
								error={Boolean(getFieldError('name'))}
								helperText={getFieldError('name')}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="number"
								required
								fullWidth
								label="ИНН"
								name="INN"
								disabled={!(user.role === 'admin')}
								value={employee.INN}
								onChange={inputChangeHandler}
								error={Boolean(getFieldError('INN'))}
								helperText={getFieldError('INN')}
							/>
						</Grid>
						<Grid item xs={12}>
							<MaskedInput
								name="phoneNumber"
								value={employee.phoneNumber}
								mask="0(999)-99-99-99"
								onChange={inputChangeHandler}
							>
								{inputProps =>
									<TextField
										type="tel"
										required
										fullWidth
										{...inputProps}
										label="Номер телефона"
										error={Boolean(getFieldError('phoneNumber'))}
										helperText={getFieldError('phoneNumber')}
									/>
								}
							</MaskedInput>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="text"
								fullWidth
								label="Адрес проживания"
								name="address"
								value={employee.address}
								onChange={inputChangeHandler}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="file"
								fullWidth
								name="avatar"
								onChange={fileChangeHandler}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="email"
								required
								fullWidth
								label="Почта"
								name="email"
								value={employee.email}
								onChange={inputChangeHandler}
								error={Boolean(getFieldError('email'))}
								helperText={getFieldError('email')}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="password"
								required
								fullWidth
								label="Пароль"
								name="password"
								value={employee.password}
								onChange={inputChangeHandler}
								error={Boolean(getFieldError('password'))}
								helperText={getFieldError('password')}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="password"
								required
								fullWidth
								label="Потвердить пароль"
								name="confirmPassword"
								value={confirmPassword}
								onChange={changeConfirmPassword}
								error={passwordsMath}
								helperText={passwordsMath === false ? '' : 'Пароль не совпадает'}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								fullWidth
								required
								select
								variant="outlined"
								label="Должность"
								name="role"
								disabled={disable}
								defaultValue=""
								value={employee.role}
								onChange={e => changeRole(e.target.value)}
								error={Boolean(getFieldError('role'))}
								helperText={getFieldError('role')}
							>
								{roles.map(role => (
									<MenuItem
										key={role.titleEng}
										value={role.titleEng}
									>
										{role.titleRu}
									</MenuItem>
								))}
							</TextField>
						</Grid>
						{employee.role === '' ? null : (employee.role === 'repairer' ? workshopField : warehouseField)}
						{!employeeData &&
							<Grid item xs={12}>
								<ButtonWithProgress
									type="submit"
									variant="contained"
									fullWidth
									style={{backgroundColor: "#4355FB"}}
									loading={loading}
									disabled={loading}
								>
									Создать
								</ButtonWithProgress>
							</Grid>
						}
						{employeeData &&
							<Grid item xs={12}>
								<ButtonWithProgress
									type="submit"
									fullWidth
									variant="contained"
									style={{backgroundColor: "#4355FB"}}
									loading={loading}
									disabled={loading}
								>
									Потвердить изменения
								</ButtonWithProgress>
							</Grid>
						}
					</Grid>
					{(employeeData && (user?.role === 'admin')) &&
						<Grid item xs={12}>
							<ButtonWithProgress
								fullWidth
								variant="contained"
								style={{backgroundColor: employeeData?.status ? 'red' : 'green'}}
								onClick={() => dispatch(changeStatus(employee._id))}
								loading={changeStatusLoading}
								disabled={changeStatusLoading}
							>
								{employeeData?.status ? 'Деактивировать' : 'Активировать'}
							</ButtonWithProgress>
						</Grid>
					}
				</div>
			</div>
		</div>
);
};

export default EmployeeForm;