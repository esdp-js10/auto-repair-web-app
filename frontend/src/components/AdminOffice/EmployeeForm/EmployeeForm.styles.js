import {makeStyles} from "@mui/styles";

export const useStyles = makeStyles({
	root: {
		margin: '35px 0',
	},
	formContainer: {
		maxWidth: '750px',
		backgroundColor: '#FEFEFE',
		border: '1px solid #c1c1c1',
		borderRadius: '8px',
		padding: '30px',
		margin: '0 auto',
	},
	employeePhoto: {
		width: '125px',
		height: '140px',
		border: '1px solid #ccc',
		borderRadius: '4px',
		textAlign: 'center',
	},
});