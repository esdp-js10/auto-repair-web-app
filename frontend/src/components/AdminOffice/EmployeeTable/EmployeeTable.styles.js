import {makeStyles} from "@mui/styles";

export const useStyles = makeStyles(theme => ({
	tableContainer: {
		marginBottom: '40px',
	},
	tableHeaderCell: {
		fontWeight: 'bold',
		backgroundColor: '#212121',
		color: theme.palette.getContrastText(theme.palette.primary.dark),
	},
	avatar: {
		backgroundColor: '#616161',
	},
	name: {
		fontWeight: 'bold',
		color: '#212121',
	},
	status: {
		fontWeight: 'bold',
		fontSize: '0.75rem',
		color: '#fff',
		backgroundColor: '#ccc',
		borderRadius: '8px',
		padding: '3px 10px',
		display: 'inline-block',
	},
}));