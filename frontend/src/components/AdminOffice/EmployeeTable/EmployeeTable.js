import React, {useState} from 'react';
import {
	Button,
	CircularProgress,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Avatar, Grid, Typography,
	TablePagination, TableFooter,
} from "@mui/material";
import {Link} from "react-router-dom";
import {useStyles} from "./EmployeeTable.styles";
import {BASE_URL} from "../../../config";
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';

const EmployeeTable = ({loading, employees, search, sortByRole, sortByStatus, sortByWorkshop}) => {
	const classes = useStyles();

	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(5);

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(+event.target.value);
		setPage(0);
	};

	const define = role => {
		switch (role) {
			case 'admin': return 'Админ';
			case 'repairer': return 'Мастер';
			case 'warehouseman': return 'Складовщик';
			default: return 'Unknown role';
		}
	};

	return (
		<TableContainer component={Paper} className={classes.tableContainer}>
			<Table sx={{minWidth: 650}} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell className={classes.tableHeaderCell}>ФИО</TableCell>
						<TableCell className={classes.tableHeaderCell}>Должность</TableCell>
						<TableCell className={classes.tableHeaderCell}>Статус</TableCell>
						<TableCell className={classes.tableHeaderCell}>Редактировать</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{loading ?
						<TableRow>
							<TableCell sx={{textAlign: 'center'}}>
								<CircularProgress/>
							</TableCell>
						</TableRow> :
						employees
							.filter(value => {
							if (search === '') {
								return value;
							} else if (value.name.toLowerCase().includes(search.toLowerCase())) {
								return value;
							} return ''; })
							.filter(employee => {
							if (sortByRole === '') {
								return employee;
							} else if (employee.role.includes(sortByRole)) {
								return employee;
							} return '';})
							.filter(employee => {
								if (sortByStatus === '') {
									return employee;
								} else if (employee.status === sortByStatus) {
									return employee;
								} return ''; })
							.filter(employee => {
								if (sortByWorkshop === '') {
									return employee;
								} else if ((employee.role === 'repairer' && employee.workshop) &&
									(employee.workshop.name === sortByWorkshop)) {
									return employee;
								} return ''; })
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(employee => (
						<TableRow
							key={employee._id}
							sx={{'&:last-child td, &:last-child th': {border: 0}}}
						>
							<TableCell>
								<Grid container>
									<Grid item lg={1.3} xs={2}>
										<Avatar
											alt={employee.name}
											src={employee.avatar ? BASE_URL + '/' + employee.avatar : '.'}
											className={classes.avatar}
										/>
									</Grid>
									<Grid item lg={10.7} xs={10}>
										<Typography className={classes.name}>{employee.name}</Typography>
										<Typography color="textSecondary" variant="body2">{employee.email}</Typography>
										<Typography color="textSecondary" variant="body2">{employee.phoneNumber}</Typography>
									</Grid>
								</Grid>
							</TableCell>
							<TableCell>
								<Typography variant="subtitle2">
									{define(employee.role)}
								</Typography>
							</TableCell>
							<TableCell>
								<Typography
									className={classes.status}
									style={{
										backgroundColor:
											((employee.status && 'green') || (!employee.status  && 'red'))
									}}
								>
									{employee.status ? 'Активный' : 'Неактивный'}
								</Typography>
								</TableCell>
							<TableCell>
								<Button component={Link} to={`/admin/employees/edit?employee=${employee._id}`}>
									<EditOutlinedIcon/>
								</Button>
							</TableCell>
						</TableRow>
					))
					}
				</TableBody>
				<TableFooter>
					<TableRow>
						<TableCell>
							<TablePagination
								rowsPerPageOptions={[5, 10, 15]}
								component="div"
								count={employees.length}
								rowsPerPage={rowsPerPage}
								page={page}
								onPageChange={handleChangePage}
								onRowsPerPageChange={handleChangeRowsPerPage}
							/>
						</TableCell>
					</TableRow>
				</TableFooter>
			</Table>
		</TableContainer>
	);
};

export default EmployeeTable;