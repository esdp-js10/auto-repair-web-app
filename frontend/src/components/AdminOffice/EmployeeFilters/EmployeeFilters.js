import React from 'react';
import {Grid, TextField} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";

const EmployeeFilters = (props) => {
	const roles = [
		{titleEng: 'admin', titleRu: 'Админ'},
		{titleEng: 'warehouseman', titleRu: 'Складовщик'},
		{titleEng: 'repairer', titleRu: 'Мастер'},
	];

	const status = [
		{status: true, titleRu: 'Активный'},
		{status: false, titleRu: 'Неактивный'},
	];

	return (
		<Grid container mb={5} spacing={2}>
			<Grid item>
				<TextField
					type="text"
					label="Поиск по имени"
					name="search"
					value={props.search}
					onChange={e => props.setSearch(e.target.value)}
				/>
			</Grid>
			<Grid item>
				<TextField
					select
					variant="outlined"
					label="Должность"
					name="role"
					value={props.sortByRole}
					onChange={e => props.setSortByRole(e.target.value)}
					style={{minWidth: '150px'}}
				>
					<MenuItem value=''>Выберите должность</MenuItem>
					{roles.map(role => (
						<MenuItem
							key={role.titleEng}
							value={role.titleEng}
						>
							{role.titleRu}
						</MenuItem>
					))}
				</TextField>
			</Grid>
			<Grid item>
				<TextField
					select
					variant="outlined"
					label="Статус"
					name="status"
					value={props.sortByStatus}
					onChange={e => props.setSortByStatus(e.target.value)}
					style={{minWidth: '150px'}}
				>
					<MenuItem value=''>Выберите статус</MenuItem>
					{status.map(s => (
						<MenuItem
							key={s.status}
							value={s.status}
						>
							{s.titleRu}
						</MenuItem>
					))}
				</TextField>
			</Grid>
			<Grid item>
				<TextField
					type="text"
					select
					fullWidth
					label="Мастерская"
					name="workshop"
					value={props.sortByWorkshop}
					onChange={e => props.setSortByWorkshop(e.target.value)}
					style={{minWidth: '150px'}}
				>
					<MenuItem value=''>Выберите мастерской</MenuItem>
					{props.workshops && props.workshops.map(workshop => (
						<MenuItem
							key={workshop._id}
							value={workshop.name}
						>
							{workshop.name}
						</MenuItem>
					))}
				</TextField>
			</Grid>
		</Grid>
	);
};

export default EmployeeFilters;