import React, {useState} from 'react';
import {Button, Grid, TextField, Typography} from "@mui/material";
import {useNavigate, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {toast} from "react-toastify";
import {changeNewsRequest, createNewsRequest} from '../../../store/actions/newsActions';

const NewsForm = () => {
  const {id} = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {news, changeNewsLoading, createNewsLoading} = useSelector(state => state.news);

  let current = null;

  if(id){
    if(news.length < 1){
      navigate('/workshop');
      toast.warning('Что то не пошло не так!');
    }else{
      for (let n of news) {
        if (n._id === id) {
          current = n;
        }
      }
    }
  }

  const [state, setState] = useState({
    title: current ? current.title : "",
    text: current ? current.text : "",
    image: current ? current.image : null,
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    if(current){
      dispatch(changeNewsRequest({data: formData, id}))
    }else{
      dispatch(createNewsRequest(formData))
    }
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];
    setState(prevState => {
      return {...prevState, [name]: file};
    });
  };



  return (
    <Grid container item xs={11} sm={10} sx={{margin: "20px auto"}} justifyContent="center" alignItems="center" direction="column">
      <Grid item xs={12} mt={3} mb={3}>
        <Typography textAlign="center" variant="h4">{current ? "Редактировать новости" : "Создать новости"}</Typography>
      </Grid>
      <Grid container item spacing={3} xs={12} direction="column" component="form" onSubmit={submitFormHandler}>
        <Grid item xs={12}>
          <TextField
            name="title"
            label="Называние"
            fullWidth
            variant="outlined"
            color="primary"
            value={state.title}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="text"
            label="Описание"
            fullWidth
            variant="outlined"
            color="primary"
            multiline
            rows={4}
            value={state.text}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={"file"}
            name={"image"}
            fullWidth
            onChange={fileChangeHandler}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            type="submit"
            variant="contained"
            color={'action1'}
            disabled={createNewsLoading || changeNewsLoading}
            fullWidth
          >
            {current ? "Редактировать" : "Создать"}
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default NewsForm;