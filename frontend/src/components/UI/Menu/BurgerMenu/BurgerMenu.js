import React from 'react';
import {stack as Menu} from 'react-burger-menu';

const styles = {
  bmBurgerButton: {
    position: 'absolute',
    width: '36px',
    height: '30px',
    right: 0,
    top: 8,
  },
  bmBurgerBars: {
    background: '#fff'
  },
  bmBurgerBarsHover: {
    background: '#a90000'
  },
  bmCrossButton: {
    height: '24px',
    width: '24px'
  },
  bmCross: {
    background: '#bdc3c7'
  },
  bmMenuWrap: {
    position: 'fixed',
    top: 0,
    height: '100%',
  },
  bmMenu: {
    padding: '40px 0',
    background: '#1D1D1F',
    fontSize: '1.15em'
  },
  bmMorphShape: {
    fill: '#1D1D1F'
  },
  bmItemList: {
    color: '#b8b7ad',
  },
  bmItem: {
    display: 'flex'
  },
  bmOverlay: {
    left: 0,
    top: 0,
    background: 'rgba(0, 0, 0, 0.3)',
  }
}

const BurgerMenu = ({...props}) => {
  return (
    <Menu right
          pageWrapId={"page-wrap"}
          styles={styles} {...props}
          outerContainerId={'outer-container'}
    />
  );
};

export default BurgerMenu;