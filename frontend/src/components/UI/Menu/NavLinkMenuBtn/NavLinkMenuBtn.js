import React from 'react';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuList from '@mui/material/MenuList';
import Stack from '@mui/material/Stack';
import {NavLink} from "react-router-dom";
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';

const NavLinkMenuBtn = ({to, btnText, children}) => {
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <Stack>
      <NavLink style={{position: "relative", paddingRight: 15}}
               to={to} ref={anchorRef}
               onMouseEnter={handleToggle}
      >
        {btnText}
        <ExpandMoreTwoToneIcon sx={{position: "absolute", right: 0, top: 5, width: 15, height: 15}}/>
      </NavLink>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        //role={undefined}
        placement="bottom"
        transition
        disablePortal
        modifiers={[
          {
            name: 'offset',
            options: {
              offset: [0, 10],
            },
          }
        ]}
      >
        {({TransitionProps, placement}) => {
          return (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === 'bottom-start' ? 'top left' : 'left bottom',
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList
                    sx={{background: '#2e2e33'}}
                    autoFocusItem={open}
                    onMouseLeave={handleClose}
                  >
                    {children}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          );
        }}
      </Popper>
    </Stack>
  );
};

export default NavLinkMenuBtn;