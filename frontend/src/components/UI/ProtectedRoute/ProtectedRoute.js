import {Navigate, Route} from "react-router-dom";
import * as React from "react";

const ProtectedRoute = ({isAllowed, redirectTo, children, ...props}) => {
  return isAllowed ?
    children :
    <Navigate to={redirectTo}/>
};
export default ProtectedRoute;