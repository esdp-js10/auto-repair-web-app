import * as React from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import {Stack} from "@mui/material";
import {useDispatch} from "react-redux";
import {useState} from "react";
import {fetchOrderCodeRequest} from "../../../../store/actions/ordersActions";

const InpSearchMain = ({placeholder}) => {
  const dispatch = useDispatch();
  const [code, setCode] = useState('');

  const inputChangeHandler = e => {
    setCode(e.target.value);
  };

  const getOrderHandler = e => {
    e.preventDefault();
    dispatch(fetchOrderCodeRequest(code));
  };


  return (
    <Paper sx={{p: '2px 4px'}}>
      <Stack component={'form'} sx={{flexDirection: "row", flexWrap: "nowrap"}}>
        <InputBase
          sx={{paddingLeft: '10px'}}
          value={code}
          onChange={inputChangeHandler}
          placeholder={placeholder}
        />
        <Divider sx={{height: 28, m: 0.5}} orientation="vertical"/>
        <IconButton onClick={getOrderHandler} type="submit" sx={{p: '10px'}} aria-label="search">
          <SearchIcon/>
        </IconButton>
      </Stack>
    </Paper>
  );
};

export default InpSearchMain;
