import React from 'react';
import {Button, Dialog, DialogContent, DialogTitle, Typography} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles(theme => ({
	dialogWrapper: {
		padding: theme.spacing(2),
		position: "absolute",
		top: theme.spacing(10),
		width: '100%',
	},
	secondary: {
		backgroundColor: theme.palette.secondary.light,
		'& .MuiButton-label': {
			color: theme.palette.secondary.main,
		},
	},
}));

const Popup = ({title, children, openPopup, setOpenPopup}) => {
	const classes = useStyles();

	return (
		<Dialog open={openPopup} maxWidth="sm" classes={{paper: classes.dialogWrapper}}>
			<DialogTitle>
				<div style={{display: 'flex', flexWrap: 'wrap', alignItems: 'center'}}>
					<Typography	variant="h6"	style={{flexGrow: 1}}>
						{title}
					</Typography>
					<Button
						variant="contained"
						color="secondary"
						className={classes.secondary}
						onClick={() => setOpenPopup(false)}
					>
						<CloseIcon/>
					</Button>
				</div>
			</DialogTitle>
			<DialogContent dividers sx={{padding: '31px 24px 16px'}}>
				{children}
			</DialogContent>
		</Dialog>
	);
};

export default Popup;