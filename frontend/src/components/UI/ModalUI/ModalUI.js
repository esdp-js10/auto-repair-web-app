import React from 'react';
import {Box, IconButton, Modal, Stack} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

const ModalUI = ({style, open, onClose, children, maxWidth = 700, isOverflow = false, maxHeight = 800, variant}) => (
  <Modal
    open={open}
    onClose={onClose}
  >
    <Box sx={{
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      width: '90vw',
      height: isOverflow ? '75vh' : 'auto',
      maxWidth,
      maxHeight,
      backgroundColor: 'background.paper',
      border: '1px solid #4D77E3',
      borderRadius: '10px',
      boxShadow: 24,
      overflow: 'hidden',
      ...style,
    }}
    >
      <Stack pt={variant === 'image' ? 0 : 4}
             position={'relative'} sx={{ height: '100%'}}
      >
        <IconButton sx={{position: "absolute", right: 5, top: 5, zIndex: 100}} size={'small'}
                    onClick={onClose}
        >
          <CloseIcon/>
        </IconButton>
        <div>
        <Stack sx={{overflow: 'auto'}} px={variant === 'image' ? 0 : 1} pb={variant === 'image' ? 0 : 2}  pl={4} pr={4}>
          {children}
        </Stack>
        </div>
      </Stack>
    </Box>
  </Modal>
)

export default ModalUI;