import React from 'react';
import c from './Recall.module.css';
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {ORDERS_ADD_PATH} from "../../../constants/paths";

const Recall = () => {
  const profile = useSelector(state => state.users.profile);
  return profile && <Link style={{textDecoration: 'none'}} to={ORDERS_ADD_PATH}>
    <button type="button" className={c.btn2}>
      Оформить заказ
    </button>
  </Link>
};

export default Recall;