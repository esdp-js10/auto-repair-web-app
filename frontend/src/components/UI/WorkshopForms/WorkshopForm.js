import React, {useEffect, useState} from 'react';
import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Grid,
  MenuItem,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {createWorkshopRequest} from "../../../store/actions/workshopActions";
import {fetchFreeWarehousesRequest} from "../../../store/actions/warehousesActions";


const WorkshopForm = () => {
  const dispatch = useDispatch();
  const { createWorkshopLoading} = useSelector(state => state.workshops)
  const { freeWarehouses} = useSelector(state => state.warehouses)
  const [checked, setChecked] = useState(true);
  const [workshop, setWorkshop] = useState({
    name: '',
    warehouse: null,
    phoneNumber: '',
    timetable: '',
    address: '',
  });

  useEffect(() => {
    dispatch(fetchFreeWarehousesRequest());
  }, [dispatch]);

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setWorkshop(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  const onClickHandler = e => {
    e.preventDefault();
    if(checked){
      const warehouse = freeWarehouses.filter(f => workshop.warehouse === f.name);
      dispatch(createWorkshopRequest({...workshop, warehouse: warehouse[0]}));
    }else{
      dispatch(createWorkshopRequest({...workshop}));
    }
  }

  return createWorkshopLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ): (
    <Stack className="container" spacing={2} maxWidth={700} width={'100%'} m={"40px auto"}>
      <Typography
        mt={2}
        variant={"h5"}
        textAlign={"left"}
      >
        Форма создания мастерской
      </Typography>
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Название'}
        name="name"
        value={workshop.name}
        onChange={inputChangeHandler}
      />
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Адрес'}
        name="address"
        value={workshop.address}
        onChange={inputChangeHandler}
      />
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Номера телефона 0(XXX)-XX-XX-XX'}
        name="phoneNumber"
        value={workshop.phoneNumber}
        onChange={inputChangeHandler}
      />
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Рабочое время XX:XX - XX:XX'}
        name="timetable"
        value={workshop.timetable}
        onChange={inputChangeHandler}
      />
      <FormControlLabel
        label="Выбрать склад"
        control={<Checkbox
          checked={checked}
          onChange={handleChange}
          inputProps={{ 'aria-label': 'controlled' }}
        />}
      />
      <TextField
        select
        label="Склад"
        fullWidth
        color={'action1'}
        onChange={inputChangeHandler}
        name="warehouse"
        value={workshop.warehouse || ''}
      >
        {freeWarehouses.map((option) => (
          <MenuItem key={option._id} value={option.name}>
            {option.name}
          </MenuItem>
        ))}
      </TextField>
      <Button
        color={'action1'}
        variant="contained"
        sx={{padding: '10px 0'}}
        onClick={onClickHandler}
        disabled={createWorkshopLoading}
      >
        Создать
      </Button>
    </Stack>
  );
};

export default WorkshopForm;