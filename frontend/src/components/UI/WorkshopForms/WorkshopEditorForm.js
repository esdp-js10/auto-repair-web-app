import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, MenuItem, Stack, TextField, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {
  changeStatusWorkshopRequest,
  changeWorkshopRequest,
} from "../../../store/actions/workshopActions";
import {fetchFreeWarehousesRequest} from "../../../store/actions/warehousesActions";
import {useNavigate, useParams} from "react-router-dom";
import {toast} from "react-toastify";

const WorkshopEditorForm = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const {workshops, changeWorkshopLoading, changeStatusLoading} = useSelector(state => state.workshops);
  const {freeWarehouses} = useSelector(state => state.warehouses)

  let current;
  for (let workshop of workshops) {
    if (workshop._id === params.id) {
      current = workshop;
    }
  }


  useEffect(() => {
    dispatch(fetchFreeWarehousesRequest());
    if (workshops.length < 1) {
      navigate('/admin/workshops');
      toast.warning('Something went wrong');
    }
  }, [dispatch, workshops.length]);

  const [workshop, setWorkshop] = useState({
    name: current?.name,
    warehouse: current?.warehouse?.name || "",
    phoneNumber: current?.phoneNumber,
    timetable: current?.timetable,
    address: current?.address,
  });

  let warehouses;
  current?.warehouse ? warehouses = [...freeWarehouses, current.warehouse] : warehouses = freeWarehouses;

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setWorkshop(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const updateHandler = e => {
    e.preventDefault();
      if(workshop.warehouse){
        const warehouse = warehouses.filter(f => workshop.warehouse === f.name);
        dispatch(changeWorkshopRequest({_id: params.id, data: {...workshop, warehouse: warehouse[0]}}));
      }else{
        dispatch(changeWorkshopRequest({_id: params.id, data: {...workshop}}));
      }
  }

  const deactivateHandler = () => {
    dispatch(changeStatusWorkshopRequest(params.id))
  }
  return changeWorkshopLoading || changeStatusLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <Stack className="container" spacing={2} maxWidth={700} width={'100%'} m={"40px auto"}>
      <Typography
        mt={2}
        variant={"h5"}
        textAlign={"left"}
      >
        Редактирование
      </Typography>
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Название'}
        value={workshop.name}
        name="name"
        onChange={inputChangeHandler}
      />
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Адрес'}
        name="address"
        value={workshop.address}
        onChange={inputChangeHandler}
      />
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Номера телефона 0(XXX)-XX-XX-XX'}
        name="phoneNumber"
        value={workshop.phoneNumber}
        onChange={inputChangeHandler}
      />
      <TextField
        fullWidth
        color={'action1'}
        placeholder={'Рабочое время XX:XX - XX:XX'}
        name="timetable"
        value={workshop.timetable}
        onChange={inputChangeHandler}
      />
      <TextField
        select
        label="Склад"
        fullWidth
        color={'action1'}
        name="warehouse"
        value={workshop.warehouse}
        onChange={inputChangeHandler}
      >
        {warehouses.map((option) => (
          <MenuItem key={option._id} value={option.name}>
            {option?.name}
          </MenuItem>
        ))}
      </TextField>
      <Button
        color={'action1'}
        variant={"contained"}
        sx={{padding: '10px 0'}}
        onClick={updateHandler}
      >
        Сохранить
      </Button>
      <Button
        sx={{padding: '10px 0'}}
        color={current?.status ? 'error' : "success"}
        variant={"contained"}
        onClick={deactivateHandler}
        disabled={changeStatusLoading}
      >
        {current?.status ? "Деактивировать мастерскую" : "Aктивировать мастерскую"}
      </Button>
    </Stack>
  )
};

export default WorkshopEditorForm;