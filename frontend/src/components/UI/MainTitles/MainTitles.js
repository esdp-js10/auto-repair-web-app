import React from 'react';
import c from './MainTitles.module.css';

const MainTitles = ({title, span}) => {
  return (
    <>
      <h3 className={c.mainTitle}><span>{span}</span> {title}</h3>
    </>
  );
};

export default MainTitles;