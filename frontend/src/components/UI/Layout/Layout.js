import React from 'react';
import {CssBaseline, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import AppToolbar from "../AppToolbar/AppToolbar";
import moment from "moment";
import ModalUI from "../ModalUI/ModalUI";
import {fetchOrderCodeSuccess} from "../../../store/actions/ordersActions";
import {useDispatch, useSelector} from "react-redux";
import {FaDatabase} from 'react-icons/fa';


const useStyles = makeStyles((theme) => ({
    layoutContent: {
      paddingTop: theme.mixins.toolbar.minHeight,
      minHeight: '100vh',
    }
  }));


const Layout = ({children}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {orderCode} = useSelector(state => state.orders);

  const onClose = () => {
    dispatch(fetchOrderCodeSuccess(null));
  }

  return (
    <>
      <CssBaseline/>
      <AppToolbar/>
      <main className={classes.layoutContent}>
        {children}
      </main>
      {orderCode?.serialNo && <ModalUI style={{borderRadius: '6px', border: 'none', background: 'rgb(162, 162, 246)'}} maxWidth={300}  open={!!orderCode} onClose={onClose}>
        <Typography sx={{margin: '0 auto 24px'}}>
          <FaDatabase style={{width: '35px', height: '35px', color: "white"}}/>
        </Typography>
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Typography textAlign="left" mr={1} color="white" variant="body1">Номер ГТФ: </Typography>
          <Typography textAlign="left"  color="white" variant="body2">{orderCode?.serialNo}</Typography>
        </div>
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Typography textAlign="left" mr={1} color="white" variant="body1">Дата: </Typography>
          <Typography textAlign="left"  color="white" variant="body2">{moment(orderCode?.date).format('DD/MM/yyyy')}</Typography>
        </div>
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Typography textAlign="left" mr={1} color="white" variant="body1">Статус заказа:</Typography>
          <Typography textAlign="left"  color="white" variant="body2">{orderCode?.status}</Typography>
        </div>
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <Typography textAlign="left" mr={1} color="white" variant="body1">Комментарий:</Typography>
          <Typography textAlign="left"  color="white" variant="body2">{orderCode?.description}</Typography>
        </div>
      </ModalUI>}
    </>
  );
};

export default Layout;