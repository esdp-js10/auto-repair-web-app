import React from 'react';
import {AppBar, Toolbar} from "@mui/material";
import MainNavbar from "../../../containers/MainNavbar/MainNavbar";

const AppToolbar = () => {

  return (
    <AppBar position={"fixed"} color={'dark'}>
      <Toolbar>
        <MainNavbar/>
      </Toolbar>
    </AppBar>
  );
};

export default AppToolbar;