import React, {useEffect, useState} from 'react';
import {Grid, Stack, TextField, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {
  deactivateWarehouseRequest,
  editWarehouseRequest,
  fetchWarehouseRequest
} from "../../../../store/actions/warehousesActions";
import {useLocation} from "react-router-dom";
import {makeStyles} from "@mui/styles";
import ButtonWithProgress from "../../ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2)
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(0, 0, 0),
  },
}));

const WarehouseEditForm = ({id, name, modalClose, status}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const location = useLocation();
  const loading = useSelector(state => state.warehouses.editWarehouseLoading);
	const error = useSelector(state => state.warehouses.editWarehouseError);
  const [state, setState] = useState({
    name: name,
  });

  useEffect(() => {
    dispatch(fetchWarehouseRequest(location.search));
  }, [dispatch, location.search]);

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	}

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(editWarehouseRequest({_id: id, warehouse: {...state}, successCallback: modalClose}));
  };

  return (
    <Stack spacing={2} maxWidth={700} width={'100%'}>
      <Typography variant={"h4"} textAlign={"center"}>
        Редактирование склада
      </Typography>
      <Grid
        container
        direction="column"
        component="form"
        className={classes.root}
        autoComplete="off"
        noValidate
      >
	      <Grid item xs={12} pb={2}>
		      <TextField fullWidth color={'action1'}
		                 label="Название"
		                 name="name"
		                 type="text"
		                 value={state.name}
		                 onChange={inputChangeHandler}
		                 required
		                 error={Boolean(getFieldError('name'))}
		                 helperText={getFieldError('name')}
		      />
	      </Grid>
        <Grid item xs={12} pb={1}>
          <ButtonWithProgress
            type="submit"
            fullWidth
            variant="contained"
            color={'action1'}
            className={classes.submit}
            onClick={submitFormHandler}
            loading={loading}
            disabled={loading}
          >
            Сохранить
          </ButtonWithProgress>
        </Grid>
        <Grid item xs={12} pb={4}>
          {status ?
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color={'error'}
              className={classes.submit}
              onClick={() => dispatch(deactivateWarehouseRequest(id))}
              loading={loading}
              disabled={loading}
            >
              Деактивировать
            </ButtonWithProgress> :
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color={'success'}
              className={classes.submit}
              onClick={() => dispatch(deactivateWarehouseRequest(id))}
              loading={loading}
              disabled={loading}
            >
              Активировать
            </ButtonWithProgress>}
        </Grid>
      </Grid>
    </Stack>
  );
};

export default WarehouseEditForm;