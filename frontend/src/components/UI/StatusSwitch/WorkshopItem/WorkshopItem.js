import React from 'react';
import {Link} from 'react-router-dom';
import {Button, Grid, Typography} from "@mui/material";
import {useStyles} from "../../../../containers/MainPages/AdminPage/Warehouses/Modal.styles";

const WorkshopItem = ({id, name, status}) => {
  const classes = useStyles();
  return (
    <Grid
      container
      item
      xs={12}
      spacing={3}
      justifyContent={"space-around"}
      alignItems={"center"}
    >
      <Grid
        item
        xs={12}
        sm={"auto"}
        md={4}
        textAlign={"center"}
      >
        <Typography
          variant={"body1"}
        >
          {name}
        </Typography>
      </Grid>
      {status ?
        <Grid item xs={12} sm={"auto"} md={4} textAlign={"center"}>
          <Typography
            className={classes.status}
            style={{
              backgroundColor: 'green'
            }}
          >
            Активный
          </Typography>
        </Grid> :
        <Grid item xs={12} sm={"auto"} md={4} textAlign={"center"}>
          <Typography
            className={classes.status}
            style={{
              backgroundColor: 'red'
            }}
          >
            Неактивный
          </Typography>
        </Grid>}
      <Grid
        item
        xs={12}
        sm={'auto'}
        md={4}
      >
        <Button
          variant={"contained"}
          fullWidth
          component={Link}
          to={"/admin/workshops/edit/" + id}
        >
          Редактировать
        </Button>
      </Grid>
    </Grid>
  );
};

export default WorkshopItem;