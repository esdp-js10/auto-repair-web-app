import * as React from 'react';
import {styled} from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import ListItemButton from "@mui/material/ListItemButton";
import {Link} from "react-router-dom";
import ListItemText from "@mui/material/ListItemText";

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({theme}) => ({
  "&.MuiPaper-root": {
    backgroundColor: 'inherit',
  },
  "& .MuiAccordionSummary-root": {
    color: 'inherit',
    borderRadius:  '50%',
  },
  "& .MuiAccordionSummary-root.MuiButtonBase-root": {
    '&:hover': {
      background: theme.palette.nav.hoverDark,
    },
    padding: '5px 10px',
  },
  "& > .MuiListItemButton-root": {
    color: theme.palette.text.navBarLight,
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{fontSize: '0.9rem'}}/>}
    {...props}
  />
))(({theme}) => ({
  minHeight: 0,
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-expandIconWrapper': {
    transform: 'rotate(180deg)',
    color: 'inherit',
  },
  '& .MuiPaper-root-MuiAccordion-root': {
    backgroundColor: theme.palette.background.dark_light,
    color: theme.palette.text.navBarLight,
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({theme}) => ({
  padding: 0,
  borderTop: '1px solid rgba(0, 0, 0, .125)',
  backgroundColor: theme.palette.background.dark_light,
}));

const NavbarAccordion = ({children, panelText, linkTo, linkText, onSelectClick, selected}) => {
  const [expanded, setExpanded] = React.useState('');

  const handleChange = (panel) => (event, newExpanded) => {
      setExpanded(newExpanded ? panel : false);
    };

  return (
    <Accordion expanded={expanded === panelText} onChange={handleChange(panelText)}>
      <ListItemButton component={Link} to={linkTo}
                      onClick={onSelectClick}
                      selected={selected}
      >
        <ListItemText primary={linkText}/>
        <AccordionSummary
          onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}/>
      </ListItemButton>
      <AccordionDetails>
        {children}
      </AccordionDetails>
    </Accordion>
  );
};

export default NavbarAccordion;