import React from 'react';
import List from "@mui/material/List";
import {styled} from "@mui/styles";

const NavbarList = styled((props) => (<List component={'nav'} {...props}/>))(({theme}) => ({
  width: '100%',
  bgcolor: theme.palette.background.dark,
  color: theme.palette.text.navBarLight,
  padding: 0,
}))

export default NavbarList;