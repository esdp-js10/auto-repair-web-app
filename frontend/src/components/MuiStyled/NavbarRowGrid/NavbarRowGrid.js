import {styled} from "@mui/styles";
import {Grid} from "@mui/material";
import React from "react";

const NavbarRowGrid = styled(props => (<Grid {...props}/>))(({theme}) => ({
  listStyle: "none",
  display: 'flex',
  alignItems: 'flex-end',
  flexWrap: 'noWrap',
  "& .MuiListItemIcon-root": {
    color: 'inherit',
  },
  "& a": {
    fontStyle: 'normal',
    textDecoration: 'none',
    text: 'none',
    color: theme.palette.text.navBar,
    fontSize: '16px',
    fontWeight: 500,
  },
  "& a:hover": {
    color: theme.palette.nav.hoverLight,
  },
  "& a.active": {
    color: theme.palette.nav.activeLight,
  },
}));

export default NavbarRowGrid;