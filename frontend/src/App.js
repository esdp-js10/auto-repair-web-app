import React from 'react';
import Layout from './components/UI/Layout/Layout';
import Main from './containers/MainPages/Main/Main';
import AuthPage from './containers/MainPages/AuthPage/AuthPage';
import {Route, Routes} from "react-router-dom";
import CreateOrder from "./containers/OrderPage/CreateOrder/CreateOrder";
import AdminPage from "./containers/MainPages/AdminPage/AdminPage";
import OrderPage from "./containers/OrderPage/OrderPage";
import RegClientsRequestsPage from "./containers/MainPages/AdminPage/RegClientsRequestsPage/RegClientsRequestsPage";
import {useSelector} from "react-redux";
import CatalogGDT from "./containers/MainPages/CatalogGDT/CatalogGDT";
import {
  A_CLIENTS_REG_REQ_PATH,
  A_DETAILS_ADD_PARTNUMBERS_PATH,
  A_WAREHOUSES_DETAILS_PATH,
  A_EMPLOYEES_ADD_PATH,
  A_EMPLOYEES_EDIT_PATH,
  A_EMPLOYEES_PATH,
  A_NEWS_ADD_PATH,
  A_NEWS_EDIT_PATH,
  A_WAREHOUSES_INCOME_PATH,
  A_WAREHOUSES_PATH,
  A_WORKSHOPS_ADD_PATH,
  A_WORKSHOPS_EDIT_PATH,
  A_WORKSHOPS_PATH,
  CATALOG_PATH,
  LOGIN_PATH,
  ORDERS_ADD_PATH,
  ORDERS_PATH,
  R_ORDERS_EDIT_PATH,
  R_ORDERS_PATH,
  REGISTER_PATH,
  W_WAREHOUSES_INCOME_PATH,
  W_WAREHOUSES_PATH,
  W_WAREHOUSES_TRANSFERS_PATH,
  R_ORDERS_HISTORY_PATH,
  W_WAREHOUSES_CURRENT_PATH,
  W_WAREHOUSES_UPCOMING_PATH, A_GDT_FORM, A_GDT_EDIT_FORM_ID, UPDATE_USER_PATH, A_WORKSHOPS_HISTORY_PATH,
} from "./constants/paths";
import DetailPage from "./containers/MainPages/AdminPage/DetailsPage/DetailPage";
import AddPartNumbersOfDetail
  from "./containers/MainPages/AdminPage/DetailsPage/AddPartNumbersOfDetail/AddPartNumbersOfDetail";
import IncomeDetails from "./containers/MainPages/IncomeDetails/IncomeDetails";
import AdminEmployeePage from "./containers/MainPages/AdminPage/Employees/AdminEmployeePage";
import AddEmployee from "./containers/MainPages/AdminPage/Employees/AddEmployee/AddEmployee";
import EditEmployee from "./containers/MainPages/AdminPage/Employees/EditEmployee/EditEmployee";
import NewsForm from "./components/UI/NewsForm/NewsForm";
import WorkshopForm from "./components/UI/WorkshopForms/WorkshopForm";
import WorkshopEditorForm from "./components/UI/WorkshopForms/WorkshopEditorForm";
import WarehousemanWarehousePage from "./containers/MainPages/WarehousemanWarehousePage/WarehousemanWarehousePage";
import IncomeNewDetails from "./containers/MainPages/WarehousemanWarehousePage/IncomeNewDetails/IncomeNewDetails";
import MigrationHistory from "./containers/MainPages/WarehousemanWarehousePage/MigrationHistory/MigrationHistory";
import ReceiveOrders from "./containers/MainPages/AdminPage/ReceiveOrders/ReceiveOrders";
import RepairPage from "./containers/MainPages/RepairPage/RepairPage";
import WorkShopPage from "./containers/MainPages/AdminPage/WorkShopPage/WorkShopPage";
import NotFoundPage from "./containers/MainPages/NotFoundPage/NotFoundPage";
import {ADMIN_ROLE, REPAIRER_ROLE, WAREHOUSEMAN_ROLE} from "./constants/userRoles";
import OrdersHistory from "./containers/MainPages/AdminPage/OrdersHistory/OrdersHistory";
import Warehouses from "./containers/MainPages/WarehousemanWarehousePage/Warehouses/Warehouses";
import IncomeHistory from "./containers/MainPages/WarehousemanWarehousePage/IncomeHistory/IncomeHistory";
import GDTForm from "./components/AdminOffice/GDTForms/GDTForm";
import GdtEditForm from "./components/AdminOffice/GDTForms/GDTEditForm";
import EditUserProfile from "./containers/MainPages/AuthPage/EditUserProfile/EditUserProfile";

const App = () => {
  const profile = useSelector(state => state.users.profile);

  return (
    <Layout>
      <Routes>
        <Route path="/" element={<Main/>}/>
        <Route path={LOGIN_PATH} element={<AuthPage/>}/>
        <Route path={REGISTER_PATH} element={<AuthPage isAuth={false}/>}/>
        <Route path={CATALOG_PATH} element={<CatalogGDT/>}/>
        {profile && (
          <>
            <Route path={ORDERS_PATH} element={<OrderPage/>}/>
            <Route path={ORDERS_ADD_PATH} element={<CreateOrder/>}/>
            <Route path={UPDATE_USER_PATH} element={<EditUserProfile/>}/>
          </>
        )}
        {profile?.role && (
          profile?.role === ADMIN_ROLE ? (
            <>
              <Route path={A_CLIENTS_REG_REQ_PATH} element={<RegClientsRequestsPage/>}/>
              <Route path={A_WAREHOUSES_PATH} element={<AdminPage/>}/>
              <Route path={A_WAREHOUSES_DETAILS_PATH} element={<DetailPage/>}/>
              <Route path={A_DETAILS_ADD_PARTNUMBERS_PATH} element={<AddPartNumbersOfDetail/>}/>
              <Route path={A_WAREHOUSES_INCOME_PATH} element={<IncomeDetails/>}/>
              <Route path={A_EMPLOYEES_PATH} element={<AdminEmployeePage/>}/>
              <Route path={A_EMPLOYEES_ADD_PATH} element={<AddEmployee/>}/>
              <Route path={A_EMPLOYEES_EDIT_PATH} element={<EditEmployee/>}/>
              <Route path={A_NEWS_ADD_PATH} element={<NewsForm/>}/>
              <Route path={A_NEWS_EDIT_PATH} element={<NewsForm/>}/>
              <Route path={A_WORKSHOPS_PATH} element={<WorkShopPage/>}/>
              <Route path={A_WORKSHOPS_HISTORY_PATH} element={<OrdersHistory/>}/>
              <Route path={A_WORKSHOPS_ADD_PATH} element={<WorkshopForm/>}/>
              <Route path={A_WORKSHOPS_EDIT_PATH} element={<WorkshopEditorForm/>}/>
              <Route path={A_GDT_FORM} element={<GDTForm/>}/>
              <Route path={A_GDT_EDIT_FORM_ID} element={<GdtEditForm/>}/>
            </>
          ) : (profile?.role === REPAIRER_ROLE ? (
            <>
              <Route path={R_ORDERS_HISTORY_PATH} element={<OrdersHistory/>}/>
              <Route path={R_ORDERS_PATH} element={<ReceiveOrders/>}/>
              <Route path={R_ORDERS_EDIT_PATH} element={<RepairPage/>}/>
            </>
          ) : (profile?.role === WAREHOUSEMAN_ROLE && (
            <>
              <Route path={W_WAREHOUSES_CURRENT_PATH} element={<WarehousemanWarehousePage/>}/>
              <Route path={W_WAREHOUSES_PATH} element={<Warehouses/>}/>
              <Route path={W_WAREHOUSES_UPCOMING_PATH} element={<IncomeHistory/>}/>
              <Route path={W_WAREHOUSES_INCOME_PATH} element={<IncomeNewDetails/>}/>
              <Route path={W_WAREHOUSES_TRANSFERS_PATH} element={<MigrationHistory/>}/>
            </>
          )))
        )}
        <Route path={"*"} element={<NotFoundPage/>}/>
      </Routes>
    </Layout>
  );
};

export default App;


