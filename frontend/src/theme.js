import {createTheme} from "@mui/material/styles";

const mainBgColor = '#FBFBFB';

const theme = createTheme({
  palette: {
    primary: {
      main: '#4D77E3',
      light: '#4063be',
    },
    action1: {
      main: '#122c86',
      contrastText: 'white',
    },
    dark: {
      main: '#1D1D1F',
      contrastText: 'white',
    },
    nav: {
      main: 'black',
      activeLight: 'white',
      hoverLight: '#8cb9fc',
      hoverDark: 'rgba(77, 119, 227, 0.08)',
      contrastText: 'rgba(234,234,234,0.8)',
    },
    text: {
      navBar: "rgba(234,234,234,0.8)",
      navBarLight: "white",
    },
    background: {
      main: '#FBFBFB',
      dark: '#1D1D1F',
      dark_light: '#3b3b3f',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
        },
      },
    },
    MuiBackdrop: {
      styleOverrides: {
        root: {
          backgroundColor: 'rgba(0,0,0,0.1)',
        }
      }
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          backgroundColor: 'white',
          "& .MuiFormHelperText-root": {
            backgroundColor: mainBgColor,
            padding: '0 10px',
            margin: 0,
          },
        },
      },
    },
  },
  mixins: {
    toolbar: {
      minHeight: 80,
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      xsm: 420,
      sm: 600,
      md: 920,
      lg: 1200,
      xl: 1536,
    },
  },
});

theme.components.MuiToolbar = {
  styleOverrides: {
    root: {
      maxWidth: 1250,
      width: '100%',
      margin: '0 auto',
      padding: '0 10px',
      [theme.breakpoints.up('sm')]: {
        padding: '0 10px',
      },
    },
  },
}

theme.components.MuiTextField = {
  styleOverrides: {
    root: {
      backgroundColor: 'white',
      "& .MuiFormHelperText-root": {
        backgroundColor: theme.palette.background.main,
        padding: '0 10px',
        margin: 0,
      },
    },
  },
}

theme.typography.h4 = {
  fontSize: '2.125rem',
  fontWeight: 300,
  [theme.breakpoints.down('sm')]: {
    fontSize: '1.5rem',
  },
}
export default theme;