export let BASE_URL = 'http://localhost:8000';
export const USERS_URL = '/users';
export const CREATE_EMPLOYEE_URL = '/employees/new';
export const GDT_URL = '/GDTs';
export const GDT_ALL_URL = '/GDTs/all';
export const GDT_SEARCH_OPTIONS_URL = '/GDTs/options';
export const USERS_REG_REQUESTS_URL = '/users/inactive';
export const USER_STATUS_CHANGE_URL = '/users/active/';
export const USER_REFRESH_TOKEN_URL = '/refresh';
export const EMPLOYEES_LOG_IN_URL = '/employees/login';
export const USER_LOG_IN_URL = '/users/login';
export const USER_REG_URL = '/users/registration';
export const USER_LOG_OUT_URL = '/users/logout';
export const EMPLOYEE_LOG_OUT_URL = '/employees/logout';

if (process.env.REACT_APP_ENV === 'test') {
  BASE_URL = 'http://localhost:8010';
}

if (process.env.NODE_ENV === 'production') {
  BASE_URL = 'https://gdt-bishkek.sytes.net/api';
}