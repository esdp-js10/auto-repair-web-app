const ApiError = require("../jwt/exceptions/api-error");
const tokenService = require("../jwt/service/token-service");
const UserModel = require("../models/User");

const authForUser = async (req, res, next) => {
  try {
  const authorizationHeader = req.headers.authorization;

  if (!authorizationHeader) {
    return res.status(401).send({error: 'No token present'});
  }
  const accessToken = authorizationHeader.split(' ')[1];
  if (!accessToken) {
    return next(ApiError.UnauthorizedError());
  }
  const userData = tokenService.validateAccessToken(accessToken);
  if (!userData) {
    return next(ApiError.UnauthorizedError());
  }
    const userDB = await UserModel.findById(userData._id)
    if (userDB.status === 'inactive') {
      return next(ApiError.UnauthorizedError());
    }

  req.user = userData;

  next();
} catch (err) {
  return next(ApiError.UnauthorizedError());
}

};

module.exports = authForUser;