const ApiError = require('../jwt/exceptions/api-error');
const mongoose = require("mongoose");

module.exports = function (err, req, res, next) {
  if (err instanceof ApiError){
    return res.status(err.status).send({message: err.message, errors: err.errors})
  }
  if (err instanceof mongoose.Error){
    if (err.name === 'ValidationError'){
      return res.status(400).json(err);
    }
  }
  return res.status(500).send({message: err.message});

};