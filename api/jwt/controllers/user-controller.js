const userService = require('../service/user-service');

class UserController {

  async login(req, res, next) {
    try {
      const {email, password} = req.body;
      const userData = await userService.login(email, password)
      res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true,})//"secure: true" on production need to be activated with https
      return res.json(userData);
    } catch (err) {
      next(err)
    }
  }

  async registration(req, res, next) {
    try {
      const {email, password, confirmPassword, name, phone, address} = req.body;
      const newUserData = {email, password, confirmPassword, name, phone, address};
      const userData = await userService.registration(newUserData, req.file);

      return res.json(userData);
    } catch (err) {
      next(err)
    }
  }

  async loginEmployee(req, res, next) {
    try {
      const {email, password} = req.body;
      const userData = await userService.loginEmployee(email, password)

      res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true,})//"secure: true" on production need to be activated with https
      return res.json(userData);
    } catch (err) {
      next(err)
    }
  }

  async logout(req, res, next) {
    try {
      const {refreshToken} = req.cookies;
      const token = await userService.logout(refreshToken);
      res.clearCookie('refreshToken');
      return res.json(token);
    } catch (err) {
      next(err)
    }
  }

  async refresh(req, res, next) {
    try {
      const {refreshToken} = req.cookies;
      const userData = await userService.refresh(refreshToken);
      res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true,})//"secure: true" on production need to be activated with https
      return res.json(userData);
    } catch (err) {
      next(err)
    }
  }
}

module.exports = new UserController();