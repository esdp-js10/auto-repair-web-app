module.exports = class EmployeeDto {
  _id;
  email;
  name;
  INN;
  phoneNumber;
  address;
  avatar;
  role;
  workshop;
  warehouse;
  status;

  constructor(model) {
    this._id = model._id;
    this.email = model.email;
    this.name = model.name;
    this.INN = model.INN;
    this.phoneNumber = model.phoneNumber;
    this.address = model.address;
    this.avatar = model.avatar;
    this.role = model.role;
    this.workshop = model.workshop;
    this.warehouse = model.warehouse;
    this.status = model.status;
  }
}