module.exports = class UserDto {
  _id;
  email;
  name;
  phone;
  address;
  status;
  creation_date;
  avatar;

  constructor(model) {
    this._id = model._id;
    this.email = model.email;
    this.name = model.name;
    this.phone = model.phone;
    this.address = model.address;
    this.status = model.status;
    this.creation_date = model.creation_date;
    this.avatar = model.avatar;
  }
}