const UserModel = require('../../models/User');
const EmployeeModel = require('../../models/Employee');
const bcrypt = require('bcrypt');
const mailService = require('./mail-service');
const tokenService = require('./token-service');
const UserDto = require('../dtos/user-dto');
const EmployeeDto = require('../dtos/employee-dto');
const ApiError = require('../exceptions/api-error');

class UserService {

  async login(email, password) {
    const user = await UserModel.findOne({email})
    if (!user) {
      throw ApiError.BadLoginRequest('Пароль или электронная почта неверны');
    }
    if (user.status !== 'active') {
      throw ApiError.DeactivatedRequest();
    }

    const isPassEquals = await bcrypt.compare(password, user.password);
    if (!isPassEquals) {
      throw ApiError.BadLoginRequest('Пароль или электронная почта неверны');
    }
    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({...userDto});
    await tokenService.saveTokenUser(userDto._id, tokens.refreshToken);

    return {...userDto, ...tokens}
  }

  async registration(userData, imgFile) {
    if (userData.password !== userData.confirmPassword) {
      throw ApiError.BadRequest(`Пароли не совпадают`)
    }
    const user = new UserModel({
      ...userData,
      avatar: imgFile ? '/uploads/' + imgFile.filename : null,
      creation_date: new Date(),
    })

    await user.save();

    const htmlMsg = `
    <div>
      <h1>Ожидайте решения администратора.</h1>
      <p>После чего ваш аккаунт активируется и вы сможете войти на сайт ${process.env.CLIENT_URL}.</p>
    </div>`;
    mailService.sendMsgToMail(user.email, `Ваша регистрация на сайте ${process.env.CLIENT_URL} принята`, htmlMsg);

    const userDto = new UserDto(user);
    return {...userDto};
  }


  async loginEmployee(email, password) {
    const user = await EmployeeModel.findOne({email})
    if (!user) {
      throw ApiError.BadLoginRequest('Пароль или электронная почта неверны');
    }
    if (user.status !== true) {
      throw ApiError.DeactivatedRequest();
    }
    const isPassEquals = await bcrypt.compare(password, user.password);
    if (!isPassEquals) {
      throw ApiError.BadLoginRequest('Пароль или электронная почта неверны');
    }
    const employeeDto = new EmployeeDto(user);
    const tokens = tokenService.generateTokens({...employeeDto});
    await tokenService.saveTokenEmployee(employeeDto._id, tokens.refreshToken);

    return {...employeeDto, ...tokens}
  }

  async logout(refreshToken) {
    return await tokenService.removeToken(refreshToken);
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }
    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await tokenService.findToken(refreshToken);

    if (!userData || !tokenFromDb) {
      throw ApiError.UnauthorizedError();
    }

    let user;
    let userDto;
    if (tokenFromDb.user) {
      if ((userData.status !== 'active')) {
        throw ApiError.DeactivatedRequest();
      }
      user = await UserModel.findById(userData._id);
      userDto = new UserDto(user);

    } else if (tokenFromDb.employee) {

      if ((userData.status !== true)) {
        throw ApiError.DeactivatedRequest();
      }
      user = await EmployeeModel.findById(userData._id);
      userDto = new EmployeeDto(user);

    } else {
      throw new ApiError.BadRequest('Несуществующий тип пользователя');
    }

    const tokens = tokenService.generateTokens({...userDto});
    if (tokenFromDb.user) {
      await tokenService.saveTokenUser(userDto._id, tokens.refreshToken);
    } else if (tokenFromDb.employee) {
      await tokenService.saveTokenEmployee(userDto._id, tokens.refreshToken);
    } else {
      throw new ApiError.BadRequest('Несуществующий тип пользователя');
    }

    return {...userDto, ...tokens}
  }
}


module.exports = new UserService();