const jwt = require('jsonwebtoken');
const TokenModel = require('../../models/Token')

class TokenService {
  generateTokens(payload) {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {expiresIn: "30min"})
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {expiresIn: "30d"})
    return {
      accessToken,
      refreshToken
    }
  }

  async saveTokenUser(userId, refreshToken) {
    const tokenData = await TokenModel.findOne({user: userId})
    if (tokenData) {
      tokenData.refreshToken = refreshToken;
      return await tokenData.save();
    }
    return await TokenModel.create({user: userId, refreshToken});
  }

  async saveTokenEmployee(userId, refreshToken) {
    const tokenData = await TokenModel.findOne({employee: userId})
    if (tokenData) {
      tokenData.refreshToken = refreshToken;
      return await tokenData.save();
    }
    return await TokenModel.create({employee: userId, refreshToken});
  }

  async removeToken(refreshToken) {
    return TokenModel.deleteOne({refreshToken});
  }

  findToken(refreshToken) {
     return TokenModel.findOne({refreshToken});
  }

  validateAccessToken(token) {
    try {
      return jwt.verify(token, process.env.JWT_ACCESS_SECRET);
    } catch (err) {
return null;
    }
  }

  validateRefreshToken(token) {
    try {
      return jwt.verify(token, process.env.JWT_REFRESH_SECRET);
    } catch (err) {
      return null;
    }
  }
}

module.exports = new TokenService();