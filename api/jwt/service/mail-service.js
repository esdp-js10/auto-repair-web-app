const nodemailer = require("nodemailer");
const config = require('../../config');
const {google} = require("googleapis");

class MailService {

  constructor() {
    (async () => {
      await this._createTransporter();
    })()
  }

  async sendMsgToMail(toEmail, msgSubject, msgHtml) {
    try {
      await this.transporter.sendMail({
        from: `ГДТ Бишкек <${process.env.SMTP_USER}>`,
        to: toEmail,
        subject: msgSubject,
        text: '',
        html: msgHtml,
      });
    } catch (err) {
      console.log(err);
      return null
    }
  }

  async _createTransporter() {
    try {
      const REDIRECT_URI = "https://developers.google.com/oauthplayground";

      const oAuth2Client = new google.auth.OAuth2(
        config.gmailApi.clientId,
        config.gmailApi.clientSecret,
        REDIRECT_URI,
      );

      oAuth2Client.setCredentials({refresh_token: config.gmailApi.refreshToken});

      const accessToken = await oAuth2Client.getAccessToken();

      this.transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          type: 'OAuth2',
          user: process.env.SMTP_USER,
          clientId: config.gmailApi.clientId,
          clientSecret: config.gmailApi.clientSecret,
          refreshToken: config.gmailApi.refreshToken,
          accessToken: accessToken,
        },
      });
    } catch (err) {
      return null;
    }
  }
}

module.exports = new MailService();