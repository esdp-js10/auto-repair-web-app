const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const DetailsSchema = new mongoose.Schema({
	typeOfAction: String,
	type: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'TypeDetail',
	},
	partNumber: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'PartNumberDetail',
		required: true,
	},
	quantity: {
		type: Number,
		required: true,
		min: [1, 'Мининальное количество 1'],
	},
	priceInitial: {
		type: String,
	},
});

const AccountsDetailSchema = new mongoose.Schema({
	fromWarehouse: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Warehouse',
	},
	toWarehouse: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Warehouse',
	},
	employee: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Employee',
	},
	date: {
		type: String,
		required: true,
	},
	details: {
		type: [DetailsSchema],
	},
	deliveryPrice: String,
	commissionPrice: String,
	customPrice: String,
	totalPrice: String,
	title: String,
	description: String,
	typeOfAction: {
		type: String,
		required: true,
		enum: ['expense', 'income', 'migration'],
	}
});

DetailsSchema.path('priceInitial').required(function () {
	return this.typeOfAction === 'income';
}, 'Это обязательное поле!');

AccountsDetailSchema.plugin(idValidator);
const AccountsDetail = mongoose.model('AccountDetail', AccountsDetailSchema);
module.exports = AccountsDetail;