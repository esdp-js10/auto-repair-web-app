const mongoose = require('mongoose');
const {nanoid} = require("nanoid");

const CommentSchema = new mongoose.Schema({
  comment_text: String,
  created_date: {
    type: Date,
    default: new Date(),
  },
});

const OrderSchema = new mongoose.Schema({
  orderNumber: {
    type: String,
    default: nanoid(6),
    unique: true,
  },
  date: {
    type: Date,
    default: new Date(),
  },
  gdt_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'GDT',
  },
  description: {
    type: String,
    required: true,
  },
  workshop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Workshop',
    required: true,
  },
  repairer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee',
  },
  status: {
    type: String,
    required: true,
    enum: ['Новый', 'Принято', 'Отклонено', 'Выполнено'],
    default: 'Новый',
  },
  customer: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  comment: {
    type: [CommentSchema]
  },
});


OrderSchema.pre('save', async function (next) {
  const order = await Order.find({orderNumber: this.orderNumber});
  if(order){
    this.orderNumber = nanoid(6);
  }
  next();
});

const Order = mongoose.model('Order', OrderSchema);
module.exports = Order;