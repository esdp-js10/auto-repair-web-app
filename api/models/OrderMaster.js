const mongoose = require('mongoose');

const ReservationSchema = new mongoose.Schema({
  type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TypeDetail',
    required: true,
  },
  partNumber: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'PartNumberDetail',
    required: true,
  },
  reservedQuantity: {
    type: Number,
    required: true,
    min: 1,
  },
});

const CommentSchema = new mongoose.Schema({
  title: String,
  date: {
    type: Date,
    default: new Date(),
  },
});

const ExtraDetailsSchema = new mongoose.Schema({
  title: String,
  price: String,
});

const ExtraWorksSchema = new mongoose.Schema({
  title: String,
  price: String,
});

const OrderMasterSchema = new mongoose.Schema({
  order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order',
    required: true,
  },
  repairer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee',
    required: true,
  },
  reserved: [ReservationSchema],
  comments: {
    type: [CommentSchema],
  },
  extraDetails: {
    type: [ExtraDetailsSchema],
  },
  extraWorks: {
    type: [ExtraWorksSchema],
  },
});

const OrderMaster = mongoose.model('OrderMaster', OrderMasterSchema);
module.exports = OrderMaster;