const mongoose = require('mongoose');
const gdtConstants = require('../../constants/gdtConstants');

const FilterOptionsSchema = new mongoose.Schema({
  group: {
    type: String,
    enum: gdtConstants.filters,
    required: true,
  },
  value: {
    type: String,
    required: true,
    unique: true,
  }

});

/*[
  {
    group: 'serialNo',
    value: 'dlmm_L7T',
    _id: new ObjectId("6242b62698362599ab2c4d0e")
  },
  {
    group: 'brand',
    value: 'Karosa',
    _id: new ObjectId("6242b62698362599ab2c4d0f")
  },
  {
    group: 'brand',
    value: 'Bureko',
    _id: new ObjectId("6242b62698362599ab2c4d10")
  },
  {
    group: 'brand',
    value: 'Bureko',
    _id: new ObjectId("6242b62698362599ab2c4d11")
  },
  {
    group: 'brand',
    value: 'Audi',
    _id: new ObjectId("6242b62698362599ab2c4d12")
  },
  {
    group: 'brand',
    value: 'Bureko',
    _id: new ObjectId("6242b62698362599ab2c4d13")
  },
  {
    group: 'models',
    value: '72 Model',
    _id: new ObjectId("6242b62698362599ab2c4d14")
  },
  {
    group: 'models',
    value: '78 Model',
    _id: new ObjectId("6242b62698362599ab2c4d15")
  },
  {
    group: 'models',
    value: '17 Model',
    _id: new ObjectId("6242b62698362599ab2c4d16")
  },
  {
    group: 'models',
    value: '15 Model',
    _id: new ObjectId("6242b62698362599ab2c4d17")
  },
  {
    group: 'models',
    value: '34 Model',
    _id: new ObjectId("6242b62698362599ab2c4d18")
  }
]
[
  {
    group: 'serialNo',
    value: '7ByucPSC',
    _id: new ObjectId("6242b62798362599ab2c4d33")
  },
  {
    group: 'brand',
    value: 'Avia',
    _id: new ObjectId("6242b62798362599ab2c4d34")
  },
  {
    group: 'models',
    value: '59 Model',
    _id: new ObjectId("6242b62798362599ab2c4d35")
  },
  {
    group: 'models',
    value: '28 Model',
    _id: new ObjectId("6242b62798362599ab2c4d36")
  },
  {
    group: 'models',
    value: '24 Model',
    _id: new ObjectId("6242b62798362599ab2c4d37")
  },
  {
    group: 'models',
    value: '57 Model',
    _id: new ObjectId("6242b62798362599ab2c4d38")
  },
  {
    group: 'models',
    value: '60 Model',
    _id: new ObjectId("6242b62798362599ab2c4d39")
  }
]
[
  {
    group: 'serialNo',
    value: 'El9P2ZiQ',
    _id: new ObjectId("6242b62798362599ab2c4d50")
  },
  {
    group: 'brand',
    value: 'Jawa',
    _id: new ObjectId("6242b62798362599ab2c4d51")
  },
  {
    group: 'models',
    value: '69 Model',
    _id: new ObjectId("6242b62798362599ab2c4d52")
  },
  {
    group: 'models',
    value: '100 Model',
    _id: new ObjectId("6242b62798362599ab2c4d53")
  },
  {
    group: 'models',
    value: '85 Model',
    _id: new ObjectId("6242b62798362599ab2c4d54")
  },
  {
    group: 'models',
    value: '77 Model',
    _id: new ObjectId("6242b62798362599ab2c4d55")
  }
]
[
  {
    group: 'serialNo',
    value: 'QNCVM32U',
    _id: new ObjectId("6242b62798362599ab2c4d6b")
  },
  {
    group: 'brand',
    value: 'Kaipan',
    _id: new ObjectId("6242b62798362599ab2c4d6c")
  },
  {
    group: 'brand',
    value: 'Skoda',
    _id: new ObjectId("6242b62798362599ab2c4d6d")
  },
  {
    group: 'models',
    value: '67 Model',
    _id: new ObjectId("6242b62798362599ab2c4d6e")
  },
  {
    group: 'models',
    value: '21 Model',
    _id: new ObjectId("6242b62798362599ab2c4d6f")
  },
  {
    group: 'models',
    value: '48 Model',
    _id: new ObjectId("6242b62798362599ab2c4d70")
  },
  {
    group: 'models',
    value: '46 Model',
    _id: new ObjectId("6242b62798362599ab2c4d71")
  }
]
[
  {
    group: 'serialNo',
    value: 'n5pXddlH',
    _id: new ObjectId("6242b62798362599ab2c4d88")
  },
  {
    group: 'models',
    value: '51 Model',
    _id: new ObjectId("6242b62798362599ab2c4d89")
  },
  {
    group: 'models',
    value: '53 Model',
    _id: new ObjectId("6242b62798362599ab2c4d8a")
  }
]
[
  {
    group: 'serialNo',
    value: '003ajqxS',
    _id: new ObjectId("6242b62798362599ab2c4d9d")
  },
  {
    group: 'models',
    value: '45 Model',
    _id: new ObjectId("6242b62798362599ab2c4d9e")
  },
  {
    group: 'models',
    value: '97 Model',
    _id: new ObjectId("6242b62798362599ab2c4d9f")
  },
  {
    group: 'models',
    value: '79 Model',
    _id: new ObjectId("6242b62798362599ab2c4da0")
  },
  {
    group: 'models',
    value: '18 Model',
    _id: new ObjectId("6242b62798362599ab2c4da1")
  },
  {
    group: 'models',
    value: '9 Model',
    _id: new ObjectId("6242b62798362599ab2c4da2")
  }
]
[
  {
    group: 'serialNo',
    value: '306uEoe7',
    _id: new ObjectId("6242b62798362599ab2c4db8")
  },
  {
    group: 'models',
    value: '82 Model',
    _id: new ObjectId("6242b62798362599ab2c4db9")
  },
  {
    group: 'models',
    value: '6 Model',
    _id: new ObjectId("6242b62798362599ab2c4dba")
  },
  {
    group: 'models',
    value: '70 Model',
    _id: new ObjectId("6242b62798362599ab2c4dbb")
  },
  {
    group: 'models',
    value: '55 Model',
    _id: new ObjectId("6242b62798362599ab2c4dbc")
  }
]
[
  {
    group: 'serialNo',
    value: '5nQxuWdX',
    _id: new ObjectId("6242b62898362599ab2c4dd1")
  },
  {
    group: 'brand',
    value: 'MTX / Metalex',
    _id: new ObjectId("6242b62898362599ab2c4dd2")
  },
  {
    group: 'brand',
    value: 'MTX / Metalex',
    _id: new ObjectId("6242b62898362599ab2c4dd3")
  },
  {
    group: 'models',
    value: '49 Model',
    _id: new ObjectId("6242b62898362599ab2c4dd4")
  },
  {
    group: 'models',
    value: '23 Model',
    _id: new ObjectId("6242b62898362599ab2c4dd5")
  }
]
[
  {
    group: 'serialNo',
    value: 'VQZTGQPh',
    _id: new ObjectId("6242b62898362599ab2c4dea")
  },
  {
    group: 'brand',
    value: 'Toyota',
    _id: new ObjectId("6242b62898362599ab2c4deb")
  },
  {
    group: 'models',
    value: '7 Model',
    _id: new ObjectId("6242b62898362599ab2c4dec")
  },
  {
    group: 'models',
    value: '4 Model',
    _id: new ObjectId("6242b62898362599ab2c4ded")
  },
  {
    group: 'models',
    value: '11 Model',
    _id: new ObjectId("6242b62898362599ab2c4dee")
  },
  {
    group: 'models',
    value: '16 Model',
    _id: new ObjectId("6242b62898362599ab2c4def")
  },
  {
    group: 'models',
    value: '94 Model',
    _id: new ObjectId("6242b62898362599ab2c4df0")
  }
]
[
  {
    group: 'serialNo',
    value: '3jqNZPn5',
    _id: new ObjectId("6242b62898362599ab2c4e07")
  },
  {
    group: 'brand',
    value: 'Gemballa',
    _id: new ObjectId("6242b62898362599ab2c4e08")
  },
  {
    group: 'models',
    value: '27 Model',
    _id: new ObjectId("6242b62898362599ab2c4e09")
  },
  {
    group: 'models',
    value: '32 Model',
    _id: new ObjectId("6242b62898362599ab2c4e0a")
  }
]
[
  {
    group: 'serialNo',
    value: 'KJJIZeGr',
    _id: new ObjectId("6242b62898362599ab2c4e1e")
  },
  {
    group: 'brand',
    value: 'Tatra',
    _id: new ObjectId("6242b62898362599ab2c4e1f")
  },
  {
    group: 'models',
    value: '88 Model',
    _id: new ObjectId("6242b62898362599ab2c4e20")
  },
  {
    group: 'models',
    value: '12 Model',
    _id: new ObjectId("6242b62898362599ab2c4e21")
  },
  {
    group: 'models',
    value: '75 Model',
    _id: new ObjectId("6242b62898362599ab2c4e22")
  },
  {
    group: 'models',
    value: '58 Model',
    _id: new ObjectId("6242b62898362599ab2c4e23")
  }
]
[
  {
    group: 'serialNo',
    value: 'OhDBtrmo',
    _id: new ObjectId("6242b62898362599ab2c4e39")
  },
  {
    group: 'brand',
    value: 'MW Motors',
    _id: new ObjectId("6242b62898362599ab2c4e3a")
  },
  {
    group: 'models',
    value: '84 Model',
    _id: new ObjectId("6242b62898362599ab2c4e3b")
  },
  {
    group: 'models',
    value: '68 Model',
    _id: new ObjectId("6242b62898362599ab2c4e3c")
  },
  {
    group: 'models',
    value: '84 Model',
    _id: new ObjectId("6242b62898362599ab2c4e3d")
  },
  {
    group: 'models',
    value: '90 Model',
    _id: new ObjectId("6242b62898362599ab2c4e3e")
  }
]
[
  {
    group: 'serialNo',
    value: 'Dt5sZ_wb',
    _id: new ObjectId("6242b62898362599ab2c4e54")
  },
  {
    group: 'models',
    value: '30 Model',
    _id: new ObjectId("6242b62898362599ab2c4e55")
  },
  {
    group: 'models',
    value: '30 Model',
    _id: new ObjectId("6242b62898362599ab2c4e56")
  },
  {
    group: 'models',
    value: '63 Model',
    _id: new ObjectId("6242b62898362599ab2c4e57")
  },
  {
    group: 'models',
    value: '8 Model',
    _id: new ObjectId("6242b62898362599ab2c4e58")
  },
  {
    group: 'models',
    value: '38 Model',
    _id: new ObjectId("6242b62898362599ab2c4e59")
  }
]
[
  {
    group: 'serialNo',
    value: '-AO3A19G',
    _id: new ObjectId("6242b62898362599ab2c4e6f")
  },
  {
    group: 'models',
    value: '42 Model',
    _id: new ObjectId("6242b62898362599ab2c4e70")
  },
  {
    group: 'models',
    value: '54 Model',
    _id: new ObjectId("6242b62898362599ab2c4e71")
  },
  {
    group: 'models',
    value: '35 Model',
    _id: new ObjectId("6242b62898362599ab2c4e72")
  }
]
[
  {
    group: 'serialNo',
    value: 'wu8u17tL',
    _id: new ObjectId("6242b62898362599ab2c4e86")
  },
  {
    group: 'models',
    value: '93 Model',
    _id: new ObjectId("6242b62898362599ab2c4e87")
  }
]
[
  {
    group: 'serialNo',
    value: 'p2KKkwHn',
    _id: new ObjectId("6242b62898362599ab2c4e99")
  },
  {
    group: 'models',
    value: '64 Model',
    _id: new ObjectId("6242b62898362599ab2c4e9a")
  },
  {
    group: 'models',
    value: '5 Model',
    _id: new ObjectId("6242b62898362599ab2c4e9b")
  },
  {
    group: 'models',
    value: '13 Model',
    _id: new ObjectId("6242b62898362599ab2c4e9c")
  }
]
[
  {
    group: 'serialNo',
    value: 'cqpCEV8U',
    _id: new ObjectId("6242b62898362599ab2c4eb0")
  },
  {
    group: 'models',
    value: '66 Model',
    _id: new ObjectId("6242b62898362599ab2c4eb1")
  },
  {
    group: 'models',
    value: '65 Model',
    _id: new ObjectId("6242b62898362599ab2c4eb2")
  }
]
[
  {
    group: 'serialNo',
    value: 'v60_R2ev',
    _id: new ObjectId("6242b62898362599ab2c4ec5")
  },
  {
    group: 'models',
    value: '47 Model',
    _id: new ObjectId("6242b62898362599ab2c4ec6")
  }
]
[
  {
    group: 'serialNo',
    value: '3qATR3nn',
    _id: new ObjectId("6242b62898362599ab2c4ed8")
  },
  {
    group: 'models',
    value: '40 Model',
    _id: new ObjectId("6242b62898362599ab2c4ed9")
  },
  {
    group: 'models',
    value: '96 Model',
    _id: new ObjectId("6242b62898362599ab2c4eda")
  }
]
[
  {
    group: 'serialNo',
    value: '4GX1M9z8',
    _id: new ObjectId("6242b62898362599ab2c4eed")
  },
  {
    group: 'models',
    value: '52 Model',
    _id: new ObjectId("6242b62898362599ab2c4eee")
  },
  {
    group: 'models',
    value: '10 Model',
    _id: new ObjectId("6242b62898362599ab2c4eef")
  },
  {
    group: 'models',
    value: '81 Model',
    _id: new ObjectId("6242b62898362599ab2c4ef0")
  },
  {
    group: 'models',
    value: '91 Model',
    _id: new ObjectId("6242b62898362599ab2c4ef1")
  }
]
[
  {
    group: 'serialNo',
    value: 'Gejn1sti',
    _id: new ObjectId("6242b62898362599ab2c4f06")
  },
  {
    group: 'models',
    value: '20 Model',
    _id: new ObjectId("6242b62898362599ab2c4f07")
  },
  {
    group: 'models',
    value: '29 Model',
    _id: new ObjectId("6242b62898362599ab2c4f08")
  },
  {
    group: 'models',
    value: '1 Model',
    _id: new ObjectId("6242b62898362599ab2c4f09")
  },
  {
    group: 'models',
    value: '37 Model',
    _id: new ObjectId("6242b62898362599ab2c4f0a")
  }
]
[
  {
    group: 'serialNo',
    value: 'ltE26p_d',
    _id: new ObjectId("6242b62898362599ab2c4f1f")
  },
  {
    group: 'models',
    value: '25 Model',
    _id: new ObjectId("6242b62898362599ab2c4f20")
  },
  {
    group: 'models',
    value: '98 Model',
    _id: new ObjectId("6242b62898362599ab2c4f21")
  }
]
[
  {
    group: 'serialNo',
    value: 'hvPuKTwi',
    _id: new ObjectId("6242b62898362599ab2c4f34")
  }
]
[
  {
    group: 'serialNo',
    value: 'Sf6FXynl',
    _id: new ObjectId("6242b62898362599ab2c4f45")
  }
]
[
  {
    group: 'serialNo',
    value: '5MgG0v5g',
    _id: new ObjectId("6242b62898362599ab2c4f56")
  },
  {
    group: 'models',
    value: '43 Model',
    _id: new ObjectId("6242b62898362599ab2c4f57")
  }
]
[
  {
    group: 'serialNo',
    value: 'eQb6n1g5',
    _id: new ObjectId("6242b62898362599ab2c4f69")
  },
  {
    group: 'models',
    value: '92 Model',
    _id: new ObjectId("6242b62898362599ab2c4f6a")
  }
]
[
  {
    group: 'serialNo',
    value: '-8W6jAFk',
    _id: new ObjectId("6242b62898362599ab2c4f7c")
  }
]
[
  {
    group: 'serialNo',
    value: 'TaXRDMUj',
    _id: new ObjectId("6242b62898362599ab2c4f8d")
  },
  {
    group: 'models',
    value: '89 Model',
    _id: new ObjectId("6242b62898362599ab2c4f8e")
  },
  {
    group: 'models',
    value: '44 Model',
    _id: new ObjectId("6242b62898362599ab2c4f8f")
  }
]
[
  {
    group: 'serialNo',
    value: 'JToKwYTa',
    _id: new ObjectId("6242b62898362599ab2c4fa2")
  },
  {
    group: 'models',
    value: '99 Model',
    _id: new ObjectId("6242b62898362599ab2c4fa3")
  }
]
[
  {
    group: 'serialNo',
    value: 'WnrVR5T4',
    _id: new ObjectId("6242b62898362599ab2c4fb5")
  }
]
[
  {
    group: 'serialNo',
    value: 'kAR5gGRf',
    _id: new ObjectId("6242b62898362599ab2c4fc6")
  },
  {
    group: 'models',
    value: '73 Model',
    _id: new ObjectId("6242b62898362599ab2c4fc7")
  }
]
[
  {
    group: 'serialNo',
    value: 'kNgg5T4w',
    _id: new ObjectId("6242b62898362599ab2c4fd9")
  }
]
[
  {
    group: 'serialNo',
    value: 'EVjlplxT',
    _id: new ObjectId("6242b62898362599ab2c4fea")
  },
  {
    group: 'models',
    value: '95 Model',
    _id: new ObjectId("6242b62898362599ab2c4feb")
  }
]
[
  {
    group: 'serialNo',
    value: 'MlXkJ9Fw',
    _id: new ObjectId("6242b62898362599ab2c4ffd")
  }
]
[
  {
    group: 'serialNo',
    value: 'hqEOlLl9',
    _id: new ObjectId("6242b62898362599ab2c500e")
  },
  {
    group: 'models',
    value: '2 Model',
    _id: new ObjectId("6242b62898362599ab2c500f")
  }
]
[
  {
    group: 'serialNo',
    value: 'djNkO41E',
    _id: new ObjectId("6242b62898362599ab2c5021")
  },
  {
    group: 'models',
    value: '3 Model',
    _id: new ObjectId("6242b62898362599ab2c5022")
  },
  {
    group: 'models',
    value: '56 Model',
    _id: new ObjectId("6242b62898362599ab2c5023")
  }
]
[
  {
    group: 'serialNo',
    value: 'IWqoS--Q',
    _id: new ObjectId("6242b62898362599ab2c5036")
  },
  {
    group: 'models',
    value: '26 Model',
    _id: new ObjectId("6242b62898362599ab2c5037")
  },
  {
    group: 'models',
    value: '36 Model',
    _id: new ObjectId("6242b62898362599ab2c5038")
  },
  {
    group: 'models',
    value: '33 Model',
    _id: new ObjectId("6242b62898362599ab2c5039")
  }
]
[
  {
    group: 'serialNo',
    value: '1Jad-Q3Z',
    _id: new ObjectId("6242b62898362599ab2c504d")
  }
]
[
  {
    group: 'serialNo',
    value: '8ecWy82R',
    _id: new ObjectId("6242b62898362599ab2c505e")
  },
  {
    group: 'models',
    value: '83 Model',
    _id: new ObjectId("6242b62898362599ab2c505f")
  }
]
[
  {
    group: 'serialNo',
    value: 'HRDyli-O',
    _id: new ObjectId("6242b62898362599ab2c5071")
  },
  {
    group: 'models',
    value: '62 Model',
    _id: new ObjectId("6242b62898362599ab2c5072")
  }
]
[
  {
    group: 'serialNo',
    value: 'Rq9aWRxn',
    _id: new ObjectId("6242b62898362599ab2c5084")
  },
  {
    group: 'models',
    value: '50 Model',
    _id: new ObjectId("6242b62898362599ab2c5085")
  }
]
[
  {
    group: 'serialNo',
    value: '0r65_iMf',
    _id: new ObjectId("6242b62898362599ab2c5097")
  },
  {
    group: 'models',
    value: '86 Model',
    _id: new ObjectId("6242b62898362599ab2c5098")
  }
]
[
  {
    group: 'serialNo',
    value: 'cwn4aTxO',
    _id: new ObjectId("6242b62898362599ab2c50aa")
  },
  {
    group: 'models',
    value: '87 Model',
    _id: new ObjectId("6242b62898362599ab2c50ab")
  }
]
[
  {
    group: 'serialNo',
    value: 't-BdByQE',
    _id: new ObjectId("6242b62898362599ab2c50bd")
  }
]
[
  {
    group: 'serialNo',
    value: 'XLIfrexC',
    _id: new ObjectId("6242b62898362599ab2c50ce")
  }
]
[
  {
    group: 'serialNo',
    value: 'XjK3exwv',
    _id: new ObjectId("6242b62898362599ab2c50df")
  }
]
[
  {
    group: 'serialNo',
    value: 'VQxLAUdv',
    _id: new ObjectId("6242b62898362599ab2c50f0")
  },
  {
    group: 'models',
    value: '41 Model',
    _id: new ObjectId("6242b62898362599ab2c50f1")
  }
]
[
  {
    group: 'serialNo',
    value: 'YZ4112oW',
    _id: new ObjectId("6242b62898362599ab2c5103")
  },
  {
    group: 'models',
    value: '74 Model',
    _id: new ObjectId("6242b62898362599ab2c5104")
  }
]
[
  {
    group: 'serialNo',
    value: 'sQmZZEPO',
    _id: new ObjectId("6242b62898362599ab2c5116")
  }
]
[
  {
    group: 'serialNo',
    value: 'a6XiEUdb',
    _id: new ObjectId("6242b62898362599ab2c5127")
  }
]
[
  {
    group: 'serialNo',
    value: '1eEUthCP',
    _id: new ObjectId("6242b62898362599ab2c5138")
  }
]
[
  {
    group: 'serialNo',
    value: 'UwPxBDLl',
    _id: new ObjectId("6242b62898362599ab2c5149")
  },
  {
    group: 'models',
    value: '80 Model',
    _id: new ObjectId("6242b62898362599ab2c514a")
  }
]
[
  {
    group: 'serialNo',
    value: 'aYQb5jps',
    _id: new ObjectId("6242b62898362599ab2c515c")
  },
  {
    group: 'models',
    value: '39 Model',
    _id: new ObjectId("6242b62898362599ab2c515d")
  }
]
[
  {
    group: 'serialNo',
    value: 'Sfa7x7CL',
    _id: new ObjectId("6242b62898362599ab2c516f")
  },
  {
    group: 'models',
    value: '76 Model',
    _id: new ObjectId("6242b62898362599ab2c5170")
  }
]
[
  {
    group: 'serialNo',
    value: 'oHIBOHbM',
    _id: new ObjectId("6242b62898362599ab2c5182")
  },
  {
    group: 'models',
    value: '61 Model',
    _id: new ObjectId("6242b62898362599ab2c5183")
  }
]
[
  {
    group: 'serialNo',
    value: '3SPzEgs5',
    _id: new ObjectId("6242b62898362599ab2c5195")
  }
]
[
  {
    group: 'serialNo',
    value: 'WTK0ZgKM',
    _id: new ObjectId("6242b62898362599ab2c51a6")
  },
  {
    group: 'models',
    value: '71 Model',
    _id: new ObjectId("6242b62898362599ab2c51a7")
  }
]
[
  {
    group: 'serialNo',
    value: 'Eyz0ai2R',
    _id: new ObjectId("6242b62898362599ab2c51b9")
  }
]
[
  {
    group: 'serialNo',
    value: 'wmDCCVjJ',
    _id: new ObjectId("6242b62898362599ab2c51ca")
  }
]
[
  {
    group: 'serialNo',
    value: 'NTF-g48K',
    _id: new ObjectId("6242b62898362599ab2c51db")
  }
]
[
  {
    group: 'serialNo',
    value: 'vr5F58OP',
    _id: new ObjectId("6242b62898362599ab2c51ec")
  }
]
[
  {
    group: 'serialNo',
    value: 'c5tcr79b',
    _id: new ObjectId("6242b62898362599ab2c51fd")
  }
]
[
  {
    group: 'serialNo',
    value: 'nb76VVot',
    _id: new ObjectId("6242b62898362599ab2c520e")
  }
]
[
  {
    group: 'serialNo',
    value: 'A-F0k8L4',
    _id: new ObjectId("6242b62898362599ab2c521f")
  },
  {
    group: 'models',
    value: '22 Model',
    _id: new ObjectId("6242b62898362599ab2c5220")
  }
]
[
  {
    group: 'serialNo',
    value: 'kL6ADanT',
    _id: new ObjectId("6242b62898362599ab2c5232")
  }
]
[
  {
    group: 'serialNo',
    value: 'KBFNj3aC',
    _id: new ObjectId("6242b62898362599ab2c5243")
  }
]
[
  {
    group: 'serialNo',
    value: 'TJPmPYFK',
    _id: new ObjectId("6242b62898362599ab2c5254")
  }
]
[
  {
    group: 'serialNo',
    value: 'kPUhf7Wk',
    _id: new ObjectId("6242b62898362599ab2c5265")
  }
]
[
  {
    group: 'serialNo',
    value: 'lhZGQeGC',
    _id: new ObjectId("6242b62898362599ab2c5276")
  }
]
[
  {
    group: 'serialNo',
    value: '02LBqGdT',
    _id: new ObjectId("6242b62898362599ab2c5287")
  }
]
[
  {
    group: 'serialNo',
    value: 'JuG8bX1Q',
    _id: new ObjectId("6242b62898362599ab2c5298")
  }
]
[
  {
    group: 'serialNo',
    value: 'sr1jkShk',
    _id: new ObjectId("6242b62898362599ab2c52a9")
  }
]
[
  {
    group: 'serialNo',
    value: 'NO0nVxNV',
    _id: new ObjectId("6242b62898362599ab2c52ba")
  }
]
[
  {
    group: 'serialNo',
    value: 'ilfBGWA3',
    _id: new ObjectId("6242b62898362599ab2c52cb")
  }
]
[
  {
    group: 'serialNo',
    value: 'E4me2KeT',
    _id: new ObjectId("6242b62898362599ab2c52dc")
  }
]
[
  {
    group: 'serialNo',
    value: 'jLOZAGBD',
    _id: new ObjectId("6242b62898362599ab2c52ed")
  },
  {
    group: 'models',
    value: '31 Model',
    _id: new ObjectId("6242b62898362599ab2c52ee")
  }
]
[
  {
    group: 'serialNo',
    value: 'MD7Zc6VP',
    _id: new ObjectId("6242b62898362599ab2c5300")
  }
]
[
  {
    group: 'serialNo',
    value: '3TXyzIz4',
    _id: new ObjectId("6242b62898362599ab2c5311")
  },
  {
    group: 'models',
    value: '14 Model',
    _id: new ObjectId("6242b62898362599ab2c5312")
  }
]
[
  {
    group: 'serialNo',
    value: 'W1NjxK8v',
    _id: new ObjectId("6242b62898362599ab2c5324")
  }
]
[
  {
    group: 'serialNo',
    value: 'TaLs-GCP',
    _id: new ObjectId("6242b62898362599ab2c5335")
  }
]
[
  {
    group: 'serialNo',
    value: 'W5wJQamX',
    _id: new ObjectId("6242b62898362599ab2c5346")
  }
]
[
  {
    group: 'serialNo',
    value: 'hvAfXgVW',
    _id: new ObjectId("6242b62898362599ab2c5357")
  }
]
[
  {
    group: 'serialNo',
    value: 'iJthP5M2',
    _id: new ObjectId("6242b62898362599ab2c5368")
  }
]
[
  {
    group: 'serialNo',
    value: 'BO5MDxf9',
    _id: new ObjectId("6242b62898362599ab2c5379")
  }
]
[
  {
    group: 'serialNo',
    value: 'eDLfGzOo',
    _id: new ObjectId("6242b62898362599ab2c538a")
  }
]
[
  {
    group: 'serialNo',
    value: '3GCaspeP',
    _id: new ObjectId("6242b62898362599ab2c539b")
  }
]
[
  {
    group: 'serialNo',
    value: 'k08Tro1A',
    _id: new ObjectId("6242b62898362599ab2c53ac")
  }
]
[
  {
    group: 'serialNo',
    value: 'Jw-Z-6bV',
    _id: new ObjectId("6242b62898362599ab2c53bd")
  }
]
[
  {
    group: 'serialNo',
    value: 'iDK1bTL-',
    _id: new ObjectId("6242b62898362599ab2c53ce")
  }
]
[
  {
    group: 'serialNo',
    value: 'cMDw3ixa',
    _id: new ObjectId("6242b62898362599ab2c53df")
  }
]
[
  {
    group: 'serialNo',
    value: 'avSj05bW',
    _id: new ObjectId("6242b62898362599ab2c53f0")
  }
]
[
  {
    group: 'serialNo',
    value: 'cX-fcCWq',
    _id: new ObjectId("6242b62898362599ab2c5401")
  }
]
[
  {
    group: 'serialNo',
    value: '1RtEWven',
    _id: new ObjectId("6242b62898362599ab2c5412")
  }
]
[
  {
    group: 'serialNo',
    value: 'geUSug3J',
    _id: new ObjectId("6242b62898362599ab2c5423")
  }
]
[
  {
    group: 'serialNo',
    value: 'WD09yRaf',
    _id: new ObjectId("6242b62898362599ab2c5434")
  }
]
[
  {
    group: 'serialNo',
    value: '2lHj4aE4',
    _id: new ObjectId("6242b62898362599ab2c5445")
  }
]
[
  {
    group: 'serialNo',
    value: 'DyxGzpPS',
    _id: new ObjectId("6242b62898362599ab2c5456")
  }
]
[
  {
    group: 'serialNo',
    value: 'Z0pThLcm',
    _id: new ObjectId("6242b62898362599ab2c5467")
  }
]
[
  {
    group: 'serialNo',
    value: 'dUEJAQzq',
    _id: new ObjectId("6242b62898362599ab2c5478")
  }
]
[
  {
    group: 'serialNo',
    value: 'ABGRGXhF',
    _id: new ObjectId("6242b62898362599ab2c5489")
  }
]
[
  {
    group: 'serialNo',
    value: '84rXkqW-',
    _id: new ObjectId("6242b62898362599ab2c549a")
  }
]
[
  {
    group: 'serialNo',
    value: '4Q-7nX3F',
    _id: new ObjectId("6242b62898362599ab2c54ab")
  }
]
[
  {
    group: 'serialNo',
    value: 'X2GgRJO4',
    _id: new ObjectId("6242b62898362599ab2c54bc")
  }
]
[
  {
    group: 'serialNo',
    value: 'kZbG8F6t',
    _id: new ObjectId("6242b62898362599ab2c54cd")
  }
]
[
  {
    group: 'serialNo',
    value: 'preQQB8g',
    _id: new ObjectId("6242b62898362599ab2c54de")
  },
  {
    group: 'models',
    value: '19 Model',
    _id: new ObjectId("6242b62898362599ab2c54df")
  }
]
[
  {
    group: 'serialNo',
    value: 'AOh3Qh7t',
    _id: new ObjectId("6242b62898362599ab2c54f1")
  }
]
[
  {
    group: 'serialNo',
    value: 'JppNh4DY',
    _id: new ObjectId("6242b62898362599ab2c5502")
  }
]
[
  {
    group: 'serialNo',
    value: 'XPLk7yVo',
    _id: new ObjectId("6242b62898362599ab2c5513")
  }
]
[
  {
    group: 'serialNo',
    value: 'swkwhcvi',
    _id: new ObjectId("6242b62898362599ab2c5524")
  }
]
[
  {
    group: 'serialNo',
    value: 'EL3UN29t',
    _id: new ObjectId("6242b62898362599ab2c5535")
  }
]
[
  {
    group: 'serialNo',
    value: 'idC8EWeM',
    _id: new ObjectId("6242b62898362599ab2c5546")
  }
]
[
  {
    group: 'serialNo',
    value: 'z0twIFZr',
    _id: new ObjectId("6242b62898362599ab2c5557")
  }
]
[
  {
    group: 'serialNo',
    value: 'utt-zSIH',
    _id: new ObjectId("6242b62898362599ab2c5568")
  }
]
[
  {
    group: 'serialNo',
    value: 'cp64Sh6S',
    _id: new ObjectId("6242b62898362599ab2c5579")
  }
]
[
  {
    group: 'serialNo',
    value: '0DRr0C6Z',
    _id: new ObjectId("6242b62898362599ab2c558a")
  }
]
[
  {
    group: 'serialNo',
    value: 'B1mfGdJO',
    _id: new ObjectId("6242b62898362599ab2c559b")
  }
]
[
  {
    group: 'serialNo',
    value: 'dmj0W757',
    _id: new ObjectId("6242b62898362599ab2c55ac")
  }
]
[
  {
    group: 'serialNo',
    value: 'A0CR-iHW',
    _id: new ObjectId("6242b62898362599ab2c55bd")
  }
]
[
  {
    group: 'serialNo',
    value: 'Zm17hUk3',
    _id: new ObjectId("6242b62898362599ab2c55ce")
  }
]
[
  {
    group: 'serialNo',
    value: 'tmlFwzqS',
    _id: new ObjectId("6242b62898362599ab2c55df")
  }
]
[
  {
    group: 'serialNo',
    value: 'fTaya2GJ',
    _id: new ObjectId("6242b62898362599ab2c55f0")
  }
]
[
  {
    group: 'serialNo',
    value: 'TsKrXOCI',
    _id: new ObjectId("6242b62898362599ab2c5601")
  }
]
[
  {
    group: 'serialNo',
    value: 'JaA0QhRV',
    _id: new ObjectId("6242b62898362599ab2c5612")
  }
]
[
  {
    group: 'serialNo',
    value: 'NmlPnNN-',
    _id: new ObjectId("6242b62898362599ab2c5623")
  }
]
[
  {
    group: 'serialNo',
    value: 'GxlUpYpM',
    _id: new ObjectId("6242b62898362599ab2c5634")
  }
]
[
  {
    group: 'serialNo',
    value: 'fcO6tfmn',
    _id: new ObjectId("6242b62898362599ab2c5645")
  }
]
[
  {
    group: 'serialNo',
    value: 'gTyVJ1kR',
    _id: new ObjectId("6242b62898362599ab2c5656")
  }
]
[
  {
    group: 'serialNo',
    value: '8BWpXgtE',
    _id: new ObjectId("6242b62898362599ab2c5667")
  }
]
[
  {
    group: 'serialNo',
    value: 'sYhegBjN',
    _id: new ObjectId("6242b62898362599ab2c5678")
  }
]
[
  {
    group: 'serialNo',
    value: '_Q92filo',
    _id: new ObjectId("6242b62898362599ab2c5689")
  }
]
[
  {
    group: 'serialNo',
    value: 'hrEucy3k',
    _id: new ObjectId("6242b62898362599ab2c569a")
  }
]
[
  {
    group: 'serialNo',
    value: 'X3DaC-7a',
    _id: new ObjectId("6242b62898362599ab2c56ab")
  }
]
[
  {
    group: 'serialNo',
    value: 'm16ifpWl',
    _id: new ObjectId("6242b62898362599ab2c56bc")
  }
]
[
  {
    group: 'serialNo',
    value: '196DhSWl',
    _id: new ObjectId("6242b62898362599ab2c56cd")
  }
]
[
  {
    group: 'serialNo',
    value: 'sPKRiLpm',
    _id: new ObjectId("6242b62898362599ab2c56de")
  }
]
[
  {
    group: 'serialNo',
    value: 'mZtMprCw',
    _id: new ObjectId("6242b62898362599ab2c56ef")
  }
]
[
  {
    group: 'serialNo',
    value: 'pmTrLf9l',
    _id: new ObjectId("6242b62898362599ab2c5700")
  }
]
[
  {
    group: 'serialNo',
    value: 'ZVU_pNwM',
    _id: new ObjectId("6242b62898362599ab2c5711")
  }
]
[
  {
    group: 'serialNo',
    value: 'STqmpCjt',
    _id: new ObjectId("6242b62898362599ab2c5722")
  }
]
[
  {
    group: 'serialNo',
    value: 'HtfGKU8D',
    _id: new ObjectId("6242b62898362599ab2c5733")
  }
]
[
  {
    group: 'serialNo',
    value: '796BYHbI',
    _id: new ObjectId("6242b62898362599ab2c5744")
  }
]
[
  {
    group: 'serialNo',
    value: 'oJRGfzr1',
    _id: new ObjectId("6242b62898362599ab2c5755")
  }
]
[
  {
    group: 'serialNo',
    value: 'dLT2k6_u',
    _id: new ObjectId("6242b62898362599ab2c5766")
  }
]
[
  {
    group: 'serialNo',
    value: 'ib7Km_A0',
    _id: new ObjectId("6242b62898362599ab2c5777")
  }
]
[
  {
    group: 'serialNo',
    value: '57wJ-V41',
    _id: new ObjectId("6242b62898362599ab2c5788")
  }
]
[
  {
    group: 'serialNo',
    value: 'P8shENqT',
    _id: new ObjectId("6242b62898362599ab2c5799")
  }
]
[
  {
    group: 'serialNo',
    value: 'CWMFE6p_',
    _id: new ObjectId("6242b62898362599ab2c57aa")
  }
]
[
  {
    group: 'serialNo',
    value: 'QgIs-XNQ',
    _id: new ObjectId("6242b62898362599ab2c57bb")
  }
]
[
  {
    group: 'serialNo',
    value: 'blVp1BOS',
    _id: new ObjectId("6242b62898362599ab2c57cc")
  }
]
[
  {
    group: 'serialNo',
    value: 'R-WiHASw',
    _id: new ObjectId("6242b62898362599ab2c57dd")
  }
]
[
  {
    group: 'serialNo',
    value: 'qIoW9kfi',
    _id: new ObjectId("6242b62898362599ab2c57ee")
  }
]
[
  {
    group: 'serialNo',
    value: 'Wil1Bv9G',
    _id: new ObjectId("6242b62898362599ab2c57ff")
  }
]
[
  {
    group: 'serialNo',
    value: '8tgkwUC5',
    _id: new ObjectId("6242b62898362599ab2c5810")
  }
]
[
  {
    group: 'serialNo',
    value: 'sFA9q377',
    _id: new ObjectId("6242b62898362599ab2c5821")
  }
]
[
  {
    group: 'serialNo',
    value: 'eEDCnQSU',
    _id: new ObjectId("6242b62898362599ab2c5832")
  }
]
[
  {
    group: 'serialNo',
    value: 'XWtPrIyv',
    _id: new ObjectId("6242b62898362599ab2c5843")
  }
]
[
  {
    group: 'serialNo',
    value: 'CM1Ym2lW',
    _id: new ObjectId("6242b62898362599ab2c5854")
  }
]
[
  {
    group: 'serialNo',
    value: 'd-4bOP8-',
    _id: new ObjectId("6242b62898362599ab2c5865")
  }
]
[
  {
    group: 'serialNo',
    value: 'jQfhj-l6',
    _id: new ObjectId("6242b62898362599ab2c5876")
  }
]
[
  {
    group: 'serialNo',
    value: 'WRBSlJwQ',
    _id: new ObjectId("6242b62898362599ab2c5887")
  }
]
[
  {
    group: 'serialNo',
    value: 'iY-iqHlh',
    _id: new ObjectId("6242b62898362599ab2c5898")
  }
]
[
  {
    group: 'serialNo',
    value: 'EFwSBMLp',
    _id: new ObjectId("6242b62898362599ab2c58a9")
  }
]
[
  {
    group: 'serialNo',
    value: 'CwYMNy0f',
    _id: new ObjectId("6242b62898362599ab2c58ba")
  }
]
[
  {
    group: 'serialNo',
    value: 'OKAAhqfX',
    _id: new ObjectId("6242b62898362599ab2c58cb")
  }
]
[
  {
    group: 'serialNo',
    value: 'MzBTiKUk',
    _id: new ObjectId("6242b62898362599ab2c58dc")
  }
]
[
  {
    group: 'serialNo',
    value: 'bBwx8yMK',
    _id: new ObjectId("6242b62898362599ab2c58ed")
  }
]
[
  {
    group: 'serialNo',
    value: 'xUHnZt9E',
    _id: new ObjectId("6242b62898362599ab2c58fe")
  }
]
[
  {
    group: 'serialNo',
    value: 'MdpXt9SB',
    _id: new ObjectId("6242b62898362599ab2c590f")
  }
]
[
  {
    group: 'serialNo',
    value: 'k60LzDDq',
    _id: new ObjectId("6242b62898362599ab2c5920")
  }
]
[
  {
    group: 'serialNo',
    value: 'BTVDFI8F',
    _id: new ObjectId("6242b62898362599ab2c5931")
  }
]
[
  {
    group: 'serialNo',
    value: 'O0zFNy8H',
    _id: new ObjectId("6242b62898362599ab2c5942")
  }
]
[
  {
    group: 'serialNo',
    value: 'qHsf-TRQ',
    _id: new ObjectId("6242b62898362599ab2c5953")
  }
]
[
  {
    group: 'serialNo',
    value: 'n323WI0a',
    _id: new ObjectId("6242b62898362599ab2c5964")
  }
]
[
  {
    group: 'serialNo',
    value: '2y-WLdZc',
    _id: new ObjectId("6242b62898362599ab2c5975")
  }
]
[
  {
    group: 'serialNo',
    value: 'G8XEWAYn',
    _id: new ObjectId("6242b62898362599ab2c5986")
  }
]
[
  {
    group: 'serialNo',
    value: 'FvdyIFSs',
    _id: new ObjectId("6242b62898362599ab2c5997")
  }
]
[
  {
    group: 'serialNo',
    value: 'Ad_e1ylH',
    _id: new ObjectId("6242b62898362599ab2c59a8")
  }
]
[
  {
    group: 'serialNo',
    value: 'wopVmQ3R',
    _id: new ObjectId("6242b62898362599ab2c59b9")
  }
]
[
  {
    group: 'serialNo',
    value: 'QBQUS974',
    _id: new ObjectId("6242b62898362599ab2c59ca")
  }
]
[
  {
    group: 'serialNo',
    value: 'OBAtvNlp',
    _id: new ObjectId("6242b62898362599ab2c59db")
  }
]
[
  {
    group: 'serialNo',
    value: 'C8PkFj8i',
    _id: new ObjectId("6242b62898362599ab2c59ec")
  }
]
[
  {
    group: 'serialNo',
    value: 'H3bNBzDI',
    _id: new ObjectId("6242b62898362599ab2c59fd")
  }
]
[
  {
    group: 'serialNo',
    value: '1h7z4v3D',
    _id: new ObjectId("6242b62898362599ab2c5a0e")
  }
]
[
  {
    group: 'serialNo',
    value: 's3_FgwIf',
    _id: new ObjectId("6242b62898362599ab2c5a1f")
  }
]
[
  {
    group: 'serialNo',
    value: 'Gv_SYCUZ',
    _id: new ObjectId("6242b62898362599ab2c5a30")
  }
]
[
  {
    group: 'serialNo',
    value: 'TshG4O5y',
    _id: new ObjectId("6242b62898362599ab2c5a41")
  }
]
[
  {
    group: 'serialNo',
    value: 'UfJ-H5ea',
    _id: new ObjectId("6242b62898362599ab2c5a52")
  }
]
[
  {
    group: 'serialNo',
    value: 'zX73IoLo',
    _id: new ObjectId("6242b62898362599ab2c5a63")
  }
]
[
  {
    group: 'serialNo',
    value: '_jLgqYHb',
    _id: new ObjectId("6242b62898362599ab2c5a74")
  }
]
[
  {
    group: 'serialNo',
    value: 'RVVMRfGJ',
    _id: new ObjectId("6242b62898362599ab2c5a85")
  }
]
[
  {
    group: 'serialNo',
    value: 'DrfSiC_O',
    _id: new ObjectId("6242b62898362599ab2c5a96")
  }
]
[
  {
    group: 'serialNo',
    value: 'nZBx3jwC',
    _id: new ObjectId("6242b62898362599ab2c5aa7")
  }
]
[
  {
    group: 'serialNo',
    value: 'pdxA6tR_',
    _id: new ObjectId("6242b62898362599ab2c5ab8")
  }
]
[
  {
    group: 'serialNo',
    value: 'XJYONkiw',
    _id: new ObjectId("6242b62898362599ab2c5ac9")
  }
]
[
  {
    group: 'serialNo',
    value: 'PisZTlN7',
    _id: new ObjectId("6242b62898362599ab2c5ada")
  }
]
[
  {
    group: 'serialNo',
    value: 'F0KqKM84',
    _id: new ObjectId("6242b62898362599ab2c5aeb")
  }
]
[
  {
    group: 'serialNo',
    value: 'JKtgQA5g',
    _id: new ObjectId("6242b62898362599ab2c5afc")
  }
]
[
  {
    group: 'serialNo',
    value: 'ZohLo_2v',
    _id: new ObjectId("6242b62898362599ab2c5b0d")
  }
]
[
  {
    group: 'serialNo',
    value: 'Qxk4OGs7',
    _id: new ObjectId("6242b62998362599ab2c5b1e")
  }
]
[
  {
    group: 'serialNo',
    value: '-_YDbF0_',
    _id: new ObjectId("6242b62998362599ab2c5b2f")
  }
]
[
  {
    group: 'serialNo',
    value: 'serial1',
    _id: new ObjectId("6242b62998362599ab2c5b42")
  },
  {
    group: 'brand',
    value: 'Lexus',
    _id: new ObjectId("6242b62998362599ab2c5b43")
  },
  {
    group: 'models',
    value: 'RX330',
    _id: new ObjectId("6242b62998362599ab2c5b44")
  },
  {
    group: 'models',
    value: 'Land Cruiser 200',
    _id: new ObjectId("6242b62998362599ab2c5b45")
  }
]
[
  {
    group: 'serialNo',
    value: 'serial2',
    _id: new ObjectId("6242b62998362599ab2c5b4a")
  },
  {
    group: 'brand',
    value: 'Mers',
    _id: new ObjectId("6242b62998362599ab2c5b4b")
  },
  {
    group: 'brand',
    value: 'BMW',
    _id: new ObjectId("6242b62998362599ab2c5b4c")
  },
  {
    group: 'models',
    value: 'C4',
    _id: new ObjectId("6242b62998362599ab2c5b4d")
  },
  {
    group: 'models',
    value: 'i3',
    _id: new ObjectId("6242b62998362599ab2c5b4e")
  }
]
*/

const FilterOption = mongoose.model('FilterOption', FilterOptionsSchema);
module.exports = FilterOption;
