const mongoose = require('mongoose');
const FilterOption = require('./FilterOption');
const mongoosePaginate = require('mongoose-paginate-v2');

const YearSchema = new mongoose.Schema({
  start: Number,
  end: Number,
});

const CarSchema = new mongoose.Schema({
  brandAuto: {
    type: String,
    required: true,
  },
  model: {
    type: String,
    required: true
  },
  year: {
    type: YearSchema,
  },
  engineVolume: {
    type: String,
  },
  transmission: {
    type: String,
    required: true,
  },
});

const GTDSchema = new mongoose.Schema({
  cars: {
    type: [CarSchema],
    required: true,
  },
  serialNo: {
    type: String,
    required: true,
  },
  partNoDetails: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'PartNumberDetail',
  }],
  description: {
    type: String,
    required: true,
  },
  images: [String],
  created: {
    type: Date,
    default: new Date,
  }
});

GTDSchema.pre('save', async function (next) {
  const carsArr = this.cars;
  let brandsAuto = new Set();
  let modelsAuto = new Set();
  carsArr.forEach(car => {
    brandsAuto.add(car.brandAuto);
    modelsAuto.add(car.model);
  })

  brandsAuto = [...brandsAuto];
  modelsAuto = [...modelsAuto];
  const checkOptions = await Promise.all([
    FilterOption.find({value: {$in: modelsAuto}}),
    FilterOption.find({value: {$in: brandsAuto}}),
    FilterOption.findOne({value: this.serialNo}),
  ])

  const [modelsOptions, brandsOption, serialNoOption] = checkOptions;
  const newOptions = [];
  if (!serialNoOption) {
    newOptions.push(new FilterOption({group: 'serialNo', value: this.serialNo}));
  }
  if (brandsOption?.length !== brandsAuto.length) {
    let nonExistentOptions = [...brandsAuto];
    for (let i = 0; i < brandsOption?.length; i++) {
      if (brandsAuto.includes(brandsOption[i]?.value)) {
        nonExistentOptions = nonExistentOptions.filter(str => str !== brandsOption[i]?.value);
      }
    }

    nonExistentOptions.forEach(value => {
      newOptions.push(new FilterOption({group: 'brand', value}));
    })
  }
  if (modelsOptions?.length !== modelsAuto.length) {
    let nonExistentOptions = [...modelsAuto];
    for (let i = 0; i < modelsOptions?.length; i++) {
      if (modelsAuto.includes(modelsOptions[i]?.value)) {
        nonExistentOptions = nonExistentOptions.filter(str => str !== modelsOptions[i]?.value);
      }
    }

    nonExistentOptions.forEach(value => {
      newOptions.push(new FilterOption({group: 'models', value}));
    })
  }
  if (newOptions.length > 0) {
    const promises = [];
    newOptions.forEach(option => {
      promises.push(option.save());
    })
    await Promise.all(promises);
  }
  next();
})
GTDSchema.plugin(mongoosePaginate);
const ModelGDT = mongoose.model('GDT', GTDSchema);
module.exports = ModelGDT;