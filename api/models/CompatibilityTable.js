const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');



const CompatibilityTableSchema = new mongoose.Schema({
  serialNumber: {
    type: String,
    required: true,
  },
  type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'TypeDetail',
    required: true,
  },
});

CompatibilityTableSchema.plugin(idValidator);
const CompatibilityTable = mongoose.model('CompatibilityTable', CompatibilityTableSchema);
module.exports = CompatibilityTable;