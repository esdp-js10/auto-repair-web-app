const mongoose = require('mongoose');

const validateNameUnique = async value => {
  const warehouse = await Warehouse.findOne({name: value});
  if (warehouse) return false;
};

const validatePhoneNumber = value => {
  const re = /^[0][(][0-9]{2,3}[)]([-][0-9]{2}){3}$/;
  if (!re.test(value)) return false;
};

const WarehouseSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true,
    validate: [
      {validator: validateNameUnique, message: 'Данное название склада уже используется!'},
    ],
  },
  status: {
    type: Boolean,
    required: true,
    default: true
  }
});

const Warehouse = mongoose.model('Warehouse', WarehouseSchema);

module.exports = Warehouse;