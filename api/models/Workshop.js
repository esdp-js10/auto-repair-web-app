const mongoose = require('mongoose');

const validateNameUnique = async value => {
  const workshop = await Workshop.findOne({name: value});
  if (workshop) return false;
};

const validatePhoneNumber = value => {
  const re = /^[0][(][0-9]{2,3}[)]([-][0-9]{2}){3}$/;
  if (!re.test(value)) return false;
};

const WorkshopSchema = new mongoose.Schema({
  warehouse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Warehouse',
    default: null,
  },
  name: {
    type: String,
    required: true,
    validate: [
      {validator: validateNameUnique, message: 'Данное название склада уже используется!'},
    ],
  },
  address: String,
  timetable: String,
  phoneNumber: {
    type: String,
    validate: [
      {validator: validatePhoneNumber, message: 'Введите правильный формат номера телефона 0(XXX)-XX-XX-XX'}
    ],
  },
  status: {
    type: Boolean,
    required: true,
    default: true,
  }
});

const Workshop = mongoose.model('Workshop', WorkshopSchema);

module.exports = Workshop;