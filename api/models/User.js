const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const userConstants = require('../constants/userConstants');

const validateEmail = value => {
  const re = /^(\w+[-.]?\w+)@(\w+)([.-]?\w+)?(\.[a-zA-Z]{2,})$/;
  if (!re.test(value)) return false;
};

const validatePhoneNumber = value => {
  const re = /^[+]*[(]?[0-9]{1,4}[)][0-9]{2,3}([-][0-9]{2}){3}$/;
  if (!re.test(value)) return false;
};

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, 'Необходимо ввести почту'],
    unique: [true, 'test'],
    validate: [
      {validator: validateEmail, message: 'Неверная почта'},
    ],
  },
  password: {
    type: String,
    required: [true, 'Необходимо ввести пароль'],
  },

  name: {
    type: String,
    required: [true, 'Необходимо ввести псевдоним'],
  },
  phone: {
    type: String,
    required: [true, 'Необходимо ввести номер телефона'],
    validate: {validator: validatePhoneNumber, message: 'Phone number is not valid'},
  },
  address: {
    type: String,
    required: [true, 'Необходимо ввести адрес'],
  },
  status: {
    type: String,
    required: true,
    default: 'inactive',
    enum: ['active', 'inactive'],
  },
  avatar: String,
  creation_date: {
    type: Date,
    required: true,
    default: new Date(),
  },
});

UserSchema.path('email').validate(async function (value) {
  if (this.isNew) {
    const user = await User.findOne({email: value});
    if (user) return false;
  }
}, 'This user is already registered!');

UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(userConstants.SALT_WORK_FACTOR);
  this.password = await bcrypt.hash(this.password, salt);

  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.password;
    return ret;
  },
});

UserSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password);
};

const User = mongoose.model('User', UserSchema);

module.exports = User;
