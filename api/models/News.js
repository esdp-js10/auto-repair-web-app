const mongoose = require('mongoose');

const NewsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true,
  },
  text: {
    type: String,
    required: true,
  },
  image: {
    type: String,
  }
});

const News = mongoose.model('News', NewsSchema);

module.exports = News;