require('dotenv').config();
const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const Employee = require("./models/Employee");
const User = require("./models/User");
const Warehouse = require("./models/Warehouse");
const Workshop = require("./models/Workshop");
const News = require("./models/News");
const GTD = require("./models/GDT/ModelGDT");
const Order = require("./models/Order");
const config = require("./config");
const TypeDetail = require("./models/TypeDetail");
const PartNumberDetail = require("./models/PartNumberDetail");
const DetailInTheWarehouse = require("./models/DetailInTheWarehouse");
const AccountsDetail = require("./models/AccountsDetail");

const run = async () => {
  await mongoose.connect(config.db.url)

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  await News.create(
    {
      title: "News 1",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/cpu.jpg'
    }, {
      title: "News 2",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/cpu.jpg',
    },
    {
      title: "News 3",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/hdd.jpg'
    },
    {
      title: "News 4",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/hdd.jpg'
    },
  );

  const [firstUser, secondUser, thirdUser, fourthUser] = await User.create(
    {
      email: 'user@gmail.com',
      password: '123',
      name: 'SimpleDummy',
      phone: '+(996)500-50-50-50',
      status: 'active',
      address: "Bishkek",
    },
    {
      email: 'user2@gmail.com',
      password: '123',
      name: 'SimpleDummy2',
      phone: '+(996)500-50-50-51',
      address: "Bishkek",
      status: "inactive",
    },
    {
      email: 'user2@gmail.com',
      password: '123',
      name: 'SimpleDummy3',
      phone: '+(996)500-50-50-61',
      address: "Bishkek",
      status: "inactive",
    },
    {
      email: 'user3@gmail.com',
      password: '123',
      name: 'SimpleDummy4',
      phone: '+(996)500-50-50-71',
      address: "Bishkek",
      status: "inactive",
    },
  );

  const [warehouseFirst, warehouseSecond, warehouseThird] = await Warehouse.create(
    {
      name: "Склад 1",
      status: true,
    },
    {
      name: "Склад 2",
      status: true,
    },
    {
      name: "Склад 3",
      status: true,
    },
    {
      name: "Склад 4",
      status: false,
    },
  );

  const [firstW, secondW] = await Workshop.create(
    {
      warehouse: warehouseFirst,
      name: 'Мастерская-1',
      address: "Фучика 20",
      timetable: "12.00-20.00",
      phoneNumber: '0(778)-53-32-36',
      status: true
    }, {
      warehouse: warehouseSecond,
      name: 'Мастерская-2',
      address: "Фучика 20",
      timetable: "12.00-20.00",
      phoneNumber: '0(778)-53-32-36',
      status: false
    }, {
      warehouse: warehouseThird,
      name: 'Мастерская-3',
      address: "Фучика 20",
      timetable: "12.00-20.00",
      phoneNumber: '0(778)-53-32-36',
      status: true
    }
  );

  const [first, second, third, fourth, fifth] = await Employee.create(
    {
      email: 'admin@gmail.com',
      password: '123',
      name: 'Admin',
      phoneNumber: '0(500)-50-50-56',
      address: "Bishkek",
      INN: '22345678901234',
      role: 'admin',
    },
    {
      email: 'repairer@gmail.com',
      password: '123',
      name: 'Repairer',
      phoneNumber: '0(500)-50-50-55',
      address: "Bishkek",
      INN: '22345678901235',
      role: 'repairer',
      workshop: firstW,
    },
    {
      email: 'repairer1@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-54',
      address: "Bishkek",
      INN: '22345678901236',
      role: 'repairer',
      workshop: secondW,
      status: false,
    },
    {
      email: 'warehouseman@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-53',
      address: "Bishkek",
      INN: '22345678901237',
      role: 'warehouseman',
      warehouse: warehouseFirst._id,
      status: false,
    },
    {
      email: 'warehouseman1@gmail.com',
      password: '123',
      name: 'Warehouseman',
      phoneNumber: '0(500)-50-50-52',
      address: "Bishkek",
      INN: '22345678901238',
      role: 'warehouseman',
      warehouse: warehouseThird._id,
    },
  );

  const [type1, type2, type3] = await TypeDetail.create(
    {type: 'Гвоздь'},
    {type: 'Болт'},
    {type: 'Шуруп'},
  );

  const [partNo1, partNo2, partNo3, partNo4, partNo5] = await PartNumberDetail.create(
    {type: type1, partNumber: 'a1', sizeIn: '10mm', sizeOut: '20mm', material: 'iron'},
    {type: type1, partNumber: 'a2', sizeIn: '5mm', sizeOut: '8mm', material: 'iron'},
    {type: type2, partNumber: 'b1', sizeIn: '5mm', sizeOut: '8mm', material: 'iron'},
    {type: type2, partNumber: 'b3', sizeIn: '5mm', sizeOut: '2mm', material: 'iron'},
    {type: type3, partNumber: 'c4', sizeIn: '1mm', sizeOut: '3mm', material: 'iron'},
  );


  if (process.env.GDT_TEST_COUNT) {
    await createTestGdt(process.env.GDT_TEST_COUNT);
  }

  const [gdt1, gdt2] = await GTD.create({
      cars: [{
        brandAuto: 'Lexus',
        model: 'RX330',
        year: {
          start: 2004,
          end: 2006,
        },
        engineVolume: '3V',
        transmission: 'Механика'
      }, {
        brandAuto: 'Toyota',
        model: 'Land Cruiser 200',
        year: {
          start: 2010,
          end: 2012,
        },
        engineVolume: '8V',
        transmission: 'Механика'
      }],
      serialNo: 'serial1',
      partNoDetails: [
        partNo1, partNo2, partNo3, partNo5
      ],
      description: 'best japan gdt ever',
      images: [
        'fixtures/gdt/gdt3.1.jpg',
        'fixtures/gdt/gdt3.2.jpg',
      ],
    }, {
      cars: [{
        brandAuto: 'Mers',
        model: 'C4',
        year: {
          start: 2004,
          end: 2006,
        },
        engineVolume: '2.5V',
        transmission: 'Механика'
      }, {
        brandAuto: 'BMW',
        model: 'i3',
        year: {
          start: 2016,
          end: 2018,
        },
        engineVolume: '2V',
        transmission: 'Механика'
      }],
      serialNo: 'serial2',
      partNoDetails: [
        partNo3, partNo4, partNo5
      ],
      description: 'best german gdt ever'
    }
  );

  const [order1, order2, order3, order4] = await Order.create(
    {
      gdt_id: gdt1,
      description: 'text',
      workshop: firstW,
      status: 'Новый',
      customer: secondUser
    },
    {
      gdt_id: gdt2,
      description: 'text text',
      workshop: firstW,
      status: 'Новый',
      customer: firstUser,
    },
    {
      gdt_id: gdt2,
      description: 'description...',
      workshop: firstW,
      status: 'Новый',
      customer: firstUser,
    },
    {
      gdt_id: gdt2,
      description: 'text text',
      workshop: secondW,
      status: 'Новый',
      customer: firstUser,
    },
  );

  await DetailInTheWarehouse.create(
    {
      warehouse: warehouseFirst._id,
      type: type1._id,
      partNumber: partNo2._id,
      sizeIn: '5mm',
      sizeOut: '8mm',
      material: 'iron',
      quantity: '85',
      completedPrice: '800',
    },
    {
      warehouse: warehouseFirst._id,
      type: type1._id,
      partNumber: partNo1._id,
      sizeIn: '10mm',
      sizeOut: '20mm',
      material: 'iron',
      quantity: '100',
      completedPrice: '1200'
    },
    {
      warehouse: warehouseFirst._id,
      type: type2._id,
      partNumber: partNo3._id,
      sizeIn: '5mm',
      sizeOut: '2mm',
      material: 'iron',
      quantity: '365',
      completedPrice: '9980',
    },
    {
      warehouse: warehouseFirst._id,
      type: type2._id,
      partNumber: partNo4._id,
      sizeIn: '5mm',
      sizeOut: '2mm',
      material: 'iron',
      quantity: '3',
      completedPrice: '9980',
    },
    {
      warehouse: warehouseFirst._id,
      type: type3._id,
      partNumber: partNo5._id,
      sizeIn: '5mm',
      sizeOut: '2mm',
      material: 'iron',
      quantity: '565',
      completedPrice: '9980',
    },
    {
      warehouse: warehouseSecond._id,
      type: type3._id,
      partNumber: partNo5._id,
      sizeIn: '1mm',
      sizeOut: '3mm',
      material: 'iron',
      quantity: '965',
      completedPrice: '425'
    },
    {
      warehouse: warehouseSecond._id,
      type: type2._id,
      partNumber: partNo4._id,
      sizeIn: '5mm',
      sizeOut: '2mm',
      material: 'iron',
      quantity: '85',
      completedPrice: '800'
    },
    {
      warehouse: warehouseThird._id,
      type: type1._id,
      partNumber: partNo2._id,
      sizeIn: '5mm',
      sizeOut: '8mm',
      material: 'iron',
      quantity: '1000',
      completedPrice: '800'
    },
  );

  await AccountsDetail.create(
    {
      fromWarehouse: warehouseFirst._id,
      toWarehouse: warehouseThird._id,
      employee: fourth._id,
      date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
      details: [{partNumber: partNo2._id, quantity: 5}],
      typeOfAction: "migration"
    },
    {
      fromWarehouse: warehouseFirst._id,
      toWarehouse: warehouseThird._id,
      employee: fourth._id,
      date: new Date("2022-02-16T14:48:18.371Z").toISOString(),
      details: [{partNumber: partNo3._id, quantity: 1}],
      typeOfAction: "migration"
    },
    {
      fromWarehouse: warehouseThird._id,
      toWarehouse: warehouseFirst._id,
      employee: fifth._id,
      date: new Date("2022-02-16T20:55:26.690Z").toISOString(),
      details: [{partNumber: partNo4._id, quantity: 5}, {partNumber: partNo5._id, quantity: 6},
        {partNumber: partNo1._id, quantity: 2}],
      typeOfAction: "migration"
    },
    {
      fromWarehouse: warehouseFirst._id,
      toWarehouse: warehouseThird._id,
      employee: fourth._id,
      date: new Date("2022-02-16T21:44:49.242Z").toISOString(),
      details: [{partNumber: partNo1._id, quantity: 12}, {partNumber: partNo5._id, quantity: 1}],
      typeOfAction: "migration"
    },
    {
      fromWarehouse: warehouseSecond._id,
      toWarehouse: warehouseFirst._id,
      employee: first._id,
      date: new Date("2022-02-16T23:59:58.375Z").toISOString(),
      details: [{partNumber: partNo4._id, quantity: 1}, {partNumber: partNo3._id, quantity: 1},
        {partNumber: partNo2._id, quantity: 1}, {partNumber: partNo1._id, quantity: 1}],
      typeOfAction: "migration"
    },
  );

  await AccountsDetail.create(
    {
      employee: fourth._id,
      date: new Date("2022-02-15T19:27:03.055Z").toISOString(),
      details: [{partNumber: partNo2._id, quantity: 5, priceInitial: 150}],
      deliveryPrice: '1500',
      commissionPrice: '0',
      customPrice: '0',
      totalPrice: '2250',
      typeOfAction: "income"
    },
    {
      employee: fourth._id,
      date: new Date("2022-02-16T14:48:18.371Z").toISOString(),
      details: [{partNumber: partNo3._id, quantity: 1, priceInitial: 300}],
      deliveryPrice: '2000',
      commissionPrice: '1250',
      customPrice: '5000',
      totalPrice: '8550',
      typeOfAction: "income"
    },
    {
      employee: fifth._id,
      date: new Date("2022-02-16T20:55:26.690Z").toISOString(),
      details: [{partNumber: partNo4._id, quantity: 7, priceInitial: 800},
        {partNumber: partNo5._id, quantity: 6, priceInitial: 150},
        {partNumber: partNo1._id, quantity: 2, priceInitial: 700}],
      deliveryPrice: '1500',
      commissionPrice: '0',
      customPrice: '8000',
      totalPrice: '17400',
      typeOfAction: "income"
    },
    {
      employee: fourth._id,
      date: new Date("2022-02-16T21:44:49.242Z").toISOString(),
      details: [{partNumber: partNo1._id, quantity: 12, priceInitial: 150},
        {partNumber: partNo5._id, quantity: 1, priceInitial: 960}],
      deliveryPrice: '500',
      commissionPrice: '200',
      customPrice: '7950',
      totalPrice: '11410',
      typeOfAction: "income",
    },
    {
      employee: first._id,
      date: new Date("2022-02-16T23:59:58.375Z").toISOString(),
      details: [{partNumber: partNo4._id, quantity: 1, priceInitial: 150},
        {partNumber: partNo3._id, quantity: 1, priceInitial: 150},
        {partNumber: partNo2._id, quantity: 8, priceInitial: 150},
        {partNumber: partNo1._id, quantity: 1, priceInitial: 150}],
      deliveryPrice: '5000',
      commissionPrice: '0',
      customPrice: '14000',
      totalPrice: '20650',
      typeOfAction: "income",
    },
  );

  async function createTestGdt(count = 1) {
    const resultArr = [];
    for (let i = 0; i < count; i++) {
      const brandsArr = ['Avia', 'Bureko', 'Jawa', 'Kaipan', 'Karosa', 'MTX / Metalex', "MW Motors", 'Praga', 'Skoda', 'SVOS', 'Tatra', '9ff', 'ABT Sportsline', 'Audi', 'Alpina', 'Artega', 'Borgward', 'Citycom', 'CityEl', 'Ford-Werke', 'Gemballa', 'Gumpert', 'Isdera', 'Lotec', 'Magirus', 'Maybach', 'Mercedes-AMG', 'Mercedes-Benz', 'Opel', 'Porsche', 'Ruf', 'Smart', 'Volkswagen', 'Wiesmann', 'Lexus', 'Mercedes', 'FunFuani', 'BMW', 'Acura', 'Lexus', 'Toyota'];

      const newGdt = await GTD.create({
        cars: [{
          brandAuto: brandsArr[Math.floor((brandsArr.length - 1) / (1 + Math.floor((Math.random() * brandsArr.length))))],
          model: 1 + Math.floor(Math.random() * 100) + ' Model',
          year: {
            start: 2008 - Math.floor(Math.random() * 100),
            end: 2008 + Math.floor(Math.random() * 100),
          },
          engineVolume: (1 + Math.floor(Math.random() * 10)) + 'V',
          transmission: 'Механика'
        }, {
          brandAuto: brandsArr[Math.floor((brandsArr.length - 1) / (1 + Math.floor((Math.random() * brandsArr.length))))],
          model: 1 + Math.floor(Math.random() * 100) + ' Model',
          year: {
            start: 2008 - Math.floor(Math.random() * 10),
            end: 2008 + Math.floor(Math.random() * 15),
          },
          engineVolume: (1 + Math.floor(Math.random() * 10)) + 'V',
          transmission: 'Механика'
        }, {
          brandAuto: brandsArr[Math.floor((brandsArr.length - 1) / (1 + Math.floor((Math.random() * brandsArr.length))))],
          model: 1 + Math.floor(Math.random() * 100) + ' Model',
          year: {
            start: 2008 - Math.floor(Math.random() * 10),
            end: 2008 + Math.floor(Math.random() * 15),
          },
          engineVolume: (1 + Math.floor(Math.random() * 10)) + 'V',
          transmission: 'Механика'
        }, {
          brandAuto: brandsArr[Math.floor((brandsArr.length - 1) / (1 + Math.floor((Math.random() * brandsArr.length))))],
          model: 1 + Math.floor(Math.random() * 100) + ' Model',
          year: {
            start: 2008 - Math.floor(Math.random() * 10),
            end: 2008 + Math.floor(Math.random() * 15),
          },
          engineVolume: (1 + Math.floor(Math.random() * 10)) + 'V',
          transmission: 'Механика'
        }, {
          brandAuto: brandsArr[Math.floor((brandsArr.length - 1) / (1 + Math.floor((Math.random() * brandsArr.length))))],
          model: 1 + Math.floor(Math.random() * 100) + ' Model',
          year: {
            start: 2008 - Math.floor(Math.random() * 10),
            end: 2008 + Math.floor(Math.random() * 15),
          },
          engineVolume: (1 + Math.floor(Math.random() * 10)) + 'V',
          transmission: 'Механика'
        }],
        serialNo: nanoid(8),
        partNoDetails: [
          partNo1, partNo2, partNo5
        ],
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda consequatur, dignissimos dolorem earum enim et explicabo id iure laborum maiores mollitia neque nesciunt nulla numquam odit perspiciatis quam qui quibusdam quis reiciendis, repudiandae suscipit. Corporis dolores eligendi, esse inventore ipsa laudantium minus officiis optio quis repudiandae similique sint voluptatibus.',
        images: [
          'fixtures/gdt/gdt' + (1 + Math.floor(Math.random() * 5)) + '.1.jpg',
          'fixtures/gdt/gdt' + (1 + Math.floor(Math.random() * 5)) + '.2.jpg',
        ],
      })
      resultArr.push(newGdt);
    }
    return resultArr;
  }

  await mongoose.connection.close();
};

run().catch(console.error);

