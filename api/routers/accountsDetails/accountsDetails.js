const express = require('express');
const authForEmployee = require('../../middleware/authForEmployee');
const AccountsDetail = require('../../models/AccountsDetail');

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
	try {
		if (req.query.typeOfAction === 'income') {
			const incomeData = await AccountsDetail.find({typeOfAction: 'income'})
				.populate({path: 'details.partNumber', select: 'partNumber type', populate: {path: 'type', select: 'type'}})
				.populate('employee', 'name email').sort('-date');
			return res.send(incomeData);
		}

		const accounts = await AccountsDetail.find();
		res.send(accounts);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get('/migration', authForEmployee, async (req, res) => {
	try {
		const migrationData = await AccountsDetail.find({typeOfAction: 'migration'})
			.populate('toWarehouse')
			.populate('fromWarehouse')
			.populate({path: 'details.partNumber', select: 'partNumber type', populate: {path: 'type', select: 'type'}})
			.populate('employee', 'name email').sort('-date');

    res.send(migrationData);
	} catch (error) {
		res.sendStatus(500);
	}
});

module.exports = router;