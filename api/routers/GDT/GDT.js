const express = require('express');
const router = express.Router();
const ModelGDT = require('../../models/GDT/ModelGDT');
const permit = require("../../middleware/permit");
const authForEmployee = require("../../middleware/authForEmployee");
const multer = require("multer");
const config = require("../../config");
const {nanoid} = require("nanoid");
const path = require("path");
const FilterOption = require('../../models/GDT/FilterOption');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  const {models, brand: brands, serialNo} = req.query;

  let {page, itemsPerPage} = req.query;
  const options = {
    page,
    limit: itemsPerPage,
  };
  let search = {};
  if (serialNo) search.serialNo = serialNo;
  if (models) search['cars.model'] = {$in: models};
  if (brands) search['cars.brandAuto'] = {$in: brands};

  try {
    const GDTs = await ModelGDT.paginate(search, options);
    res.send(GDTs)
  } catch (err) {
    res.sendStatus(500);
  }
});

router.get('/all', async (req, res) => {
  try {
    const gTDS = await ModelGDT.find();
    res.send(gTDS);
  } catch (err) {
    res.sendStatus(500);
  }
});

router.get('/options', async (req, res) => {
  try {
    const options = await FilterOption.find();
    res.send(options.sort((a, b) => -b.group.localeCompare(a.group)));
  } catch (err) {
    res.sendStatus(500);
  }
});

router.get('/one/:id', authForEmployee, permit('admin'), async (req, res) => {
  try {
    const GDT = await ModelGDT.findById(req.params.id).populate('partNoDetails');
    res.send(GDT);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/', authForEmployee, permit('admin'), upload.any('images'), async (req, res) => {
  try {
    const cars = JSON.parse(req.body.cars);
    const partNoDetails = JSON.parse(req.body.partNoDetails);
    if (cars.length < 1) return res.status(400).send({message: 'Поля для машины не заполнено!'});
    if (partNoDetails.length < 1) return res.status(400).send({message: 'Поля для парт номер не заполнено!'});
    if (!req.body.serialNo) return res.status(400).send({message: 'Серыйный номер не заполнено!'});
    if (!req.body.description) return res.status(400).send({message: 'Описание не заполнено!'});

    let GDT_data = {
      cars,
      serialNo: req.body.serialNo,
      description: req.body.description,
      partNoDetails,
      images: [],
    };

    if (req.files) {
      req.files.forEach(f => GDT_data.images.push('uploads/' + f.filename));
    }

    const newGDT = new ModelGDT(GDT_data);
    await newGDT.save();
    res.send(newGDT);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.put('/:id', authForEmployee, permit('admin'), upload.any('images'), async (req, res) => {
  try {
    const {id} = req.params;
    const GDT = await ModelGDT.findById(id);
    if (!GDT) return res.status(404).send({message: 'Не найдено!'})
    const cars = JSON.parse(req.body.cars);
    const partNoDetails = JSON.parse(req.body.partNoDetails);
    if (cars.length < 1) return res.status(400).send({message: 'Поля для машины не заполнено!'});
    if (partNoDetails.length < 1) return res.status(400).send({message: 'Поля для парт номер не заполнено!'});
    if (!req.body.serialNo) return res.status(400).send({message: 'Серыйный номер не заполнено!'});
    if (!req.body.description) return res.status(400).send({message: 'Описание не заполнено!'});

    let GDT_data = {
      cars,
      serialNo: req.body.serialNo,
      description: req.body.description,
      partNoDetails,
    };

    if (req.files.length > 0) {
      GDT_data.images = [];
      req.files.forEach(f => GDT_data.images.push('uploads/' + f.filename));
    } else {
      GDT_data.images = GDT.images;
    }

    await ModelGDT.findByIdAndUpdate(id, GDT_data, {new: true});
    res.send(GDT_data);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete('/:id', authForEmployee, permit('admin'), async (req, res) => {
  try {
    const gTD = await ModelGDT.findByIdAndDelete(req.params.id);

    if (gTD) {
      res.send(`ГДТ удалено!`);
    } else {
      res.status(404).send({message: 'ГТФ не найдено!'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;
