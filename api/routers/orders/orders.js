const express = require('express');
const router = express.Router();
const Order = require('../../models/Order');
const Employee = require('../../models/Employee');
const OrderMaster = require('../../models/OrderMaster');
const permit = require("../../middleware/permit");
const authForEmployee = require("../../middleware/authForEmployee");

router.get('/', authForEmployee, async (req, res) => {
  try {
    const query = {};
    if (req.user) {
      if (req.user?.role === 'admin') {
        const orders = await Order.find().populate('workshop', 'name').populate('gdt_id', 'serialNo');
        return res.send(orders);
      }

      if (req.user?.role === 'repairer') {
        const employee = await Employee.findById(req.user._id);
        query.workshop = employee.workshop;
        const orders = await Order.find(query).populate('workshop', 'name').populate('gdt_id', 'serialNo');
        return res.send(orders);
      }

      const orders = await Order.find().populate('workshop', 'name').populate('gdt_id', 'serialNo');
      const ordersFilter = orders.filter(order => order.customer?.email === req.user.email);
      return res.send(ordersFilter);
    }

    res.status(400).send('Something went wrong');
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', authForEmployee, async (req, res) => {
  try {
    const {id} = req.params;
    if (req.user) {
      const orders = await Order.findById(id).populate('workshop', 'name').populate('gdt_id', 'serialNo');
      const receivedOrder = await OrderMaster.findOne({order: orders._id});
      return res.send({...JSON.parse(JSON.stringify(orders)), master: receivedOrder?.comments});
    } else {
      return res.status(403).send('Вы не можете получить заказ!')
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/order/code', async (req, res) => {
  try {
    const query = {};
    if(req.query.code){
      query.orderNumber = req.query.code
    }
    const order = await Order.findOne(query).populate('workshop', 'name');
    if(!order) return res.status(404).send({message: 'По вашему запросу ничего не найдено!'})
    res.send(order)
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/repairer/history', authForEmployee,  async (req, res) => {
  try {
    if (req.user.role === 'repairer') {
      const orders = await Order.find({workshop: req.user.workshop})
        .populate('workshop' )
        .populate('gdt_id', 'serialNo');
      const order = [...orders].filter(o => o.status === 'Выполнено')
      return res.send(order);
    } else if(req.user.role === 'admin') {
      const orders = await Order.find()
        .populate('workshop' )
        .populate('gdt_id', 'serialNo');
      const order = [...orders].filter(o => o.status === 'Выполнено');
      return res.send(order);
    } else {
      return res.status(403).send('Вы не можете получить заказ!')
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', authForEmployee, async (req, res) => {
  try {
    if (!req.body.gdt_id) return res.status(401).send({message: 'Поля для серыйный номер не заполнено!'});
    if (!req.body.workshop) return res.status(401).send({message: 'Поля для мастерских не заполнено!'});
    if (!req.body.description) return res.status(401).send({message: 'Поля для описание не заполнено!'});

    let orderData = {
      gdt_id: req.body.gdt_id,
      workshop: req.body.workshop,
      description: req.body.description,
      customer: req.user
    };

    if (req.body.repairer) {
      orderData.repairer = req.body.warehouseman;
    }

    const order = new Order(orderData);

    await order.save();
    res.send(order);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', authForEmployee, permit('admin', 'repairer'), async (req, res) => {
  try {
    const {id} = req.params;
    const order = await Order.findById(id);
    if (!req.body.gdt_id) return res.status(401).send({message: 'Поля для серыйный номер не заполнено!'});
    if (!req.body.workshop) return res.status(401).send({message: 'Поля для мастерских не заполнено!'});
    if (!req.body.description) return res.status(401).send({message: 'Поля для описание не заполнено!'});

    let orderData = {
      gdt_id: req.body.gdt_id,
      workshop: req.body.workshop,
      repairer: req.body.repairer,
      description: req.body.description,
      _id: id
    };
    if (!order) {
      return res.status(404).send({message: 'Заказ не найдено!'});
    }
    await Order.findByIdAndUpdate(id, orderData, {new: true});
    return res.send(orderData);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.put('/changeStatus/:id', authForEmployee, permit('admin', 'repairer'), async (req, res) => {
  try {
    const {id} = req.params;
    const order = await Order.findById(id);
    if (!req.body.status) return res.status(401).send({message: 'Поля для  статус не заполнено!'});

    if (!order) {
      return res.status(404).send({message: 'Заказ не найдено!'});
    }
    order.status = req.body.status;
    await Order.findByIdAndUpdate(id, order, {new: true});

    await OrderMaster.create({
      order: id,
      repairer: req.user,
    });

    return res.send(order);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id', authForEmployee, permit('admin'), async (req, res) => {
  try {
    const order = await Order.findByIdAndDelete(req.params.id);

    if (order) {
      res.send(`Заказ удалено!`);
    } else {
      res.status(404).send({message: 'Заказ не найдено!'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;
