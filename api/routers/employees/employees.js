const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const Employee = require('../../models/Employee');
const Workshop = require('../../models/Workshop');
const Warehouse = require('../../models/Warehouse');
const TokenModel = require('../../models/Token');
const config = require('../../config');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const userController = require("../../jwt/controllers/user-controller");
const mailService = require("../../jwt/service/mail-service");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/login', userController.loginEmployee);
router.delete('/logout', userController.logout);

router.get('/', authForEmployee, async (req, res) => {
  try {

    if (req.query.employee) {
      const employee = await Employee.findById(req.query.employee);
      res.send(employee);
    } else if (req.query.role) {

      if (req.query.role === 'repairer') {
        const employees = await Employee.find({role: 'repairer'});
        res.send(employees);
      } else if (req.query.role === 'warehouseman') {
        const employees = await Employee.find({role: 'warehouseman'});
        res.send(employees);
      }

    } else if (req.query.workshop) {
      const employees = await Employee.find({workshop: req.query.workshop});
      res.send(employees);
    } else {
      const employees = await Employee.find().populate('warehouse').populate('workshop');
      res.send(employees);
    }

  } catch (e) {
    res.sendStatus(500);
  }
});

router.put('/changeStatus/:id', authForEmployee, permit('admin'), async (req, res) => {
  let employee = await Employee.findById(req.params.id);

  if (employee?.role === 'admin') {
    return res.status(400).send({error: 'Деактивировать администратора нельзя!'});
  }

  if (employee) {
    employee.status = !employee.status;

    try {
      employee = await Employee.findByIdAndUpdate(req.params.id, employee, {new: true});
      if (!employee.status) {
        await TokenModel.findOneAndDelete({employee: employee._id})
      }
      return res.send(employee);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Сотрудник не найден'});
  }
});

router.post('/new', authForEmployee, permit('admin'), upload.single('avatar'), async (req, res) => {
  try {

    const employeeData = {
      email: req.body.email,
      password: req.body.password,
      name: req.body.name,
      INN: req.body.INN,
      phoneNumber: req.body.phoneNumber,
      address: req.body.address,
      role: req.body.role,
    };

    if (req.file) {
      employeeData.avatar = 'uploads/' + req.file.filename;
    } else {
      employeeData.avatar = null;
    }

    if (req.body.workshop) {

      if (employeeData.role === 'repairer') {
        const workshop = await Workshop.findById(req.body.workshop);

        if (!workshop) {
          return res.status(404).send({error: 'Мастерская не найдена!'});
        }

        if (!workshop.status) {
          return res.status(400).send({error: 'Данная мастерская неактивна!'});
        } else {
          employeeData.workshop = req.body.workshop;
        }
      } else {
        return res.status(400).send({error: 'Складовщик не может быть подвязан к мастерской!'});
      }

    }

    if (req.body.warehouse) {

      if (employeeData.role === 'warehouseman') {
        const warehouse = await Warehouse.findById(req.body.warehouse);

        if (!warehouse) {
          return res.status(404).send({error: 'Склад не найдена!'});
        }

        if (!warehouse.status) {
          return res.status(400).send({error: 'Данный склад неактивен!'});
        } else {
          employeeData.warehouse = req.body.warehouse;
        }
      } else {
        return res.status(400).send({error: 'Мастер не может быть подвязан к складу!'});
      }

    }

    const employee = new Employee(employeeData);

    await employee.save();

    const htmlMsg = `
    <div>
      <h1>Вы зарегистрированы администратором на сайте ${process.env.CLIENT_URL}.</h1>
    </div>`;

    mailService.sendMsgToMail(employee.email, `Регистрация на сайте ${process.env.CLIENT_URL} принята`, htmlMsg)

    res.send(employee);
  } catch (error) {
    res.status(400).send(error);
  }
});


router.put('/', authForEmployee, upload.single('avatar'), async (req, res) => {
  const user = req.user.role;

  const newData = {
    email: req.body.email,
    password: req.body.password,
    name: req.body.name,
    INN: req.body.INN,
    phoneNumber: req.body.phoneNumber,
    address: req.body.address,
    role: req.body.role,
  };

  if (req.file) {
    newData.avatar = 'uploads/' + req.file.filename;
  }

  if (req.body.workshop) {

    try {
      const workshop = await Workshop.findById(req.body.workshop);

      if (!workshop) return res.status(404).send({error: 'Мастерская не найдена'});

      if (!workshop.status) {
        res.status(400).send({error: 'Данная мастерская не активна'});
      }

      newData.workshop = req.body.workshop;

    } catch (error) {
      res.status(400).send(error);
    }
  }

  if (req.body.warehouse) {

    try {
      const warehouse = await Warehouse.findById(req.body.warehouse);

      if (!warehouse) return res.status(404).send({error: 'Склад не найден'});

      if (!warehouse.status) {
        res.status(400).send({error: 'Данный склад не активен'});
      }

      newData.warehouse = req.body.warehouse;

    } catch (error) {
      res.status(400).send(error);
    }

  }

  try {

    if (req.query.employee) {

      if (user.role === 'admin') {
        await Employee.findByIdAndUpdate(req.query.employee, newData, {new: true});
        return res.send('Изменения успешно сохранены');
      } else {
        delete newData.INN;
        await Employee.findByIdAndUpdate(req.query.employee, newData, {new: true});
        return res.send('Изменения успешно сохранены');
      }

    } else {
      res.status(404).send({error: 'Сотрудник не найден'});
    }
  } catch (error) {
    res.status(400).send(error);
  }

});


module.exports = router;