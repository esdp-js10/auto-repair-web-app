const express = require('express');
const Warehouse = require('../../models/Warehouse');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const Workshop = require("../../models/Workshop");

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
  try {
	  if (req.query.status === "active"){
		  const activeWarehouses = await Warehouse.find({status: true});
		  return res.send(activeWarehouses);
	  } else if (req.query.status === "inactive") {
		  const inactiveWarehouses = await Warehouse.find({status: false});
		  return res.send(inactiveWarehouses)
	  }

    if (req.query.warehouse){
      const warehouse = await Warehouse.findById(req.query.warehouse);
      return res.send(warehouse);
    } else {
      const warehouses= await Warehouse.find();
      return  res.send(warehouses);
    }
  } catch (e){
    res.sendStatus(500);
  }
});

router.get('/freeWarehouses', authForEmployee, async (req, res) => {
  try{
    const warehouses = await Warehouse.find();
    const workshops = await Workshop.find();
    const freeWarehouses = [];

    for(let warehouse of [...warehouses]){
      let freeWarehouse = [];
      [...workshops].forEach((w, i) => {
        if(warehouse?._id.equals(w.warehouse?._id)){
          freeWarehouse.push(warehouse)
        }

        if(i === [...workshops].length - 1){
          freeWarehouse.length < 1 && freeWarehouses.push(warehouse)
        }
      });
    }

    res.send(freeWarehouses);
  }catch (e){
    console.log(e)
    res.sendStatus(500);
  }
});

router.post('/new', authForEmployee, permit('admin'), async (req, res) => {
  try {
    const warehouseData = {
      name: req.body.name,
    };

    const warehouse = new Warehouse(warehouseData);

    await warehouse.save();
    res.send(warehouse);
  } catch (error){
    res.status(400).send(error);
  }
});



router.put('/changeName/:id', authForEmployee, permit('admin'), async (req, res) => {
  const warehouse = await Warehouse.findById(req.params.id);

  if(warehouse) {
    warehouse.name = req.body.name;

    try {
      await warehouse.save();
      res.send(warehouse);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Склад не найден'});
  }
});

router.put('/changeStatus/:id', authForEmployee, permit('admin'), async (req, res) => {
  const warehouse = await Warehouse.findById(req.params.id);

  if(warehouse) {
    warehouse.status = !warehouse.status;

    try {
      await Warehouse.findByIdAndUpdate(req.params.id, warehouse, {new: true});
      return res.send(warehouse);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Склад не найден'});
  }
});


module.exports = router;

