const express = require('express');
const News = require('../../models/News');
const multer = require('multer');
const config = require("../../config");
const {nanoid} = require("nanoid");
const path = require('path');
const permit = require("../../middleware/permit");
const authForEmployee = require("../../middleware/authForEmployee");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/',async(req, res) => {
  try{
    const news = await News.find();
    res.send(news);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', upload.single("image"), authForEmployee,  permit('admin'), async(req, res) => {
  try{
    if(!req.body.title) return res.status(401).send({message: 'Называние новости не заполнено!'});
    if(!req.body.text) return res.status(401).send({message: 'Описание новости не заполнено!'});

    let newsData = {
      title: req.body.title,
      text: req.body.text
    };

    if (req.file) {
      newsData.image = 'uploads/' + req.file.filename;
    }

    const news = new News(newsData);

    await news.save();
    res.send(news);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', upload.single("image"), authForEmployee,  permit('admin'),async(req, res) => {
  try{
    if(!req.body.title) return res.status(401).send({message: 'Называние новости не заполнено!'});
    if(!req.body.text) return res.status(401).send({message: 'Описание новости не заполнено!'});

    const {id} = req.params;
    const news = await News.findById(id);
    let newsData = {
      title: req.body.title,
      text: req.body.text,
      _id: id
    };

    if (req.file) {
      newsData.image = 'uploads/' + req.file.filename;
    }
    if(!news){
      return res.status(404).send({message: 'Новости не найдено!'});
    }
    await News.findByIdAndUpdate(id, newsData, {new: true});
    return res.send(newsData);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id', authForEmployee, permit('admin'), async(req, res) => {
  try {
    const news = await News.findByIdAndDelete(req.params.id);

    if (news) {
      res.send(`Новости ${news.title} удалено!`);
    } else {
      res.status(404).send({message: 'Новости не найдено!'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;