const express = require('express');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const PartNumberDetail = require('../../models/PartNumberDetail');
const TypeDetail = require("../../models/TypeDetail");

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
  try {
    if (req.query.type) {
      const type = await PartNumberDetail.find({type: req.query.type}).populate('type', 'type');

      if (!type) {
        return res.send({message: 'Part Numbers not'})
      }

      return res.send(type)
    }

    if (req.query.partNumberID) {
      const partNumberID = await PartNumberDetail.findById(req.query.partNumberID);

      if (!partNumberID) {
        return res.status(404).send({error: 'Партномер не найден'});
      }

      return res.send(partNumberID);
    }

    if (req.query.partNumber) {
      const partNumber = await PartNumberDetail.findOne({partNumber: req.query.partNumber});

      if (!partNumber) {
        return res.status(404).send({error: 'Партномер не найден'});
      }

      return res.send(partNumber);
    }

    const partNumbers = await PartNumberDetail.find();
    res.send(partNumbers);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.post('/new', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
  try {
    if (req.query.type) {
      const partNumbers = JSON.parse(req.body.partNumbers);

      const isType = await TypeDetail.findById(req.query.type);
      if (!isType) {
        return res.send({error: 'Type not found'});
      }

      const partNumbersErrorArray = [];
      const newDataArray = [];

      for (let i = 0; i < partNumbers.length; i++) {
        const newPartNumber = {
          type: req.query.type,
          partNumber: partNumbers[i].partNumber,
          sizeIn: partNumbers[i].sizeIn,
          sizeOut: partNumbers[i].sizeOut,
          material: partNumbers[i].material,
        };

        const existPartNumber = await PartNumberDetail.findOne({partNumber: newPartNumber.partNumber});
        const errors = {}
        if(!newPartNumber.material) {
          errors.material = {message: 'Заполните поле'}
        }
        if(!newPartNumber.sizeIn) {
          errors.sizeIn = {message: 'Заполните поле'}
        }
        if(!newPartNumber.sizeOut) {
          errors.sizeOut = {message: 'Заполните поле'}
        }
        if(!newPartNumber.partNumber) {
          errors.partNumber = {message: 'Заполните поле'}
        }
        if(existPartNumber) {
          errors.partNumber = {message: 'Такой парт номер уже существует'}
        }

        partNumbersErrorArray.push(errors);

        const newData = new PartNumberDetail(newPartNumber);
        newDataArray.push(newData);
      }


      for (let j = 0; j < partNumbersErrorArray.length; j++ ) {
        if(Object.keys(partNumbersErrorArray[j]).length > 0) {
          return res.status(400).send(partNumbersErrorArray);
        }
      }

      for(let j = 0; j < newDataArray.length; j++) {
        await newDataArray[j].save();
      }

      return res.send({message: 'Success'});
    }

  } catch (e) {
    res.status(400).send(e);
  }
});


router.put('/edit', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
  try {

    if (req.query.partNumberId) {
      const isPartNumber = await PartNumberDetail.findById(req.query.partNumberId);
      if (!isPartNumber) {
        return res.status(400).send({error: 'Partnumber not found'});
      }
      const newData = {
        type: req.body.type,
        partNumber: req.body.partNumber,
        sizeIn: req.body.sizeIn,
        sizeOut: req.body.sizeOut,
        material: req.body.sizeOut
      }
      if (isPartNumber.type.toString() !== req.body.type.toString()) {
        return res.status(400).send({error: 'Type is not match'});
      }
      const newPartNumber = await PartNumberDetail.findByIdAndUpdate(req.query.partNumberId, newData, {new: true});
      res.send(newPartNumber);
    }
  } catch (e) {
    res.status(400).send(e);
  }
})


module.exports = router;