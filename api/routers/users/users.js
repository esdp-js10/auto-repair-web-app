const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../../models/User');
const TokenModel = require('../../models/Token');
const multer = require('multer');
const config = require("../../config");
const {nanoid} = require("nanoid");
const path = require('path');
const authForEmployee = require("../../middleware/authForEmployee");
const permit = require("../../middleware/permit");
const userController = require("../../jwt/controllers/user-controller");
const ApiError = require('../../jwt/exceptions/api-error');
const mailService = require('../../jwt/service/mail-service');
const TokenService = require('../../jwt/service/token-service');
const userConstants = require("../../constants/userConstants");
const UserDto = require("../../jwt/dtos/user-dto");
const authForUser = require("../../middleware/authForUser");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.post('/login', userController.login);
router.post('/registration', upload.single('avatar'), userController.registration);
router.delete('/logout', userController.logout);

router.get('/inactive', authForEmployee, permit('admin'), async (req, res) => {
  try {
    const notRegUsers = await User.aggregate([
      {$match: {status: 'inactive'}},
      {$sort: {creation_date: -1}},
      {
        $project: {
          email: 1,
          name: 1,
          phone: 1,
          address: 1,
          avatar: 1,
          status: 1,
          creation_date: {$dateToString: {format: "%d/%m/%Y | %H:%M", date: "$creation_date", timezone: '+06:00'}},
        }
      }
    ])
    res.send(notRegUsers);
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }
});

router.post('/active/:id', authForEmployee, permit('admin'), async (req, res) => {
  const id = req.params.id;
  try {
    const user = await User.findById(id);
    if (!user) return res.status(404).send({global: 'Этот пользователь ненайден !'});
    if (user.status === 'inactive') {
      user.status = 'active';
      const htmlMsg = `
        <div>
            <h1>Регистрация одобрена.</h1>
            <p>Вы можете войти на сайт ${process.env.CLIENT_URL}.</p>
        </div>`;
      mailService.sendMsgToMail(user.email, `Ваша регистрация на сайте ${process.env.CLIENT_URL} принята`, htmlMsg);

    } else {
      throw ApiError.BadRequest('Этот пользователь уже активирован');
    }
    await user.save({validateBeforeSave: false});
    res.send(user);
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }
});

router.put('/', authForUser, upload.single('avatar'), async (req, res) => {
  try {
    if (req.query.user) {
      const newData = {
        email: req.body.email,
        name: req.body.name,
        phone: req.body.phone,
        address: req.body.address,
      };

      const userDb = await User.findById(req.query.user);

      if (!userDb) {
        return res.status(400).send({error: 'Что то не то с вашим профилем свяжитесь с администратором'});
      }

      const userUpdateErrors = {};

      if (userDb?.email !== req.body.email) {
        const user = await User.findOne({email: req.body.email});
        if (user) userUpdateErrors.email = {message: 'Пользователь с таким email уже существует'};
      }


      if (req.file) newData.avatar = '/uploads/' + req.file.filename;

      if (req.body.password !== '' && req.body.currentPassword === '') {
        userUpdateErrors.currentPassword = {message: 'Введите текущий пароль'};
      }

      if (req.body.password !== '') {
        const salt = await bcrypt.genSalt(userConstants.SALT_WORK_FACTOR);
        newData.password = await bcrypt.hash(req.body.password, salt);
      }

      const isMatch = await userDb.checkPassword(req.body.currentPassword);

      if (!isMatch) {
        return res.status(400).send({error: 'Неверный пароль'});
      }

      if (!isMatch) userUpdateErrors.currentPassword = {message: 'Пароль неверный!'};

      if (Object.keys(userUpdateErrors).length !== 0) {
        return res.status(400).send({errors: userUpdateErrors});
      }

      const user = await User.findByIdAndUpdate(
        req.query.user,
        newData,
        {new: true, runValidators: true}
      );

      const currToken = await TokenModel.findOne({user: user._id})

      let newTokens = null;
      if (currToken) {
        newTokens = TokenService.generateTokens({...new UserDto(user)});
        currToken.refreshToken = newTokens.refreshToken;
        res.cookie('refreshToken', newTokens.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true,})//"secure: true" on production need to be activated with https
        await currToken.save();
      }

      return res.send({...user._doc, ...newTokens});
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', authForEmployee, permit('admin'), async (req, res) => {
  try {
    await User.findByIdAndRemove(req.params.id);
    res.send({global: 'Пользователь удалён'});
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }
});

module.exports = router;
