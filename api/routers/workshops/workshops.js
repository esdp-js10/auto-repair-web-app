const express = require('express');
const Workshop = require('../../models/Workshop');
const authForEmployee = require("../../middleware/authForEmployee");
const permit = require("../../middleware/permit");
const Warehouse = require("../../models/Warehouse");

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
  try {

    if (req.query.workshop) {
      const workshop = await Workshop.findById(req.query.workshop).populate('warehouse', 'name');
      res.send(workshop);
    } else if (req.query.status) {

      if (req.query.status === 'active') {
        const activeWorkshops = await Workshop.find({status: true});
        res.send(activeWorkshops);
      } else if (req.query.status === 'inactive') {
        const notActiveWorkshops = await Workshop.find({status: false});
        res.send(notActiveWorkshops);
      }

    } else {
      const workshops = await Workshop.find().populate("warehouse");
      res.send(workshops);
    }

  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/freeWorkshops', async (req, res) => {
  try {
    const freeWorkshops = await Workshop.find({warehouse: null});
    res.send(freeWorkshops);
  } catch (e) {
    console.log(e)
    res.sendStatus(500);
  }
});

router.post('/new', authForEmployee, permit('admin'), async (req, res) => {
  try {
    const workshopData = {
      name: req.body.name,
      address: req.body.address || null,
      timetable: req.body.timetable || null,
      phoneNumber: req.body.phoneNumber || null,
    };

    if (req.body.warehouse) {
      const warehouse = await Warehouse.findById(req.body.warehouse._id);
      const workshop = await Workshop.findOne({warehouse: req.body.warehouse});

      if (!warehouse) return res.status(404).send({error: 'Склад не найден'});

      if (workshop) return res.status(400).send({error: 'Данный склад используется другой мастерской!'});

      if (!warehouse.status) {
        return res.status(400).send({error: 'Данный склад неактивен!'});
      } else {
        workshopData.warehouse = req.body.warehouse;
      }

    } else {
      workshopData.warehouse = null;
    }

    const workshop = new Workshop(workshopData);

    await workshop.save();
    res.send(workshop);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/changeName/:id', authForEmployee, permit('admin'), async (req, res) => {
  const workshop = await Workshop.findById(req.params.id);
  if (!workshop) return res.status(404).send({error: 'Мастерская не найдена'});
  const anotherWorkshop = await Workshop.findOne({name: req.body.name});
  if (anotherWorkshop && anotherWorkshop?.name !== workshop.name) {
    res.status(400).send({error: 'Данное имя используется другой мастерской'});
  }

  try{
    const workshopData = {
      name: req.body.name,
      address: req.body.address || null,
      timetable: req.body.timetable || null,
      phoneNumber: req.body.phoneNumber || null,
    };

    if (req.body.warehouse) {
      const warehouse = await Warehouse.findById(req.body.warehouse?._id);

      const w = await Workshop.findOne({warehouse: req.body.warehouse?._id});

      if (!warehouse) {
        return res.status(404).send({error: 'Склад не найден'});
      }else if(!w){
        workshopData.warehouse = req.body.warehouse;
      }else if(!warehouse.status){
        return res.status(400).send({error: 'Данный склад неактивен!'});
      }else if(!w?._id.equals(workshop?._id)){
        return res.status(400).send({error: 'Данный склад используется другой мастерской!'});
      }
    } else {
      workshopData.warehouse = null;
    }

    await Workshop.findByIdAndUpdate(req.params.id, workshopData, {new: true});
    return res.send(workshopData);
  }catch (e){
    res.status(400).send(error);
  }
})

    router.put('/changeStatus/:id', authForEmployee, permit('admin'), async (req, res) => {

      const workshop = await Workshop.findById(req.params.id);

      if (workshop) {
        workshop.status = !workshop.status;

        try {
          await Workshop.findByIdAndUpdate(req.params.id, workshop, {new: true});
          return res.send(workshop);
        } catch (error) {
          res.status(400).send(error);
        }
      } else {
        res.status(404).send({error: 'Мастерская не найдена'});
      }

    });


    module.exports = router;