const express = require('express');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const TypeDetail = require('../../models/TypeDetail');

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
	try {
		if (req.query.type) {
			const type = await TypeDetail.findById(req.query.type);

			if (!type) {
				return res.status(404).send({error: 'Тип не найден'});
			}

			return res.send(type);
		}

		const types = await TypeDetail.find();
		res.send(types);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
	// if (!req.body.type) {
	// 	return res.status(400).send('Data not valid');
	// }

	const typeData = {
		type: req.body.type,
	};

	try {
		const type = new TypeDetail(typeData);
		await type.save();
		res.send(type);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.put('/changeType/:id', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
  const detail = await TypeDetail.findById(req.params.id);

  if(detail) {
    detail.type = req.body.type;

    try {
      await detail.save();
      res.send(detail);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Тип не найден'})
  }
});


module.exports = router;