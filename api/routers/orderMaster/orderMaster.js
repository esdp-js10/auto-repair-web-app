const express = require('express');
const router = express.Router();
const authForEmployee = require('../../middleware/authForEmployee');
const OrderMaster = require("../../models/OrderMaster");
const Workshop = require("../../models/Workshop");
const Order = require("../../models/Order");
const DetailInTheWarehouse = require("../../models/DetailInTheWarehouse");
const PartNumberDetail = require("../../models/PartNumberDetail");
const mailService = require('../../jwt/service/mail-service');

router.get('/availableDetails', authForEmployee, async (req, res) => {
  try {
    const query = {
      order: req.query.order,
    };

    const availableOrderPartNumbers = await Order.findById(query.order)
      .populate({
        path: 'gdt_id',
        populate: {
          path: 'partNoDetails',
          select: 'partNumber',
          populate: {
            path: 'type',
          }
        }
      });

    const warehouseId = (await Workshop.findById(req.user.workshop)).warehouse;

    const availableOrderPartNumbersInTheWarehouse = [];
    for (let availablePartNumber of availableOrderPartNumbers.gdt_id.partNoDetails) {
      const detailInTheWarehouseExists = await DetailInTheWarehouse.findOne({
        warehouse: warehouseId,
        type: availablePartNumber.type._id,
        partNumber: availablePartNumber._id,
      }).populate('type', 'type').populate('partNumber', 'partNumber');
      detailInTheWarehouseExists && availableOrderPartNumbersInTheWarehouse.push(detailInTheWarehouseExists);
    }

    res.send(availableOrderPartNumbersInTheWarehouse);
  } catch (e) {
    res.status(500).send(e)
  }
});

router.get('/orderInfo', authForEmployee, async (req, res) => {
  try {
    const orderMaster = await OrderMaster.findOne({
      order: req.query.order,
    });
    res.send(orderMaster || 'success');
  } catch (e) {
    res.status(500).send(e);
  }
});

router.post('/changeItems', authForEmployee, async (req, res) => {
  try {
    // Looking for order to work with
    const order = await Order.findById(req.body.order);
    if (!order) {
      return res
        .status(404)
        .send({global: 'Incorrect order. Please check order !'});
    } else if (order.status === 'Отклонено' || order.status === 'Выполнено') {
      return res
        .status(404)
        .send({global: 'You cannot update fields because this order has already done or canceled'});
    }

    // Looking for warehouse of employee (repairer)
    const warehouseId = (await Workshop.findById(req.user.workshop)).warehouse;

    // checking input fields
    const checkOrderMaster = new OrderMaster({
      order: order,
      repairer: req.user,
      reserved: req.body.parts,
    });

    const validationErrors = checkOrderMaster.validateSync();
    if (validationErrors) {
      return res.status(400).send(validationErrors);
    }

    // all accepted orders of workshop, except current order
    let acceptedOrders =
      await OrderMaster
        .find({
          workshop: req.user.workshop,
          order: {$ne: order},
        })
        .populate('order', 'status'); // со статусом 'Принято'

    acceptedOrders = acceptedOrders.filter(orderItem => orderItem.order.status === 'Принято');

    // Array of details coming to request
    const partsReserved = req.body.parts;
    for (let itemReserved of partsReserved) {
      const detailInTheWarehouse =
        await DetailInTheWarehouse.findOne({
          warehouse: warehouseId,
          type: itemReserved.type,
          partNumber: itemReserved.partNumber
        });

      const partNumberExisted = await PartNumberDetail.findById(itemReserved.partNumber).populate('type', 'type');
      if (!detailInTheWarehouse) {        // detail not exists in Warehouse
        if (!partNumberExisted) {
          return res.status(404).send({global: 'PartNumber not found'});
        } else {
          return res
            .status(404)
            .send({
              global: `No such detail (${partNumberExisted.type.type} - ${partNumberExisted.partNumber}) in the warehouse. Please check it !`
            });
        }
      } else {  // if detail exists in the warehouse then check quantity
        const reservedQuantitySum = acceptedOrders.reduce((acc, order) => {
          const reservedDetail = order.reserved //looking for detail in each order by using 'filter'
            .filter(detail => detail.type.equals(itemReserved.type) && detail.partNumber.equals(itemReserved.partNumber))
          if (reservedDetail[0]) {
            return acc + reservedDetail[0].reservedQuantity;
          }
          return acc;
        }, 0);

        const availableQuantity = detailInTheWarehouse.quantity - reservedQuantitySum - itemReserved.reservedQuantity;

        if (availableQuantity < 0) {
          return res
            .status(404)
            .send({
              global: `Not enough detail (${partNumberExisted.type.type} - ${partNumberExisted.partNumber}) in the warehouse. Please check it !`
            })
        }
      }
    }

    // updating fields
    const updatedOrderMaster = await OrderMaster.findOneAndUpdate(
      {
        order: order,
      },
      {
        reserved: partsReserved,
        extraDetails: req.body.extraDetails,
        extraWorks: req.body.extraWorks,
        $push: {comments: {title: req.body.comment, date: new Date()}},
      }, {
        new: true,
      },
    );

    // subtract quantity if order is done
    if (req.body.status === 'Выполнено') {
      for (const reservations of updatedOrderMaster.reserved) {
        // Подтягиваем текущие данные из DetailInTheWarehouse
        const oldData = await DetailInTheWarehouse.findOne({
          warehouse: warehouseId,
          type: reservations.type,
          partNumber: reservations.partNumber, //details[d].partNumber
        });

        await DetailInTheWarehouse.findOneAndUpdate({
            warehouse: warehouseId,
            type: reservations.type,
            partNumber: reservations.partNumber,
          },
          {
            $inc: {
              quantity: -reservations.reservedQuantity,
            },
            completedPrice: (
              ((Number(oldData.quantity) * Number(oldData.completedPrice)) +
                ((Number(oldData.quantity) - Number(reservations.reservedQuantity)) * Number(oldData.completedPrice))) /
              (Number(oldData.quantity) + (Number(oldData.quantity) - Number(reservations.reservedQuantity)))
            ).toFixed(3),
          },
          {
            new: true,
          },
        );
      }
    }

    // changing status if order is completed
    if (req.body.status === 'Выполнено' || req.body.status === 'Отклонено') {
      await Order.findByIdAndUpdate(order, {
        status: req.body.status,
      });
    }

    if (req.body.status === 'Выполнено') {
      const messageTextSuccess = '<div><p>Ваш заказ выполнен</p></div>';
      mailService.sendMsgToMail(order.customer.email, 'Инфо по заказу',  messageTextSuccess);
    } else if (req.body.status === 'Отклонено') {
      const messageTextCanceled = '<div><p>Ваш заказ отклонен</p></div>';
      mailService.sendMsgToMail('order.customer.email', 'Инфо по заказу',  messageTextCanceled);
    }

    res.send(updatedOrderMaster || 'success');
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;