const express = require('express');
const CompatibilityTable = require('../../models/CompatibilityTable');
const authForEmployee = require('../../middleware/authForEmployee');

const router = express.Router();

router.get('/', authForEmployee, async(req, res) => {
  try{
    const compatibilityTables = await CompatibilityTable.find();
    res.send(compatibilityTables);
  }catch{
    res.sendStatus(500);
  }
});


module.exports = router;