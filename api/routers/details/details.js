const express = require('express');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const AccountsDetail = require('../../models/AccountsDetail');
const PartNumberDetail = require("../../models/PartNumberDetail");
const DetailInTheWarehouse = require("../../models/DetailInTheWarehouse");
const Warehouse = require("../../models/Warehouse");

const router = express.Router();

router.post('/', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
  try {
    if (req.query.warehouse) {
	    const myWarehouse = await Warehouse.findById(req.query.warehouse);

	    if (!myWarehouse) {
		    return res.status(404).send({error: 'Склад не найден!'});
	    }

	    if (!myWarehouse.status) {
		    return res.status(404).send({error: 'Склад не активен!'});
	    }

    	if (req.user.role !== 'admin') {
		    if (req.user.warehouse.toString() !== req.query.warehouse.toString()) {
			    return res.status(403).send({error: 'У вас нет прав на выполнение этой операции!'});
		    }
	    }

      const arrDetails = JSON.parse(req.body.details);

      const data = {
        employee: req.user._id,
        fromWarehouse: req.body.fromWarehouse || null,
        toWarehouse: req.body.toWarehouse || null,
        date: new Date().toISOString(),
        details: JSON.parse(req.body.details),
        deliveryPrice: req.body.deliveryPrice,
        commissionPrice: req.body.commissionPrice,
        customPrice: req.body.customPrice,
        totalPrice: req.body.totalPrice,
        title: req.body.title || '',
        description: req.body.description || '',
        typeOfAction: req.body.typeOfAction,
      };

			const newData = new AccountsDetail(data);
			const validationErrors = newData.validateSync();

			if (validationErrors) {
				return res.status(400).send(validationErrors);
			}

			for (let d = 0; d < arrDetails.length; d++) {

				const findByPartNumberId = await PartNumberDetail.findById(arrDetails[d].partNumber);

        const detailData = {
          warehouse: req.query.warehouse,
          type: arrDetails[d].type,
          partNumber: arrDetails[d].partNumber,
          sizeIn: findByPartNumberId.sizeIn,
          sizeOut: findByPartNumberId.sizeOut,
          material: findByPartNumberId.material,
          quantity: arrDetails[d].quantity,
          typeOfAction: arrDetails[d].typeOfAction,
	        completedPrice: Number(arrDetails[d].priceInitial) + Number(req.body.deliveryPrice) +
		                      Number(req.body.commissionPrice) + Number(req.body.customPrice),
        };

        const hasPartNumber = await DetailInTheWarehouse.findOne({warehouse: req.query.warehouse, partNumber: arrDetails[d].partNumber});

        if (hasPartNumber) {
          if (arrDetails[d].typeOfAction === 'income') {
            await DetailInTheWarehouse.findOneAndUpdate(
              {warehouse: req.query.warehouse, partNumber: arrDetails[d].partNumber},
              {$inc: {quantity: +arrDetails[d].quantity},
	              completedPrice: ((
	              	(Number(hasPartNumber.quantity) * Number(hasPartNumber.completedPrice)) +
		              (Number(arrDetails[d].quantity) * (Number(arrDetails[d].priceInitial) +
			             Number(req.body.commissionPrice) + Number(req.body.customPrice) + Number(req.body.deliveryPrice)))) /
		              (Number(arrDetails[d].quantity) + Number(hasPartNumber.quantity))).toFixed(3),
              },
              {new: true}
            );
          }

          if (arrDetails[d].typeOfAction === 'expense') {
            await DetailInTheWarehouse.findOneAndUpdate(
              {warehouse: req.query.warehouse, partNumber: arrDetails[d].partNumber},
              {$inc: {quantity: -arrDetails[d].quantity}},
              {new: true}
            );
          }
        } else {
          const detail = new DetailInTheWarehouse(detailData);
          await detail.save();
        }
      }

        await newData.save();
        return res.send({message: 'Success!'});
      }
    } catch (error) {
      res.status(400).send(error);
    }
});

router.post('/moving', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
	try {
		const fromWarehouse = await Warehouse.findById(req.body.fromWarehouse);
		const toWarehouse = await Warehouse.findById(req.body.toWarehouse);

		if (req.user.role !== 'admin') {
			if (req.user.warehouse.toString() !== req.body.fromWarehouse.toString()) {
				return res.status(403).send({error: 'У вас нет прав на выполнение этой операции!'});
			}
		}

		if (!fromWarehouse) {
			return res.status(404).send({error: 'Склад-отправитель не найден!'});
		}

		if (!fromWarehouse.status) {
			return res.status(400).send({error: 'Склад-отправитель неактивен!'})
		}

		if (!toWarehouse) {
			return res.status(404).send({error: 'Склад-получатель не найден!'});
		}

		if (!toWarehouse.status) {
			return res.status(400).send({error: 'Склад-получатель неактивен!'})
		}

		const details = JSON.parse(req.body.details);

		for (let d = 0; d < details.length; d++) {
			const oldData = await DetailInTheWarehouse.findOne({warehouse: req.body.fromWarehouse, partNumber: details[d].partNumber});

			await DetailInTheWarehouse.findOneAndUpdate(
				{warehouse: req.body.fromWarehouse, partNumber: details[d].partNumber},
				{$inc: {quantity: -details[d].quantity},
					completedPrice: (
						((Number(oldData.quantity) * Number(oldData.completedPrice)) +
						((Number(oldData.quantity) - Number(details[d].quantity)) * Number(oldData.completedPrice))) /
						(Number(oldData.quantity) + (Number(oldData.quantity) - Number(details[d].quantity))))
							.toFixed(3),
					},
				{new: true},
				);
		}

		for (let d = 0; d < details.length; d++) {
			const fromWarehouseData = await DetailInTheWarehouse.findOne({warehouse: req.body.fromWarehouse, partNumber: details[d].partNumber});

			const findDetailByPartNumber = await DetailInTheWarehouse.findOne(
				{warehouse: req.body.toWarehouse, partNumber: details[d].partNumber});

			if (!findDetailByPartNumber) {
				const findByPartNumberId = await PartNumberDetail.findById(details[d].partNumber);

				const newDetail = {
					warehouse: req.body.toWarehouse,
					type: findByPartNumberId.type,
					partNumber: details[d].partNumber,
					sizeIn: findByPartNumberId.sizeIn,
					sizeOut: findByPartNumberId.sizeOut,
					material: findByPartNumberId.material,
					quantity: details[d].quantity,
					completedPrice: fromWarehouseData.completedPrice
				};

				const detail = new DetailInTheWarehouse(newDetail);
				await detail.save();
			} else {
				await DetailInTheWarehouse.findOneAndUpdate(
					{warehouse: req.body.toWarehouse, partNumber: details[d].partNumber},
					{$inc: {quantity: +details[d].quantity},
						completedPrice: (
							((Number(findDetailByPartNumber.completedPrice) * Number(findDetailByPartNumber.quantity)) +
							(Number(details[d].quantity) * Number(fromWarehouseData.completedPrice))) /
							(Number(details[d].quantity) + Number(findDetailByPartNumber.quantity))).toFixed(3),
					},
					{new: true},
				);
			}
		}

		const MovingData = {
			employee: req.user._id,
			fromWarehouse: req.body.fromWarehouse,
			toWarehouse: req.body.toWarehouse,
			date: new Date().toISOString(),
			details: details,
			typeOfAction: 'migration',
		};

		const newData = new AccountsDetail(MovingData);
		await newData.save();
		res.send({message: 'Success!'});
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;
