require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routers/users/users');
const employees = require('./routers/employees/employees');
const GDTs = require('./routers/GDT/GDT');
const warehouses = require('./routers/warehouses/warehouses');
const workshops = require('./routers/workshops/workshops');
const news = require('./routers/news/news');
const typeDetails = require('./routers/typeDetails/typeDetails');
const partNumbersDetails = require('./routers/partNumbersDetails/partNumbersDetails');
const detailsInTheWarehouses = require('./routers/detailsInTheWarehouse/detailsInTheWarehouse');
const accountsDetails = require('./routers/accountsDetails/accountsDetails');
const details = require('./routers/details/details');
const orders = require('./routers/orders/orders');
const compatibilityTables = require('./routers/compatibilityTables/compatibilityTables');
const orderMaster = require('./routers/orderMaster/orderMaster');
const errorMiddleware = require('./middleware/error-middleware');
const userController = require("./jwt/controllers/user-controller");


const server = express();
server.use(express.json());
server.use(cookieParser());
server.use(express.static('public'));
server.use(cors({
  credentials: true,
  origin: process.env.CLIENT_URL,
}));

server.use('/users', users);
server.use('/employees', employees);
server.get('/refresh', userController.refresh);
server.use('/GDTs', GDTs);
server.use('/warehouses', warehouses);
server.use('/workshops', workshops);
server.use('/news', news);
server.use('/typeDetails', typeDetails);
server.use('/partNumbersDetails', partNumbersDetails);
server.use('/detailsInTheWarehouses', detailsInTheWarehouses);
server.use('/accountsDetails', accountsDetails);
server.use('/details', details);
server.use('/orders', orders);
server.use('/compatibilityTables', compatibilityTables);
server.use('/orderMaster', orderMaster);

server.get('/', (req, res) => {
    res.send('Hello team')
})

server.use(errorMiddleware);

const run = async () => {
  await mongoose.connect(config.db.url);

  server.listen(config.port, () => {
    console.log(`Server is started on ${config.port} port !`);
  })

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
}
run().catch(e => console.error(e));